#ifndef CHIASS_OPENING_HPP
#define CHIASS_OPENING_HPP
#include <ctime>

#include "chess.hpp"
#include "hash.hpp"




namespace ChIAss {
    namespace Opening {
        typedef struct openings_position_ {
            u64 h;

            // Number of different lines that are planned in the opening
            int n_lines;
            Chess::Move* lines;
            //  Probas associated with each lines
            int* probas;
        } OpeningPosition;
        extern size_t n_opening_boards;
        extern OpeningPosition* ChIAss_openings;

        /*
            * \brief Loads an opening for ChIAss from a file. The opening is loaded in ChIAss_opening.
            * \param filename The name of the file containing the opening.
            * \return 0 if the loading went accordingly, -1 if any error happened.
        */
        int LoadOpening(char* filename);

        void ClearOpening(void);
    }
}

#endif // CHIASS_OPENING_HPP