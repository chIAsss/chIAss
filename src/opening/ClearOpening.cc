#include "opening.hpp"

void ChIAss::Opening::ClearOpening(void) {
    for (size_t i=0; i<n_opening_boards; i++) {
        free(ChIAss_openings[i].lines);
        free(ChIAss_openings[i].probas);
    }
    free(ChIAss_openings);
    ChIAss_openings = NULL;
}