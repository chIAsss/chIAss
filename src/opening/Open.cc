#include "engine.hpp"


bool ChIAss::Engine::Open(const Chess::Board board, Chess::Move* returned) {
    using namespace Chess;

    returned->type = NORMAL_MOVE;

    u64 target_hash = Hash(board);

    size_t lower_bound = 0;
    size_t upper_bound = Opening::n_opening_boards;
    size_t middle = (lower_bound + upper_bound) / 2;

    while ( lower_bound != upper_bound ) {
        if ( target_hash > Opening::ChIAss_openings[middle].h ) { // item > middle 
            lower_bound = middle + 1;
        } else if ( target_hash < Opening::ChIAss_openings[middle].h ) { // item < middle
            upper_bound = middle;
        } else { // item == middle
            lower_bound = middle;
            upper_bound = middle;
        }
        middle = (lower_bound + upper_bound) / 2;
    }

    if ( target_hash != Opening::ChIAss_openings[middle].h ) {// Not in the opening database...
        returned->beg = not_a_square;
        returned->end = not_a_square;
        return false;
    } else {
        srand(time(NULL));
        int prob = rand()%100;
        int id_mov=0;
        int sum = Opening::ChIAss_openings[middle].probas[id_mov];

        while (prob >= sum) {
            id_mov ++;
            sum += Opening::ChIAss_openings[middle].probas[id_mov];
        }
        *returned = Opening::ChIAss_openings[middle].lines[id_mov];
        return true;
    }
}