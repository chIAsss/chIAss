#include "opening.hpp"

size_t ChIAss::Opening::n_opening_boards = 0;
ChIAss::Opening::OpeningPosition* ChIAss::Opening::ChIAss_openings=NULL;

bool FENToBoard_file(FILE* file, ChIAss::Chess::Board* board) {
    using namespace ChIAss::Chess;

    char char_read;
    // a cursor would go : 56->...->63->48->...->55->40->...->47 ...
    size_t cursor_c = 0;
    size_t cursor_r = 7;
    size_t cursor;

    while (cursor_r < 8) {
        char_read = getc(file);
        if (char_read == EOF) {
            return false;
        }
        cursor = 8*cursor_r + cursor_c;
        switch (char_read) {
            case 'P':
                if (cursor_r == 1) {
                    board->B[cursor] = {STARTING_PAWN, WHITE};
                } else {
                    board->B[cursor] = {PAWN, WHITE};
                }
            break;
            case 'p':
                if (cursor_r == 6) {
                    board->B[cursor] = {STARTING_PAWN, BLACK};
                } else {
                    board->B[cursor] = {PAWN, BLACK};
                }
            break;

            case 'K':
                if (cursor == 4) {
                    board->B[cursor] = {KING_CAN_CASTLE, WHITE};
                } else {
                    board->B[cursor] = {KING, WHITE};
                }
            break;
            case 'k':
                if (cursor == 60) {
                    board->B[cursor] = {KING_CAN_CASTLE, BLACK};
                } else {
                    board->B[cursor] = {KING, BLACK};
                }
            break;

            case 'R':
                if(cursor == 0 || cursor == 7) {
                    board->B[cursor] = {ROOK_CAN_CASTLE, WHITE};
                } else {
                    board->B[cursor] = {ROOK, WHITE};
                }
            break;
            case 'r':
                if(cursor == 56 || cursor == 63) {
                    board->B[cursor] = {ROOK_CAN_CASTLE, BLACK};
                } else {
                    board->B[cursor] = {ROOK, BLACK};
                }
            break;

            case 'N':
                board->B[cursor] = {KNIGHT, WHITE};
            break;
            case 'n':
                board->B[cursor] = {KNIGHT, BLACK};
            break;

            case 'B':
                board->B[cursor] = {BISHOP, WHITE};
            break;
            case 'b':
                board->B[cursor] = {BISHOP, BLACK};
            break;

            case 'Q':
                board->B[cursor] = {QUEEN, WHITE};
            break;
            case 'q':
                board->B[cursor] = {QUEEN, BLACK};
            break;

            default: // read a number
                for (size_t i=0; i<(size_t) char_read-48; i++) {
                    board->B[cursor+i] = {EMPTY, WHITE};
                }
                cursor_c += char_read-49;
            break;
        }
        cursor_c ++;
        if (cursor_c == 8) {
            cursor_c = 0;
            cursor_r --;
//            fscanf(file, "/");
            char_read = getc(file);
        }
    }
    return true;
}

void SortOpenings( const size_t beg, const size_t end) {
    using namespace ChIAss::Opening;

	if ( end - beg > 1 ) {
		size_t cursor = beg ;
		OpeningPosition pivot;
		pivot = ChIAss_openings[end - 1 ];

		for (size_t i = beg; i < end - 1; i++) {
			if ( ChIAss_openings[i].h < pivot.h ) {
				OpeningPosition temporary_1 = ChIAss_openings[i];
				ChIAss_openings[i] =  ChIAss_openings[cursor];
				ChIAss_openings[cursor] = temporary_1;

				cursor ++;
			}
/*            else if (ChIAss_openings[i].h < pivot.h) {
                printf("Error : we got two times the same hash... %zu == %zu\n", ChIAss_openings[i].h, pivot.h);
            }*/
		}
		OpeningPosition temporary_1 = ChIAss_openings[end - 1];
		ChIAss_openings[end - 1] =  ChIAss_openings[cursor];
		ChIAss_openings[cursor] = temporary_1;

		SortOpenings( beg, cursor );
		SortOpenings( cursor + 1, end );
	}
}

int ChIAss::Opening::LoadOpening(char* filename) {
    int fscanf_return=0;
    FILE* opening_file = fopen(filename, "r");
    if (opening_file == NULL) {
        printf("Can't open file : %s\n", filename);
        return -1;
    }

    Chess::Board b;

    u32 proba;
    char letter;
    char number;
    Chess::Move m;
    while (FENToBoard_file(opening_file, &b)) {
        n_opening_boards ++;
        ChIAss_openings = (OpeningPosition*) realloc(ChIAss_openings, n_opening_boards*sizeof(OpeningPosition));


        ChIAss_openings[n_opening_boards-1].h = Hash(b);
        ChIAss_openings[n_opening_boards-1].n_lines = 0;
        ChIAss_openings[n_opening_boards-1].lines = NULL;
        ChIAss_openings[n_opening_boards-1].probas = NULL;

        // Get the lines 
        fscanf_return = fscanf(opening_file, " [%u (%c%c", &proba, &letter, &number);
        do {
            m.beg = (Chess::square) (letter-97 + (number - 49)*8);
            fscanf_return = fscanf(opening_file, " %c%c)", &letter, &number);
            m.end = (Chess::square) (letter-97 + (number - 49)*8);
            Chess::ConvertMove(m, b);

            ChIAss_openings[n_opening_boards-1].n_lines ++;
            ChIAss_openings[n_opening_boards-1].lines = (Chess::Move*) realloc( ChIAss_openings[n_opening_boards-1].lines, ChIAss_openings[n_opening_boards-1].n_lines*sizeof(Chess::Move));
            ChIAss_openings[n_opening_boards-1].probas = (int*) realloc( ChIAss_openings[n_opening_boards-1].probas, ChIAss_openings[n_opening_boards-1].n_lines*sizeof(int));
            ChIAss_openings[n_opening_boards-1].lines[ChIAss_openings[n_opening_boards-1].n_lines-1] = m;
            ChIAss_openings[n_opening_boards-1].probas[ChIAss_openings[n_opening_boards-1].n_lines-1] = proba;
        }
        while (fscanf(opening_file, ", %u (%c%c", &proba, &letter, &number) == 3);
        fscanf_return = fscanf(opening_file, "]");
        fscanf_return = fscanf(opening_file, " #%*[^\n]\n");
    }

    //  We sort the openings 
    SortOpenings(0, n_opening_boards);

    fclose(opening_file);
    fscanf_return = 0;
    return fscanf_return; // ignore the unused result warning
}