#include "engine.hpp"
#include "GUI.hpp"

/*#include <chrono>
std::chrono::steady_clock::time_point beg;
std::chrono::steady_clock::time_point end;*/

ufast8 depth=6;
i8 victory = -1;

int main(void) {
    ChIAss::Chess::Board the_board;
    ChIAss::GUI::ChIAssMoveData pdata;
    pdata._depth = 6;
    pdata._ChIAss_colour = ChIAss::Chess::WHITE;
    pdata._played = {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square,ChIAss::Chess::NORMAL_MOVE};
    pdata.ChIAss_is_playing = false;
    pdata._last_move_played = {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square,ChIAss::Chess::NORMAL_MOVE};
    pdata._board = &the_board;

    ChIAss::Chess::EmptyBoard( pdata._board );
    if ( ChIAss::GUI::InitInterface() != 0 ) {
        exit(EXIT_FAILURE);
    }

    ChIAss::Chess::colour ChIAss_colour = ChIAss::Chess::WHITE;

    ChIAss::GUI::DisplayBoard(*(pdata._board), ChIAss::Chess::WHITE);

    SDL_Thread *second_thread = NULL;

    SDL_Event event;
    SDL_PollEvent(&event);
    while (1) {
        if (SDL_PollEvent(&event)) {// if an event is detected
            switch (event.type) { // Depending on the event
                case SDL_QUIT: // If the window has been closed
                    ChIAss::GUI::CloseInterface();
                    SDL_WaitThread( second_thread, NULL );
                    return 0;
                
                case SDL_KEYDOWN:
                    if ( event.key.repeat != 1 ) {
                        switch (event.key.keysym.scancode) {
                            case SDL_SCANCODE_S:// Setting the position, then ChIAss plays
                                if ( !pdata.ChIAss_is_playing ) {
                                    if (pdata._played != (ChIAss::Chess::Move) {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square}) {
                                        ChIAss::GUI::UnhighlightSquare(pdata._played.beg, ChIAss_colour);
                                        ChIAss::GUI::UnhighlightSquare(pdata._played.end, ChIAss_colour);
                                    }
                                    int what_to_do = ChIAss::GUI::SetPosition_GUI(pdata._board, &pdata._ChIAss_colour, &pdata._last_move_played);
                                    if ( -1 == what_to_do ) {
                                        ChIAss::GUI::CloseInterface();
                                        return 0;
                                    }

//  TODO : TransformToRealBoard : starting_pawns+check for king
                                    if ( pdata._last_move_played.beg == ChIAss::Chess::not_a_square ) {
                                        pdata._last_move_played = {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square,ChIAss::Chess::NORMAL_MOVE};
                                    }
                                    pdata._played = {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square,ChIAss::Chess::NORMAL_MOVE};
                                    pdata.ChIAss_is_playing = true;
                                    if (what_to_do == 1) {
                                        second_thread = SDL_CreateThread( ChIAss::GUI::ChIAssMove_GUI, "ChIAssMove", &pdata );
                                    } else {
                                        second_thread = SDL_CreateThread( ChIAss::GUI::ChIAssAnalytics_GUI, "ChIAssAnalytics", &pdata );
                                    }
                                }
                            break;
                            default:
                                continue;
                        }
                    }
                break;
                
            }
        }
        SDL_Delay(10);
    }

    ChIAss::GUI::CloseInterface();
    return 0;
}