#include "engine.hpp"

void ChIAss::Engine::TakeCheckingPiece(Chess::Board* b, const Chess::PieceAndPosition king, const u8 square_checking,
                std::vector<Chess::Move>& returned) {

    ufast8 current_square;
    Chess::Move added_move;
    Chess::PieceAndPosition tempo;
    unsigned char is_pinned;

    added_move.end = (Chess::square) square_checking;
    added_move.type = Chess::NORMAL_MOVE;

    //  horizontal -
    current_square = square_checking-1;
    while (current_square%8 != 7) {
        if ( b->B[current_square].piece != Chess::EMPTY ) {
            if ( b->B[current_square].colour == king.colour ) {
                if ( b->B[current_square].piece == Chess::ROOK || 
                b->B[current_square].piece == Chess::QUEEN ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 3 ) {
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                    }
                } else if ( b->B[current_square].piece == Chess::ROOK_CAN_CASTLE ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 3 ) {
                        added_move.type = Chess::FIRST_MOVE;
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                        added_move.type = Chess::NORMAL_MOVE;
                    }
                }
            }
            break;
        }
        current_square --;
    }

    //  horizontal +
    current_square = square_checking+1;
    while (current_square%8 != 0) {
        if ( b->B[current_square].piece != Chess::EMPTY ) {
            if ( b->B[current_square].colour == king.colour ) {
                if ( b->B[current_square].piece == Chess::ROOK || 
                b->B[current_square].piece == Chess::QUEEN ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 3 ) {
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                    }
                } else if ( b->B[current_square].piece == Chess::ROOK_CAN_CASTLE ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 3 ) {
                        added_move.type = Chess::FIRST_MOVE;
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                        added_move.type = Chess::NORMAL_MOVE;
                    }
                }
            }
            break;
        }
        current_square ++;
    }



    //  vertical -
    current_square = square_checking-8;
    while (current_square < 64) {
        if ( b->B[current_square].piece != Chess::EMPTY ) {
            if ( b->B[current_square].colour == king.colour ) {
                if ( b->B[current_square].piece == Chess::ROOK || 
                b->B[current_square].piece == Chess::QUEEN ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 2 ) {
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                    }
                } else if ( b->B[current_square].piece == Chess::ROOK_CAN_CASTLE ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 2 ) {
                        added_move.type = Chess::FIRST_MOVE;
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                        added_move.type = Chess::NORMAL_MOVE;
                    }
                }
            }
            break;
        }
        current_square -= 8;
    }

    //  vertical +
    current_square = square_checking+8;
    while (current_square < 64) {
        if ( b->B[current_square].piece != Chess::EMPTY ) {
            if ( b->B[current_square].colour == king.colour ) {
                if ( b->B[current_square].piece == Chess::ROOK || 
                b->B[current_square].piece == Chess::QUEEN ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 2 ) {
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                    }
                } else if ( b->B[current_square].piece == Chess::ROOK_CAN_CASTLE ) {
                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                            b->B[current_square].colour, (Chess::square) current_square};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 || is_pinned == 2 ) {
                        added_move.type = Chess::FIRST_MOVE;
                        added_move.beg = (Chess::square) current_square;
                        returned.push_back(added_move);
                        added_move.type = Chess::NORMAL_MOVE;
                    }
                }
            }
            break;
        }
        current_square += 8;
    }



    //  diagonal up left 
    current_square = square_checking+7;
    if ( king.colour == Chess::WHITE ) {
        while (current_square<64 && current_square%8 != 7) {
            if ( b->B[current_square].piece != Chess::EMPTY ) {
                if ( b->B[current_square].colour == king.colour ) {
                    if ( b->B[current_square].piece == Chess::BISHOP || 
                    b->B[current_square].piece == Chess::QUEEN ) {
                        tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                b->B[current_square].colour, (Chess::square) current_square};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 || is_pinned == 4 ) {
                            added_move.beg = (Chess::square) current_square;
                            returned.push_back(added_move);
                        }
                    }
                }
                break;
            }
            current_square += 7;
        }
    } else {
        if (current_square<64 && current_square%8 != 7) {
            if (b->B[current_square] == (Chess::Piece) {Chess::PAWN,Chess::BLACK}) {
                tempo = (Chess::PieceAndPosition) {Chess::PAWN, Chess::BLACK, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 4 ) {
                    added_move.beg = (Chess::square) current_square;
                    returned.push_back(added_move);
                }
            } else if (b->B[current_square] == (Chess::Piece) {Chess::STARTING_PAWN,Chess::BLACK}) {
                tempo = (Chess::PieceAndPosition) {Chess::PAWN, Chess::BLACK, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 4 ) {
                    added_move.beg = (Chess::square) current_square;
                    added_move.type = Chess::FIRST_MOVE;
                    returned.push_back(added_move);
                    added_move.type = Chess::NORMAL_MOVE;
                }
            } else {
                while (current_square<64 && current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::BISHOP || 
                            b->B[current_square].piece == Chess::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 4 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square += 7;
                }
            }
        }
    }

    //  diagonal down right 
    current_square = square_checking-7;
    if (king.colour == Chess::BLACK ) {
        while (current_square<64 && current_square%8 != 0) {
            if ( b->B[current_square].piece != Chess::EMPTY ) {
                if ( b->B[current_square].colour == king.colour ) {
                    if ( b->B[current_square].piece == Chess::BISHOP || 
                    b->B[current_square].piece == Chess::QUEEN ) {
                        tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                b->B[current_square].colour, (Chess::square) current_square};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 || is_pinned == 4 ) {
                            added_move.beg = (Chess::square) current_square;
                            returned.push_back(added_move);
                        }
                    }
                }
                break;
            }
            current_square -= 7;
        }
    } else {
        if (current_square<64 && current_square%8 != 0) {
            if (b->B[current_square] == (Chess::Piece) {Chess::PAWN,Chess::WHITE}) {
                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                        b->B[current_square].colour, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 4 ) {
                    added_move.beg = (Chess::square) current_square;
                    returned.push_back(added_move);
                }
            } else if (b->B[current_square] == (Chess::Piece) {Chess::STARTING_PAWN,Chess::WHITE}) {
                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                        b->B[current_square].colour, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 4 ) {
                    added_move.beg = (Chess::square) current_square;
                    added_move.type = Chess::FIRST_MOVE;
                    returned.push_back(added_move);
                    added_move.type = Chess::NORMAL_MOVE;
                }
            } else {
                while (current_square<64 && current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::BISHOP || 
                            b->B[current_square].piece == Chess::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 4 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square -= 7;
                }
            }
        }
    }



    //  diagonal up right 
    current_square = square_checking+9;
    if (king.colour == Chess::WHITE ) {
        while (current_square<64 && current_square%8 != 0) {
            if ( b->B[current_square].piece != Chess::EMPTY ) {
                if ( b->B[current_square].colour == king.colour ) {
                    if ( b->B[current_square].piece == Chess::BISHOP || 
                    b->B[current_square].piece == Chess::QUEEN ) {
                        tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                b->B[current_square].colour, (Chess::square) current_square};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 || is_pinned == 5 ) {
                            added_move.beg = (Chess::square) current_square;
                            returned.push_back(added_move);
                        }
                    }
                }
                break;
            }
            current_square += 9;
        }
    } else {
        if (current_square<64 && current_square%8 != 0) {
            if (b->B[current_square] == (Chess::Piece) {Chess::PAWN,Chess::BLACK}) {
                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                        b->B[current_square].colour, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 5 ) {
                    added_move.beg = (Chess::square) current_square;
                    returned.push_back(added_move);
                }
            } else if (b->B[current_square] == (Chess::Piece) {Chess::STARTING_PAWN,Chess::BLACK}) {
                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                        b->B[current_square].colour, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 5 ) {
                    added_move.beg = (Chess::square) current_square;
                    added_move.type = Chess::FIRST_MOVE;
                    returned.push_back(added_move);
                    added_move.type = Chess::NORMAL_MOVE;
                }
            } else {
                while (current_square<64 && current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::BISHOP || 
                            b->B[current_square].piece == Chess::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 5 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square += 9;
                }
            }
        }
    }

    //  diagonal down left 
    current_square = square_checking-9;
    if (king.colour == Chess::BLACK ) {
        while (current_square<64 && current_square%8 != 7) {
            if ( b->B[current_square].piece != Chess::EMPTY ) {
                if ( b->B[current_square].colour == king.colour ) {
                    if ( b->B[current_square].piece == Chess::BISHOP || 
                    b->B[current_square].piece == Chess::QUEEN ) {
                        tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                b->B[current_square].colour, (Chess::square) current_square};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 || is_pinned == 5 ) {
                            added_move.beg = (Chess::square) current_square;
                            returned.push_back(added_move);
                        }
                    }
                }
                break;
            }
            current_square -= 9;
        }
    } else {
        if (current_square<64 && current_square%8 != 7) {
            if (b->B[current_square] == (Chess::Piece) {Chess::PAWN,Chess::WHITE}) {
                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                        b->B[current_square].colour, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 5 ) {
                    added_move.beg = (Chess::square) current_square;
                    returned.push_back(added_move);
                }
            } else if (b->B[current_square] == (Chess::Piece) {Chess::STARTING_PAWN,Chess::WHITE}) {
                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                        b->B[current_square].colour, (Chess::square) current_square};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 || is_pinned == 5 ) {
                    added_move.beg = (Chess::square) current_square;
                    added_move.type = Chess::FIRST_MOVE;
                    returned.push_back(added_move);
                    added_move.type = Chess::NORMAL_MOVE;
                }
            } else {
                while (current_square<64 && current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::BISHOP || 
                            b->B[current_square].piece == Chess::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 5 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square -= 9;
                }
            }
        }
    }





    // knight presence
    if ( square_checking%8 > 1 ) {
        if ( square_checking < 48 ) {
            if ( b->B[ square_checking+6 ].piece == Chess::KNIGHT && b->B[ square_checking+6 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+6].piece, 
                        b->B[square_checking+6].colour, (Chess::square) (square_checking+6)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+6);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking+15 ].piece == Chess::KNIGHT && b->B[ square_checking+15 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+15].piece, 
                        b->B[square_checking+15].colour, (Chess::square) (square_checking+15)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+15);
                    returned.push_back(added_move);
                }
            }

            if ( square_checking%8 < 6 ) {
                if ( b->B[ square_checking+17 ].piece == Chess::KNIGHT && b->B[ square_checking+17 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking+17].piece, 
                            b->B[square_checking+17].colour, (Chess::square) (square_checking+17)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking+17);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking+10 ].piece == Chess::KNIGHT && b->B[ square_checking+10 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking+10].piece, 
                            b->B[square_checking+10].colour, (Chess::square) (square_checking+10)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking+10);
                        returned.push_back(added_move);
                    }
                }

                if ( square_checking > 15 ) {
                    if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                                b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                                b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-15);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                                b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                                b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-10);
                            returned.push_back(added_move);
                        }
                    }

                } else if ( square_checking > 7) {// square_checking <= 15
                    if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                                b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                                b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-10);
                            returned.push_back(added_move);
                        }
                    }
                }

            } else if ( square_checking%8 == 6 ) {
                if ( b->B[ square_checking+17 ].piece == Chess::KNIGHT && b->B[ square_checking+17 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking+17].piece, 
                            b->B[square_checking+17].colour, (Chess::square) (square_checking+17)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking+17);
                        returned.push_back(added_move);
                    }
                }

                if ( square_checking > 15 ) {
                    if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                                b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-15);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                                b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                                b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-10);
                            returned.push_back(added_move);
                        }
                    }

                } else if ( square_checking > 7) {// square_checking <= 15
                    if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                                b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-10);
                            returned.push_back(added_move);
                        }
                    }
                }
            } else {// square_checking%8 == 7
                if ( square_checking > 15 ) {
                    if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                                b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                                b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-10);
                            returned.push_back(added_move);
                        }
                    }

                } else if ( square_checking > 7) {// square_checking <= 15
                    if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                                b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (square_checking-10);
                            returned.push_back(added_move);
                        }
                    }
                }
            }
        } else if ( square_checking < 56 ) {// square_checking >= 48 so square_checking > 15
            if ( b->B[ square_checking+6 ].piece == Chess::KNIGHT && b->B[ square_checking+6 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+6].piece, 
                        b->B[square_checking+6].colour, (Chess::square) (square_checking+6)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+6);
                    returned.push_back(added_move);
                }
            }

            if ( square_checking%8 < 6 ) {
                if ( b->B[ square_checking+10 ].piece == Chess::KNIGHT && b->B[ square_checking+10 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking+10].piece, 
                            b->B[square_checking+10].colour, (Chess::square) (square_checking+10)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking+10);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                            b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-6);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                            b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-15);
                        returned.push_back(added_move);
                    }
                }
            } else if (square_checking%8 == 6) {
                if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                            b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-15);
                        returned.push_back(added_move);
                    }
                }
            }// square_checking% == 7
            if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                        b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-17);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                        b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-10);
                    returned.push_back(added_move);
                }
            }
        } else {// square_checking >= 56 so square_checking > 15
            if ( square_checking%8 < 6 ) {
                if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                            b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-6);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                            b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-15);
                        returned.push_back(added_move);
                    }
                }
            } else if ( square_checking%8 == 6 ) {
                if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                            b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-15);
                        returned.push_back(added_move);
                    }
                }
            }
            if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                        b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-17);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking-10 ].piece == Chess::KNIGHT && b->B[ square_checking-10 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-10].piece, 
                        b->B[square_checking-10].colour, (Chess::square) (square_checking-10)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-10);
                    returned.push_back(added_move);
                }
            }
        }
    } else if ( square_checking%8 == 1 ) {// so square_checking%8 < 6
        if ( square_checking < 48 ) {
            if ( b->B[ square_checking+15 ].piece == Chess::KNIGHT && b->B[ square_checking+15 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+15].piece, 
                        b->B[square_checking+15].colour, (Chess::square) (square_checking+15)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+15);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking+17 ].piece == Chess::KNIGHT && b->B[ square_checking+17 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+17].piece, 
                        b->B[square_checking+17].colour, (Chess::square) (square_checking+17)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+17);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking+10 ].piece == Chess::KNIGHT && b->B[ square_checking+10 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+10].piece, 
                        b->B[square_checking+10].colour, (Chess::square) (square_checking+10)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+10);
                    returned.push_back(added_move);
                }
            }
            
            if ( square_checking > 15 ) {
                if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                            b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-6);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                            b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-15);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                            b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-17);
                        returned.push_back(added_move);
                    }
                }
            } else if ( square_checking > 7 ) {
                if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                            b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-6);
                        returned.push_back(added_move);
                    }
                }
            }

        } else {
            if ( square_checking < 56 ) {// square_checking >= 48 and square_checking < 56
                if ( b->B[ square_checking+10 ].piece == Chess::KNIGHT && b->B[ square_checking+10 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking+10].piece, 
                            b->B[square_checking+10].colour, (Chess::square) (square_checking+10)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking+10);
                        returned.push_back(added_move);
                    }
                }
            }
            if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                        b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-6);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                        b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-15);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking-17 ].piece == Chess::KNIGHT && b->B[ square_checking-17 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-17].piece, 
                        b->B[square_checking-17].colour, (Chess::square) (square_checking-17)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-17);
                    returned.push_back(added_move);
                }
            }
        }
    } else {// square_checking%8 == 0
        if ( square_checking < 48 ) {
            if ( b->B[ square_checking+17 ].piece == Chess::KNIGHT && b->B[ square_checking+17 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+17].piece, 
                        b->B[square_checking+17].colour, (Chess::square) (square_checking+17)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+17);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking+10 ].piece == Chess::KNIGHT && b->B[ square_checking+10 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking+10].piece, 
                        b->B[square_checking+10].colour, (Chess::square) (square_checking+10)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking+10);
                    returned.push_back(added_move);
                }
            }
            if ( square_checking > 15 ) {
                if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                            b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-6);
                        returned.push_back(added_move);
                    }
                }
                if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                            b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-15);
                        returned.push_back(added_move);
                    }
                }
            } else if ( square_checking > 7 ) {
                if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                            b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking-6);
                        returned.push_back(added_move);
                    }
                }
            }
        } else {
            if (square_checking < 56) {
                if ( b->B[ square_checking+10 ].piece == Chess::KNIGHT && b->B[ square_checking+10 ].colour != b->B[square_checking].colour ) {
                    tempo = (Chess::PieceAndPosition) {b->B[square_checking+10].piece, 
                            b->B[square_checking+10].colour, (Chess::square) (square_checking+10)};
                    is_pinned = IsPinnedDown(b, tempo, king);
                    if ( is_pinned == 1 ) {
                        added_move.beg = (Chess::square) (square_checking+10);
                        returned.push_back(added_move);
                    }
                }
            }
            if ( b->B[ square_checking-6 ].piece == Chess::KNIGHT && b->B[ square_checking-6 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-6].piece, 
                        b->B[square_checking-6].colour, (Chess::square) (square_checking-6)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-6);
                    returned.push_back(added_move);
                }
            }
            if ( b->B[ square_checking-15 ].piece == Chess::KNIGHT && b->B[ square_checking-15 ].colour != b->B[square_checking].colour ) {
                tempo = (Chess::PieceAndPosition) {b->B[square_checking-15].piece, 
                        b->B[square_checking-15].colour, (Chess::square) (square_checking-15)};
                is_pinned = IsPinnedDown(b, tempo, king);
                if ( is_pinned == 1 ) {
                    added_move.beg = (Chess::square) (square_checking-15);
                    returned.push_back(added_move);
                }
            }
        }
    }
}