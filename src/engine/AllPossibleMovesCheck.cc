#include "engine.hpp"

// first, get all the moves for the king, THEN, get the rest of the moves and check if they're valid
std::vector<ChIAss::Chess::Move> ChIAss::Engine::AllPossibleMovesCheck( Chess::Board* b, const std::vector<Chess::PieceAndPosition>& list_pieces,
            const Chess::Move last_move_played, const Chess::PieceAndPosition king, const u8 result ) {
	std::vector<Chess::Move> returned;
	std::vector<Chess::Move> all_moves;

    if (result < 64) {//    A (single) piece (not a knight) is checking the king, from that square
        AllMovesKing(b, king, returned);
        if (king.piece == Chess::KING_CAN_CASTLE) {
            for (size_t i=0; i<returned.size(); i++) {
                returned[i].type = Chess::FIRST_MOVE;
            }
        }
        BlockLineOfSight(b, king, result, returned);
        TakeCheckingPiece(b, king, result, returned);

    } else {
        AllMovesKing(b, king, returned);
        if (king.piece == Chess::KING_CAN_CASTLE) {
            for (size_t i=0; i<returned.size(); i++) {
                returned[i].type = Chess::FIRST_MOVE;
            }
        }
        if ( result != 200 ) {// a knight checks, you can take it, otherwise it's 2 distinct pieces checking
            TakeCheckingPiece(b, king, result-100, returned);
        }
    }
    return returned;
}