#include "engine.hpp"

void ChIAss::Engine::AllMovesKing(Chess::Board* b, const Chess::PieceAndPosition p, 
            std::vector<Chess::Move>& returned_moves) {
    Chess::Move added_move;
    added_move.type=Chess::type_of_move::NORMAL_MOVE;
    added_move.beg=p.position;
	Chess::Board temporary = *b;
    Chess::Piece taken;

    if ( p.position%8 > 0 ) {
        if ( b->B[ p.position - 1 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 1 ].colour != p.colour ) {
            added_move.end =  (Chess::square) ( p.position - 1 );
            Chess::Play(&temporary, added_move, &taken);
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                returned_moves.push_back( added_move );
            }
            Chess::Unplay(&temporary, added_move, taken);
        }
        if ( p.position < 56 ) {// %8 > 0    < 56
            if ( b->B[ p.position + 7 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 7 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position + 7 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
            if ( b->B[ p.position + 8 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 8 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position + 8 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
            if ( p.position%8 < 7 ) {// %8 > 0    %8 < 7     < 56
                if ( b->B[ p.position + 9 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 9 ].colour != p.colour ) {
                    added_move.end =  (Chess::square) ( p.position + 9 );
                    Chess::Play(&temporary, added_move, &taken);
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( added_move );
                    }
                    Chess::Unplay(&temporary, added_move, taken);
                }
                if ( b->B[ p.position + 1 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
                    added_move.end =  (Chess::square) ( p.position + 1 );
                    Chess::Play(&temporary, added_move, &taken);
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( added_move );
                    }
                    Chess::Unplay(&temporary, added_move, taken);
                }
                if ( p.position > 7 ) {// %8 > 0    %8 < 7     < 56    > 7
                    if ( b->B[ p.position - 7 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
                        added_move.end =  (Chess::square) ( p.position - 7 );
                        Chess::Play(&temporary, added_move, &taken);
                        if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                            returned_moves.push_back( added_move );
                        }
                        Chess::Unplay(&temporary, added_move, taken);
                    }
                    if ( b->B[ p.position - 8 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
                        added_move.end =  (Chess::square) ( p.position - 8 );
                        Chess::Play(&temporary, added_move, &taken);
                        if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                            returned_moves.push_back( added_move );
                        }
                        Chess::Unplay(&temporary, added_move, taken);
                    }
                    if ( b->B[ p.position - 9 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 9 ].colour != p.colour ) {
                        added_move.end =  (Chess::square) ( p.position - 9 );
                        Chess::Play(&temporary, added_move, &taken);
                        if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                            returned_moves.push_back( added_move );
                        }
                        Chess::Unplay(&temporary, added_move, taken);
                    }
                }
            } else {// %8 > 0    %8==7    < 56
                if ( p.position > 7 ) {// %8 > 0    %8==7     < 56    > 7
                    if ( b->B[ p.position - 8 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
                        added_move.end =  (Chess::square) ( p.position - 8 );
                        Chess::Play(&temporary, added_move, &taken);
                        if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                            returned_moves.push_back( added_move );
                        }
                        Chess::Unplay(&temporary, added_move, taken);
                    }
                    if ( b->B[ p.position - 9 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 9 ].colour != p.colour ) {
                        added_move.end =  (Chess::square) ( p.position - 9 );
                        Chess::Play(&temporary, added_move, &taken);
                        if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                            returned_moves.push_back( added_move );
                        }
                        Chess::Unplay(&temporary, added_move, taken);
                    }
                }
            }
        } else {// %8 > 0    >=56
            if ( p.position%8 < 7 ) {// %8 > 0    %8 < 7     >=56
                if ( b->B[ p.position + 1 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
                    added_move.end =  (Chess::square) ( p.position + 1 );
                    Chess::Play(&temporary, added_move, &taken);
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( added_move );
                    }
                    Chess::Unplay(&temporary, added_move, taken);
                }
                if ( b->B[ p.position - 7 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
                    added_move.end =  (Chess::square) ( p.position - 7 );
                    Chess::Play(&temporary, added_move, &taken);
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( added_move );
                    }
                    Chess::Unplay(&temporary, added_move, taken);
                }
            }
            if ( b->B[ p.position - 8 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position - 8 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
            if ( b->B[ p.position - 9 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 9 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position - 9 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
        }
    } else {//  %8<=0
        if ( p.position < 56 ) {// %8<=0    < 56
            if ( b->B[ p.position + 8 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 8 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position + 8 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
            if ( b->B[ p.position + 9 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 9 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position + 9 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
        }
        if ( b->B[ p.position + 1 ].piece == Chess::type_value::EMPTY || b->B[ p.position + 1 ].colour != p.colour ) {
            added_move.end =  (Chess::square) ( p.position + 1 );
            Chess::Play(&temporary, added_move, &taken);
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                returned_moves.push_back( added_move );
            }
            Chess::Unplay(&temporary, added_move, taken);
        }
        if ( p.position > 7 ) {// %8<=0    > 7
            if ( b->B[ p.position - 8 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 8 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position - 8 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
            if ( b->B[ p.position - 7 ].piece == Chess::type_value::EMPTY || b->B[ p.position - 7 ].colour != p.colour ) {
                added_move.end =  (Chess::square) ( p.position - 7 );
                Chess::Play(&temporary, added_move, &taken);
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    returned_moves.push_back( added_move );
                }
                Chess::Unplay(&temporary, added_move, taken);
            }
        }
    }
}