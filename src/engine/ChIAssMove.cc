#include <engine.hpp>

ChIAss::Chess::Move ChIAss::Engine::ChIAssMove( Chess::Board* b, const Chess::colour col, 
                            const ufast8 expected_depth, const Chess::Move last_move_played ) {
    // We create the vectors of pieces
    std::vector<Chess::PieceAndPosition> ChIAss_pieces;
    ChIAss_pieces.reserve(16);
    std::vector<Chess::PieceAndPosition> opponent_pieces;
    opponent_pieces.reserve(16);
    for ( ufast8 i = 0 ; i < 64 ; i++ ) {
        Chess::PieceAndPosition added_piece;
		if ( b->B[i].piece != Chess::EMPTY ) {
            added_piece.piece = b->B[i].piece;
            added_piece.colour = b->B[i].colour;
            added_piece.position = (Chess::square) i;
            if ( b->B[i].colour == col ) {
                ChIAss_pieces.push_back( added_piece );
            } else {
                opponent_pieces.push_back( added_piece );
            }
            // the king has to be in first place in the vector of pieces
            if ( b->B[i].piece >= Chess::KING_CAN_CASTLE ) {
                Chess::PieceAndPosition temporary;
                if ( b->B[i].colour == col ) {
                    temporary = ChIAss_pieces[0];
                    ChIAss_pieces[0] = ChIAss_pieces.back();
                    ChIAss_pieces.back() = temporary;
                } else {
                    temporary = opponent_pieces[0];
                    opponent_pieces[0] = opponent_pieces.back();
                    opponent_pieces.back() = temporary;
                }
            }
        }
    }

//  ========== shared variables ==========
    std::vector<Chess::Move> all_possible_moves;
    ufast8 result_check;
    all_possible_moves = Engine::Moves( b, ChIAss_pieces, last_move_played, &result_check );

    if ( all_possible_moves.size() == 0 ) {
        return {Chess::not_a_square,Chess::not_a_square,Chess::NORMAL_MOVE};
    }
    fast16 best_value;
    if ( col == Chess::WHITE ) {
        best_value = -INFINITY_CHIASS;
    } else {
        best_value = INFINITY_CHIASS;
    }
    Chess::Move best_move;

//  Just in case there's only 1 move possible, then a single thread is enough
    #pragma omp parallel num_threads(std::min((int)omp_get_max_threads(), (int)all_possible_moves.size()))
    {
    //  ========== thread-specific variables ==========
        int number_of_threads = omp_get_num_threads();
        int id_of_thread = omp_get_thread_num();
        fast16 current_value;
        Chess::Piece taken;// The piece that's been taken, if ever, or {EMPTY, WHITE}, used to unplay the move
        Chess::Board thread_board;
        std::vector<Chess::PieceAndPosition> thread_ChIAss_pieces;
        std::vector<Chess::PieceAndPosition> thread_opponent_pieces;
        for ( ufast8 i = 0 ; i < 64 ; i++ ) {
            thread_board.B[i] = b->B[i];
            Chess::PieceAndPosition thread_added_piece;
            if ( b->B[i].piece != Chess::EMPTY ) {
                thread_added_piece.piece = b->B[i].piece;
                thread_added_piece.colour = b->B[i].colour;
                thread_added_piece.position = (Chess::square) i;
                if ( b->B[i].colour == col ) {
                    thread_ChIAss_pieces.push_back( thread_added_piece );
                } else {
                    thread_opponent_pieces.push_back( thread_added_piece );
                }
                // the king has to be in first place in the vector of pieces
                if ( b->B[i].piece >= Chess::KING_CAN_CASTLE ) {
                    Chess::PieceAndPosition temporary;
                    if ( b->B[i].colour == col ) {
                        temporary = thread_ChIAss_pieces[0];
                        thread_ChIAss_pieces[0] = thread_ChIAss_pieces.back();
                        thread_ChIAss_pieces.back() = temporary;
                    } else {
                        temporary = thread_opponent_pieces[0];
                        thread_opponent_pieces[0] = thread_opponent_pieces.back();
                        thread_opponent_pieces.back() = temporary;
                    }
                }
            }
        }


        if ( col == Chess::WHITE ) {
    //  ======================================== ChIAss plays WHITE ===================================
            Chess::Play( &thread_board , all_possible_moves[id_of_thread] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);
            

            
            current_value = Engine::ValueMove( &thread_board, 1, expected_depth, 
                                            thread_opponent_pieces, thread_ChIAss_pieces, 
                                            all_possible_moves[id_of_thread] , best_value );

            Chess::Unplay( &thread_board, all_possible_moves[id_of_thread], taken, thread_ChIAss_pieces, thread_opponent_pieces);
            
            #pragma omp critical
            {
                if ( current_value > best_value ) {
                    best_move = all_possible_moves[id_of_thread];
                    best_value=current_value;
                }
            }


            
            #pragma omp for
            for ( ufast8 i=number_of_threads ; i<(int)all_possible_moves.size() ; i++ ) {
                Chess::Play( &thread_board , all_possible_moves[i] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);

                current_value = Engine::ValueMove( &thread_board, 1, expected_depth, 
                                                thread_opponent_pieces, thread_ChIAss_pieces, 
                                                all_possible_moves[i] , best_value );

                Chess::Unplay( &thread_board, all_possible_moves[i], taken, thread_ChIAss_pieces, thread_opponent_pieces);
                
                #pragma omp critical
                {
                    if ( current_value > best_value ) {
                        best_move = all_possible_moves[i];
                        best_value=current_value;
                    }
                }
            }



        } else {
    //  ======================================== ChIAss plays BLACK ===================================
            Chess::Play( &thread_board , all_possible_moves[id_of_thread] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);
            current_value = Engine::ValueMove( &thread_board, 1, expected_depth, 
                                            thread_opponent_pieces, thread_ChIAss_pieces, 
                                            all_possible_moves[id_of_thread] , best_value );
            Chess::Unplay( &thread_board, all_possible_moves[id_of_thread], taken, thread_ChIAss_pieces, thread_opponent_pieces);
            #pragma omp critical
            {
                if ( current_value < best_value ) {
                    best_move = all_possible_moves[id_of_thread];
                    best_value=current_value;
                }
            }



            #pragma omp for
            for ( ufast8 i=number_of_threads ; i<(int)all_possible_moves.size() ; i++ ) {
                Chess::Play( &thread_board , all_possible_moves[i] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);

                current_value = Engine::ValueMove( &thread_board, 1, expected_depth, 
                                                thread_opponent_pieces, thread_ChIAss_pieces, 
                                                all_possible_moves[i] , best_value );

                Chess::Unplay( &thread_board, all_possible_moves[i], taken, thread_ChIAss_pieces, thread_opponent_pieces);
                #pragma omp critical
                {
                    if ( current_value < best_value ) {
                        best_move = all_possible_moves[i];
                        best_value=current_value;
                    }
                }
            }
        }
    }
    return best_move;
}
