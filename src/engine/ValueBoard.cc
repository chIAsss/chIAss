#include "engine.hpp"

/*
 * Here the value of the board doesn't depend on which player's turn it is
 */
fast16 ChIAss::Engine::ValueBoard( Chess::Board* b , const std::vector<Chess::PieceAndPosition>& your_pieces, 
		const std::vector<Chess::PieceAndPosition>& oppo_pieces ) {
	fast16 your_sum=0;
	fast16 opponent_sum=0;
	for ( unsigned char i = 1 ; i < your_pieces.size() ; i++ ) {
		switch ( your_pieces[ i ].piece ) {
			case Chess::QUEEN:
				your_sum += 950;
				your_sum += 10*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 10*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case Chess::ROOK_CAN_CASTLE:
				your_sum -= 10;
			case Chess::ROOK:
				your_sum += 500;
				your_sum += 5*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 5*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case Chess::BISHOP:
				your_sum += 330;
				your_sum += 8*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 8*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case Chess::KNIGHT:
				your_sum += 300;
				your_sum += 7*std::min( 8 - your_pieces[ i ].position%8 , your_pieces[ i ].position%8);
				your_sum += 7*std::min( 8 - your_pieces[ i ].position/8 , your_pieces[ i ].position/8);
				break;
			case Chess::STARTING_PAWN:
			case Chess::PAWN:
				your_sum += 100;
			    if ( your_pieces[0].colour == Chess::WHITE ) {
					your_sum += 5*your_pieces[ i ].position/8;
				} else {
					your_sum += 5*(8 - your_pieces[ i ].position/8);
				}
				break;
			default:
				continue;
		}
	}
	for ( unsigned char i = 1 ; i < oppo_pieces.size() ; i++ ) {
		switch ( oppo_pieces[ i ].piece ) {
			case Chess::QUEEN:
				opponent_sum -= 950;
				opponent_sum -= 10*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				opponent_sum -= 10*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case Chess::ROOK_CAN_CASTLE:
				opponent_sum += 10;
			case Chess::ROOK:
				opponent_sum -= 500;
				opponent_sum -= 5*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				opponent_sum -= 5*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case Chess::BISHOP:
				opponent_sum -= 330;
				opponent_sum -= 8*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				opponent_sum -= 8*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case Chess::KNIGHT:
				opponent_sum -= 300;
				opponent_sum -= 7*std::min( 8 - oppo_pieces[ i ].position%8 , oppo_pieces[ i ].position%8);
				opponent_sum -= 7*std::min( 8 - oppo_pieces[ i ].position/8 , oppo_pieces[ i ].position/8);
				break;
			case Chess::STARTING_PAWN:
			case Chess::PAWN:
				opponent_sum -= 100;
			    if ( oppo_pieces[0].colour == Chess::WHITE ) {
					opponent_sum += 5*oppo_pieces[ i ].position/8;
				} else {
					opponent_sum += 5*(8 - oppo_pieces[ i ].position/8);
				}
				break;
			default:
				continue;
		}
	}
    if ( your_pieces[0].colour == Chess::WHITE ) {
    	return your_sum + opponent_sum;
    } else {
    	return - opponent_sum - your_sum;
    }
}