#include "engine.hpp"

void ChIAss::Engine::AllMovesBishop(Chess::Board* b, const Chess::PieceAndPosition p, 
		std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king) {
    Chess::Move added_move;
    added_move.type=Chess::type_of_move::NORMAL_MOVE;
    added_move.beg=p.position;
	ufast8 checking_square;

	ufast8 is_pinned = Engine::IsPinnedDown( b , p , king );
	switch ( is_pinned ) {
		case 1:// The bishop isn't pinned
		// Up left
			checking_square = (ufast8) ( p.position + 7 );
		// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
			while ( checking_square < 64 && checking_square%8 != 7 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );

				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square += 7;
			}
		// Up right
			checking_square = (ufast8) ( p.position + 9 );
		// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
			while ( checking_square < 64 && checking_square%8 != 0 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );
				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square ;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square += 9;
			}
		// Down right
			checking_square = (ufast8) ( p.position - 7 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
			while ( checking_square < 64 && checking_square%8 != 0 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );
				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square -= 7;
			}
		// Down left
			checking_square = (ufast8) ( p.position - 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
			while ( checking_square < 64 && checking_square%8 != 7 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );
				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square -= 9;
			}
		break;
		case 4:
// Up left
			checking_square = (u8) ( p.position + 7 );
// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
			while ( checking_square < 64 && checking_square%8 != 7 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );

				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square += 7;
			}
// Down right
			checking_square = (u8) ( p.position - 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
			while ( checking_square < 64 && checking_square%8 != 0 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );
				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square -= 7;
			}
		break;
		case 5:
// Up right
			checking_square = (u8) ( p.position + 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
			while ( checking_square < 64 && checking_square%8 != 0 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );
				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square ;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square += 9;
			}	
// Down left
			checking_square = (u8) ( p.position - 9 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
			while ( checking_square < 64 && checking_square%8 != 7 ) {
				if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
					added_move.end = (Chess::square) checking_square;
					returned_moves.push_back( added_move );
				} else {
					if ( b->B[ checking_square ].colour != p.colour ) {
						added_move.end = (Chess::square) checking_square;
						returned_moves.push_back( added_move );
					}
					break;
				}
				checking_square -= 9;
			}
		break;
		default:// the bishop is pinned from a straight line, it can't move
			break;
	}
}

