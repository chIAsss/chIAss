#include "engine.hpp"

void ChIAss::Engine::AllMovesQueen(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king) {
    Chess::Move added_move;
    added_move.type=Chess::type_of_move::NORMAL_MOVE;
    added_move.beg=p.position;
    ufast8 checking_square;

	ufast8 is_pinned = Engine::IsPinnedDown( b , p , king );
	switch ( is_pinned ) {
        case 1:// The queen isn't pinned

            checking_square = (ufast8) ( p.position + 8 );
        // as long as the case is in the board : not too high
            while ( checking_square < 64 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square += 8;
            }

            checking_square = (ufast8) ( p.position - 8 ) ;
        // as long as the case is in the board : not too low ( which would make it appear too high, since it's unsigned, you go from 6 to 253 )
            while ( checking_square < 64 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square -= 8;
            }

            checking_square = (ufast8) ( p.position + 1 ) ;
        // as long as the case is in the board : not too much on the right ( which would make it pop on the left )
            while ( checking_square%8 != 0 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square ++;
            }

            checking_square = (ufast8) ( p.position - 1 );
        // as long as the case is in the board : not too much on the left ( which would make it pop on the right )
            while ( checking_square%8 != 7 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square --;
            }

            checking_square = (ufast8) ( p.position + 7 );
        // as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
            while ( checking_square < 64 && checking_square%8 != 7 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square += 7;
            }

            checking_square = (ufast8) ( p.position + 9 );
        // as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
            while ( checking_square < 64 && checking_square%8 != 0 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square += 9;
            }

            checking_square = (ufast8) ( p.position - 7 );
        // as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
            while ( checking_square < 64 && checking_square%8 != 0 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square -= 7;
            }

            checking_square = (ufast8) ( p.position - 9 );
        // as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
            while ( checking_square < 64 && checking_square%8 != 7 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square -= 9;
            }
        break;
        case 2:// can only move up and down
            checking_square = (u8) ( p.position + 8 );
// UP : as long as the case is in the board : not too high
            while ( checking_square < 64 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square += 8;
            }
            checking_square = (u8) ( p.position - 8 ) ;
// DOWN : as long as the case is in the board : not too low ( which would make it appear too high, since it's unsigned, you go from 6 to 253 )
            while ( checking_square < 64 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square -= 8;
            }
        break;
        case 3:// can only move left and right
            added_move.beg = p.position;
// The piece is necessarily a queen or a rook
            checking_square = (u8) ( p.position + 1 ) ;
// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
            while ( checking_square%8 != 0 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square ++;
            }
            checking_square = (u8) ( p.position - 1 );
// as long as the case is in the board : not too much on the left ( which would make it pop on the right )
            while ( checking_square%8 != 7 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square --;
            }
        break;
        case 4:// can only move up left / down right
// Up left
            checking_square = (u8) ( p.position + 7 );
// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
            while ( checking_square < 64 && checking_square%8 != 7 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );

                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square += 7;
            }
// Down right
            checking_square = (u8) ( p.position - 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
            while ( checking_square < 64 && checking_square%8 != 0 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square -= 7;
            }
        break;
        case 5:// can only move up right / down left
// Up right
            checking_square = (u8) ( p.position + 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
            while ( checking_square < 64 && checking_square%8 != 0 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square ;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square += 9;
            }	
// Down left
            checking_square = (u8) ( p.position - 9 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
            while ( checking_square < 64 && checking_square%8 != 7 ) {
                if ( b->B[ checking_square ].piece == Chess::type_value::EMPTY ) {
                    added_move.end = (Chess::square) checking_square;
                    returned_moves.push_back( added_move );
                } else {
                    if ( b->B[ checking_square ].colour != p.colour ) {
                        added_move.end = (Chess::square) checking_square;
                        returned_moves.push_back( added_move );
                    }
                    break;
                }
                checking_square -= 9;
            }
        break;
        default: // for the warnings
            break;
    }
}