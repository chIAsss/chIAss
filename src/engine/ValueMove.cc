#include "engine.hpp"
#include <cstdlib>

fast16 ChIAss::Engine::ValueMove( Chess::Board* b, const ufast8 current_depth, const ufast8 expected_depth, 
std::vector<Chess::PieceAndPosition>& your_pieces, std::vector<Chess::PieceAndPosition>& opponent_pieces, 
const Chess::Move last_move_played , const fast16 parent_bound ) {
// ============================================= COMPUTING ALL MOVES ==================================================
    std::vector<Chess::Move> moves;
    ufast8 result_check;
    moves = Moves( b, your_pieces, last_move_played, &result_check );

// ============================================== CHECKING FOR MATE ===================================================
    if ( moves.size() == 0 ) {// No possibility : pat or checkmate
        if ( result_check == 255 ) {// PAT the great equalizer
            return 0;
        } else {
            if ( opponent_pieces[0].colour == Chess::BLACK ) {// that means black has mated, so it needs the lowest value
                return -INFINITY_CHIASS+current_depth;
            } else {
                return INFINITY_CHIASS-current_depth;
            }
        }
    }

    fast16 best_value;
    fast16 personnal_bound;

    if ( your_pieces[0].colour == Chess::WHITE ) {
        personnal_bound = -INFINITY_CHIASS;
    } else {
        personnal_bound = INFINITY_CHIASS;
    }
    fast16 current_value;
// The piece that's been taken, if ever, or {EMPTY, WHITE}, used to unplay the move
    Chess::Piece taken;

    if ( current_depth == expected_depth ) { // ==================== LEAF =============================================
        return ValueBoard( b, your_pieces, opponent_pieces );
    } else {// ============================================= NOT LEAF =================================================
// Play the first move, check if it's a mate, then check for the best value yet
// Do you have the update the your_pieces vector? Only if there's no check, to value the board

// First move =========================================================================================================
        // play the move ==============================================================================================
        
        Chess::Play( b , moves[0] , &taken, your_pieces, opponent_pieces);

        best_value = ValueMove( b, current_depth+1, expected_depth, 
                                        opponent_pieces, your_pieces, 
                                        moves[0] , personnal_bound );
        Chess::Unplay( b, moves[0], taken, your_pieces, opponent_pieces);
        // ============================================ BRANCH ESCAPING ===============================================
        if ( your_pieces[0].colour == Chess::WHITE ) {
            if ( best_value >= parent_bound ) { // We're over the bound : the opponent won't choose this branch, we escape it
                return best_value;// We escape this branch
            }
        } else {
            if ( best_value <= parent_bound  ) { // We're under the bound : the opponent won't choose this branch, we escape it
                return best_value;// We escape this branch
            }
        }
        personnal_bound = best_value;
// END First move =====================================================================================================

// Rest of the moves ==================================================================================================
        for ( ufast8 i = 1 ; i < moves.size() ; i++ ) {
            Chess::Play( b , moves[i] , &taken, your_pieces, opponent_pieces);
            current_value = ValueMove( b, current_depth+1, expected_depth, 
                                        opponent_pieces, your_pieces, 
                                        moves[i] , personnal_bound );

            Chess::Unplay( b, moves[i], taken, your_pieces, opponent_pieces);
        // ============================================ BRANCH ESCAPING ===============================================
            if ( your_pieces[0].colour == Chess::WHITE ) {
                if ( current_value > best_value ) {
// If the value is better than the better value yet, the opponent isn't going to choose this branch anyway
                    if ( current_value >= parent_bound ) { // We're over the bound : the opponent won't choose this branch
                        return current_value;// We escape this branch
                    }
                    best_value = current_value;
                    personnal_bound = best_value;    
                }
            } else {
                if ( current_value < best_value ) {
                    if ( current_value <= parent_bound ) { // We're under the bound : the opponent won't choose this branch
                        return current_value;// We escape this branch
                    }
                    best_value = current_value;
                    personnal_bound = best_value;
                }
            }
        }
// END Rest of the moves ==============================================================================================
    }
    return best_value;
}