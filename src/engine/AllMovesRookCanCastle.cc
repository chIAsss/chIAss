#include "engine.hpp"

void ChIAss::Engine::AllMovesRookCanCastle(Chess::Board* b, const Chess::PieceAndPosition p, 
            std::vector<Chess::Move>& returned_moves) {
//  Can never be pinned down
    Chess::Move added_move;
    added_move.type=Chess::FIRST_MOVE;
    added_move.beg=p.position;
    ufast8 checking_square;

//  TODO : can be improved : checking which rook it is (1 test) to evade 2 tests

    checking_square = (ufast8) ( p.position + 8 );
// as long as the case is in the board : not too high
    while ( checking_square < 64 ) {
        if ( b->B[ checking_square ].piece == Chess::EMPTY ) {
            added_move.end = (Chess::square) checking_square;
            returned_moves.push_back( added_move );
        } else {
            if ( b->B[ checking_square ].colour != p.colour ) {
                added_move.end = (Chess::square) checking_square;
                returned_moves.push_back( added_move );
            }
            break;
        }
        checking_square += 8;
    }
    checking_square = (ufast8) ( p.position - 8 ) ;
// as long as the case is in the board : not too low ( which would make it appear too high, since it's unsigned, you go from 6 to 253 )
    while ( checking_square < 64 ) {
        if ( b->B[ checking_square ].piece == Chess::EMPTY ) {
            added_move.end = (Chess::square) checking_square;
            returned_moves.push_back( added_move );
        } else {
            if ( b->B[ checking_square ].colour != p.colour ) {
                added_move.end = (Chess::square) checking_square;
                returned_moves.push_back( added_move );
            }
            break;
        }
        checking_square -= 8;
    }
    checking_square = (ufast8) ( p.position + 1 ) ;
// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
    while ( checking_square%8 != 0 ) {
        if ( b->B[ checking_square ].piece == Chess::EMPTY ) {
            added_move.end = (Chess::square) checking_square;
            returned_moves.push_back( added_move );
        } else {
            if ( b->B[ checking_square ].colour != p.colour ) {
                added_move.end = (Chess::square) checking_square;
                returned_moves.push_back( added_move );
            }
            break;
        }
        checking_square ++;
    }
    checking_square = (ufast8) ( p.position - 1 );
// as long as the case is in the board : not too much on the left ( which would make it pop on the right )
    while ( checking_square%8 != 7 ) {
        if ( b->B[ checking_square ].piece == Chess::EMPTY ) {
            added_move.end = (Chess::square) checking_square;
            returned_moves.push_back( added_move );
        } else {
            if ( b->B[ checking_square ].colour != p.colour ) {
                added_move.end = (Chess::square) checking_square;
                returned_moves.push_back( added_move );
            }
            break;
        }
        checking_square --;
    }
}