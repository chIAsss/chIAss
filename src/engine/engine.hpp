#ifndef CHIASS_HPP
#define CHIASS_HPP
#include "chess.hpp"
#include "opening.hpp"
#include <string>

#ifdef _OPENMP
#include <omp.h>
#endif

// This is used as a bound for checkmate and for the alpha-beta pruning
#define INFINITY_CHIASS 16000


namespace ChIAss {
        namespace Engine {
//  This variable stops the game when ChIAss wins (1) or loses (0), and stays at -1 otherwise
extern i8 victory;

bool Open(const Chess::Board board, Chess::Move* returned);



unsigned char IsPinnedDown( Chess::Board* b , const Chess::PieceAndPosition p , 
        const Chess::PieceAndPosition king );

void AllMovesStartingPawn(Chess::Board* b, const Chess::PieceAndPosition pawn, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king);
void AllMovesPawn(Chess::Board* b, const Chess::PieceAndPosition pawn, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king);
void AllMovesKnight(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king);
void AllMovesBishop(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king);
void AllMovesQueen(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king);
void AllMovesRook(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king);
void AllMovesRookCanCastle(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves);
void AllMovesKingCanCastle(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves);
void AllMovesKing(Chess::Board* b, const Chess::PieceAndPosition p, 
        std::vector<Chess::Move>& returned_moves);

/*
 * \brief Function used to find all possible moves in a non-check situation
 * \param b The board
 * \param list_pieces the pieces of the player
 * \return vector of all possible moves
 */
std::vector<Chess::Move> AllPossibleMoves( Chess::Board* b, 
    const std::vector<Chess::PieceAndPosition>& list_pieces );
std::vector<Chess::Move> AllPossibleMovesCheck( Chess::Board* b, 
    const std::vector<Chess::PieceAndPosition>& list_pieces, const Chess::Move last_move_played, 
    const Chess::PieceAndPosition king, const u8 result );

//  It could be better to have separate check_all_lines for the king and the other pieces, since can't check
/*
 * \brief Determines if the player is in check
 * \brief the move HAS TO have been played on the board. Otherwise, the comportment is to be unexpected.
 * \brief ( if the last move was a promotion, the piece is therefore considered as promoted )
 * \param b The board
 * \param last_move_played the last move played
 * \param king the position of the king
 * \return 255 if there is no check \
 * \return a case (0<=...<64), if a single piece, other than a knight, is checking \
 * \return 100+(0<=...<64) if a knight checks, where the return value-100 is the square of the knight\
 * \return 200, if two pieces are checking at the same time \
 */
ufast8 CheckCheck( Chess::Board* b , const Chess::Move last_move_played, 
    const Chess::PieceAndPosition king );
void BlockLineOfSight(ChIAss::Chess::Board* b, const ChIAss::Chess::PieceAndPosition king, const u8 square_checking,
                std::vector<ChIAss::Chess::Move>& returned);
void TakeCheckingPiece(ChIAss::Chess::Board* b, const ChIAss::Chess::PieceAndPosition king, const u8 square_checking,
                std::vector<ChIAss::Chess::Move>& returned);

fast16 ValueBoard( Chess::Board* b , const std::vector<Chess::PieceAndPosition>& your_pieces, 
    const std::vector<Chess::PieceAndPosition>& oppo_pieces );

std::vector<Chess::Move> Moves( Chess::Board* b, 
        const std::vector<Chess::PieceAndPosition>& list_pieces,
        const Chess::Move last_move_played, ufast8* result_checkcheck );

fast16 ValueMove( Chess::Board* b, const ufast8 current_depth, const ufast8 expected_depth, 
        std::vector<Chess::PieceAndPosition>& your_pieces, std::vector<Chess::PieceAndPosition>& opponents_pieces, 
        const Chess::Move last_move_played, const fast16 parent_bound );
/*fast16 ValueMoveWhite( Chess::Board* b, fast8 current_depth, fast8 expected_depth, 
std::vector<Chess::PieceAndPosition> your_pieces, std::vector<Chess::PieceAndPosition> opponents_pieces, 
Chess::Move last_move_played, fast16 parent_bound );
fast16 ValueMoveBlack( Chess::Board* b, fast8 current_depth, fast8 expected_depth, 
std::vector<Chess::PieceAndPosition> your_pieces, std::vector<Chess::PieceAndPosition> opponents_pieces, 
Chess::Move last_move_played, fast16 parent_bound );*/

Chess::Move ChIAssMove( Chess::Board* b, const Chess::colour col, 
        const ufast8 expected_depth, const Chess::Move last_move_played );




        }       // namespace Engine
}       // namespace ChIAss

#endif //CHIASS_HPP