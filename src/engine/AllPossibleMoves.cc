#include "engine.hpp"

std::vector<ChIAss::Chess::Move> ChIAss::Engine::AllPossibleMoves( Chess::Board* b , 
			const std::vector<Chess::PieceAndPosition>& list_pieces ) {
// TODO reserve
	std::vector<Chess::Move> returned;

//	We start by looking at the moves of the last pieces in the vector : 
//		Each time a piece is taken during the exploration, it's popped out of the vector, then put back
//		This means that the pieces having "interesting moves" are generally at the end of the vector 
	Chess::PieceAndPosition p;
	for (ufast8 i=list_pieces.size()-1; i>0; i--) {
		p = list_pieces[i];
//	for (PieceAndPosition p : list_pieces) {
		switch ( p.piece ) {
			case Chess::type_value::STARTING_PAWN: 
				Engine::AllMovesStartingPawn(b, p, returned, list_pieces[0]);
				break;
			case Chess::type_value::PAWN:
				Engine::AllMovesPawn(b, p, returned, list_pieces[0]);
				break;
			case Chess::type_value::KNIGHT:
				Engine::AllMovesKnight(b, p, returned, list_pieces[0]);
				break;
			case Chess::type_value::BISHOP:
				Engine::AllMovesBishop(b, p, returned, list_pieces[0]);
				break;
// Deprecated : The QUEEN doesn't have a break statement, so we don't have to rewrite the rook moves
			case Chess::type_value::QUEEN: 
				Engine::AllMovesQueen(b, p, returned, list_pieces[0]);
				break;
			case Chess::type_value::ROOK:
				Engine::AllMovesRook(b, p, returned, list_pieces[0]);
				break;
// once rooks move, those aren't called anymore
			case Chess::type_value::ROOK_CAN_CASTLE:
				Engine::AllMovesRookCanCastle(b, p, returned);
				break;
/*			case KING_CAN_CASTLE:
				Engine::AllMovesKingCanCastle(b, p, returned);
				break;
			case KING:
				Engine::AllMovesKing(b, p, returned);
				break;*/
			default: // EMPTY, KING, KING_CAN_CASTLE...
				fprintf( stdout , "Error %d piece is in the pieces vector\n", p.piece);
				PrintBoard(*b);
				exit(EXIT_FAILURE);
			break;
		}
	}
	
	if (list_pieces[0].piece == Chess::type_value::KING_CAN_CASTLE) {
		Engine::AllMovesKingCanCastle(b, list_pieces[0], returned);
	} else {
		Engine::AllMovesKing(b, list_pieces[0], returned);
	}
	
	return returned;
}
