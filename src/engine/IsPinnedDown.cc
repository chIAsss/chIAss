#include "engine.hpp"

// TODO : you can actually optimize the diagonals search
// instead of checking the condition in a while, you could just calculate how far you are from both borders
// then, take the min, and go in a for loop
//	min(modulo, division) + verify (min) inequalities for the loop
//	against : verify (min) inequalities + modulo
/*
 * \return a value indicating if the piece is pinned down : \
 * \return 0 if it is completely pinned down (deprecated)\
 * \return 1 if it isn't pinned down \
 * \return 2 if it can move up and down \
 * \return 3 if it can move left and right \
 * \return 4 if it can move diagonaly up left/down right \
 * \return 5 if it can move diagonaly up right/down left \
 */
u8 ChIAss::Engine::IsPinnedDown( Chess::Board* b , const Chess::PieceAndPosition p, 
			const Chess::PieceAndPosition king ) {
	ufast8 checking_king;
// checking if the king is in any line of view from the piece
	if ( p.position%8 == king.position%8 ) {// UP/DOWN
		if ( p.position < king.position ) {//UP
			goto king_is_up;
		} else {//DOWN
			goto king_is_down;
		}
	}
	if ( p.position/8 == king.position/8 ) {//RIGHT/LEFT
		if ( p.position < king.position ) {//RIGHT
			goto king_is_right;
		} else {//LEFT
			goto king_is_left;
		}
	}
//	if ( p.position/8 + p.position%8 == king.position/8 + king.position%8 ) {// UP LEFT / DOWN RIGHT
	if ( p.position%7 == king.position%7 ) {// UP LEFT / DOWN RIGHT
		if ( p.position < king.position ) {//UP lEFT
			goto king_is_up_left;
		} else {// DOWN RIGHT
			goto king_is_down_right;
		}
	}
//	if ( p.position/8 + 8 - p.position%8 == king.position/8 + 8 - king.position%8 ) {// UP RIGHT / DOWN LEFT
	if ( p.position%9 == king.position%9 ) {// UP RIGHT / DOWN LEFT
		if ( p.position < king.position ) {//UP RIGHT
			goto king_is_up_right;
		} else {// DOWN LEFT
			goto king_is_down_left;
		}
	}
	return 1;// No danger for the KING



// UP =====================================
	king_is_up:
		checking_king = ( p.position + 8 );
		while ( checking_king < 64 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position - 8 );
					while ( checking_king < 64 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece >= Chess::type_value::ROOK_CAN_CASTLE && b->B[ checking_king ].piece <= Chess::type_value::QUEEN ) {
									return 2;
								}
							}
							return 1;// No danger for the KING, since it's not a piece that can attack in a straight line or an enemy piece
						}
						checking_king -= (ufast8) 8;
					}
				}
				return 1;// No danger for the KING
			}
			checking_king += (ufast8) 8;
		}
	return 1;// useless, since we know we will find the king
	

// DOWN =====================================
	king_is_down:
		checking_king = ( p.position - 8 );
		while ( checking_king < 64 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position + 8 );
					while ( checking_king < 64 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece >= Chess::type_value::ROOK_CAN_CASTLE && b->B[ checking_king ].piece <= Chess::type_value::QUEEN ) {
									return 2;
								}
							}
							return 1;// No danger for the KING, since it's not a piece that can attack in a straight line or an enemy piece
						}
						checking_king += (ufast8) 8;
					}
				}
				return 1;// No danger for the KING
			}
			checking_king -= (ufast8) 8;
		}
	return 1;// useless, since we know we will find the king



// RIGHT ======================================================
	king_is_right:
		checking_king = ( p.position + 1 );
		while ( checking_king%8 != 0 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position - 1 );
					while ( checking_king%8 != 7 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece >= Chess::type_value::ROOK_CAN_CASTLE && b->B[ checking_king ].piece <= Chess::type_value::QUEEN ) {
									return 3;
								}
							}
							return 1;// No danger for the KING
						}
						checking_king --;
					}
				}
				return 1;// Can't find the king
			}
			checking_king ++;
		}
	return 1;// useless, since we know we will find the king



// LEFT =======================================================
	king_is_left:
		checking_king = ( p.position - 1 );
		while ( checking_king%8 != 7 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position + 1 );
					while ( checking_king%8 != 0 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece >= Chess::type_value::ROOK_CAN_CASTLE && b->B[ checking_king ].piece <= Chess::type_value::QUEEN ) {
									return 3;
								}
							}
							return 1;// No danger for the KING
						}
						checking_king ++;
					}
				}
				return 1;// Can't find the king
			}
			checking_king --;
		}
	return 1;// useless, since we know we will find the king




// UP LEFT ===========================================================
	king_is_up_left:
		checking_king = ( p.position + 7 );
		while ( checking_king < 64 && checking_king%8 != 7 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position - 7 );
					while ( checking_king < 64 && checking_king%8 != 0 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece == Chess::type_value::QUEEN || b->B[ checking_king ].piece == Chess::type_value::BISHOP ) {
									return 4;
								}
							}
							return 1;// No danger for the KING
						}
						checking_king -= (ufast8) 7;
					}
				}
				return 1;// Can't find the king
			}
			checking_king += (ufast8) 7;
		}
	return 1;// useless, since we know we will find the king

// UP RIGHT ===========================================================
	king_is_up_right:
		checking_king = ( p.position + 9 );
		while ( checking_king < 64 && checking_king%8 != 0 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position - 9 );
					while ( checking_king < 64 && checking_king%8 != 7 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece == Chess::type_value::QUEEN || b->B[ checking_king ].piece == Chess::type_value::BISHOP ) {
									return 5;
								}
							}
							return 1;// No danger for the KING
						}
						checking_king -= (ufast8) 9;
					}
				}
				return 1;// Can't find the king
			}
			checking_king += (ufast8) 9;
		}
	return 1;// useless, since we know we will find the king

// DOWN RIGHT ===========================================================
	king_is_down_right:
		checking_king = ( p.position - 7 );
		while ( checking_king < 64 && checking_king%8 != 0 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position + 7 );
					while ( checking_king < 64 && checking_king%8 != 7 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece == Chess::type_value::QUEEN || b->B[ checking_king ].piece == Chess::type_value::BISHOP ) {
									return 4;
								}
							}
							return 1;// No danger for the KING
						}
						checking_king += (ufast8) 7;
					}
				}
				return 1;// Can't find the king
			}
			checking_king -= (ufast8) 7;
		}
	return 1;// useless, since we know we will find the king

// DOWN LEFT ===========================================================
	king_is_down_left:
		checking_king = ( p.position - 9 );
		while ( checking_king < 64 && checking_king%8 != 7 ) {
			if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
				if ( checking_king == king.position ) {
					checking_king = ( p.position + 9 );
					while ( checking_king < 64 && checking_king%8 != 0 ) {
						if ( b->B[ checking_king ].piece != Chess::type_value::EMPTY ) {
							if ( b->B[ checking_king ].colour != p.colour ) {
								if ( b->B[ checking_king ].piece == Chess::type_value::QUEEN || b->B[ checking_king ].piece == Chess::type_value::BISHOP ) {
									return 5;
								}
							}
							return 1;// No danger for the KING
						}
						checking_king += (ufast8) 9;
					}
				}
				return 1;// Can't find the king
			}
			checking_king -= (ufast8) 9;
		}
	return 1;
}
