#include "engine.hpp"

void ChIAss::Engine::AllMovesStartingPawn(Chess::Board* b, const Chess::PieceAndPosition p, 
            std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king) {
    Chess::Move added_move;
    added_move.type=Chess::FIRST_MOVE;
    added_move.beg=p.position;

	ufast8 is_pinned = Engine::IsPinnedDown( b , p , king );
	switch ( is_pinned ) {
        case 1:// The pawn isn't pinned
            if ( p.colour == Chess::WHITE) {
                if ( b->B[ p.position + 8 ].piece == Chess::type_value::EMPTY ) {
        // note : %8 doesn't seem to be that slower than just -8
        // But could be an improvement 
                    added_move.end =  (Chess::square) ( p.position + 8 );
                    returned_moves.push_back( added_move );
                    if ( b->B[ p.position + 16 ].piece == Chess::type_value::EMPTY ) {
        // note : %8 doesn't seem to be that slower than just -8
        // But could be an improvement 
                        added_move.end = (Chess::square) ( p.position + 16 );
                        returned_moves.push_back( added_move );
                    }
                }
                if ( p.position %8 > 0 ) {
                    if ( b->B[ p.position + 7 ].colour == Chess::BLACK ) {
                        added_move.end =  (Chess::square) ( p.position + 7 );
                        returned_moves.push_back( added_move );
                    }
                    if ( p.position %8 < 7 ) {
                        if ( b->B[ p.position + 9 ].colour == Chess::BLACK ) {
                            added_move.end =  (Chess::square) ( p.position + 9 );
                            returned_moves.push_back( added_move );
                        }
                    }
                } else {// Still on my shenanigans to evade as much tests as possible, if position%8 == 0, we don't test for < 7
                    if ( b->B[ p.position + 9 ].colour == Chess::BLACK ) {
                        added_move.end =  (Chess::square) ( p.position + 9 );
                        returned_moves.push_back( added_move );
                    }
                }
            } else {
                if ( b->B[ p.position - 8 ].piece == Chess::type_value::EMPTY ) {
                    added_move.end =  (Chess::square) ( p.position - 8 );
                    returned_moves.push_back( added_move );

                    if ( b->B[ p.position - 16 ].piece == Chess::type_value::EMPTY ) {
                        added_move.end =  (Chess::square) ( p.position - 16 );
                        returned_moves.push_back( added_move );
                    }
                }
                if ( p.position%8 < 7 ) {
                    if ( b->B[ p.position - 7 ].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[ p.position - 7 ].colour == Chess::WHITE ) {
                            added_move.end =  (Chess::square) ( p.position - 7 );
                            returned_moves.push_back( added_move );
                        }
                    }
                    if ( p.position%8 > 0) {
                        if ( b->B[ p.position - 9 ].piece != Chess::type_value::EMPTY ) {
                            if ( b->B[ p.position - 9 ].colour == Chess::WHITE ) {
                                added_move.end =  (Chess::square) ( p.position - 9 );
                                returned_moves.push_back( added_move );
                            }
                        }
                    }
                } else {// Still on my shenanigans to evade as much tests as possible, if position%8 == 7, we don't test for > 0
                    if ( b->B[ p.position - 9 ].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[ p.position - 9 ].colour == Chess::WHITE ) {
                            added_move.end =  (Chess::square) ( p.position - 9 );
                            returned_moves.push_back( added_move );
                        }
                    }
                }
            }
        break;
        case 2:// can only move up and down
            if ( p.colour == Chess::WHITE ) {
                if ( b->B[ p.position + 8 ].piece == Chess::type_value::EMPTY ) {
                    added_move.end =  (Chess::square) ( p.position + 8 );
                    returned_moves.push_back( added_move );

                    if ( b->B[ p.position + 16 ].piece == Chess::type_value::EMPTY ) {
                        added_move.end =  (Chess::square) ( p.position + 16 );
                        returned_moves.push_back( added_move );
                    }
                }
            } else {
                if ( b->B[ p.position - 8 ].piece == Chess::type_value::EMPTY ) {
                    added_move.end =  (Chess::square) ( p.position - 8 );
                    returned_moves.push_back( added_move );

                    if ( b->B[ p.position - 16 ].piece == Chess::type_value::EMPTY ) {
                        added_move.end =  (Chess::square) ( p.position - 16 );
                        returned_moves.push_back( added_move );
                    }
                }
            }
        break;
        case 4:// can only move up left / down right
            if ( p.colour == Chess::WHITE ) {
                if ( p.position %8 > 0 ) {
                    if ( b->B[ p.position + 7 ].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[ p.position + 7 ].colour == Chess::BLACK ) {
                            added_move.end =  (Chess::square) ( p.position + 7 );
                            returned_moves.push_back( added_move );
                        }
                    }
                }
            } else {
                if ( p.position %8 < 7 ) {
                    if ( b->B[ p.position - 7 ].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[ p.position - 7 ].colour == Chess::WHITE ) {
                            added_move.end =  (Chess::square) ( p.position - 7 );
                            returned_moves.push_back( added_move );
                        }
                    }
                }
            }
        break;
        case 5:// can only move up right / down left
            if ( p.colour == Chess::WHITE ) {
                if ( p.position %8 < 7 ) {
                    if ( b->B[ p.position + 9 ].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[ p.position + 9 ].colour == Chess::BLACK ) {
                            added_move.end =  (Chess::square) ( p.position + 9 );
                            returned_moves.push_back( added_move );
                        }
                    }
                }
            } else {
                if ( p.position %8 > 0 ) {
                    if ( b->B[ p.position - 9 ].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[ p.position - 9 ].colour == Chess::WHITE ) {
                            added_move.end =  (Chess::square) ( p.position - 9 );
                            returned_moves.push_back( added_move );
                        }
                    }
                }
            }
        break;
        default:
            break;
    }
}