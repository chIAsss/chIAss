#include "engine.hpp"

fast16 ChIAss::Engine::ValueMoveWhite( Chess::Board* b, fast8 current_depth, fast8 expected_depth, 
std::vector<Chess::PieceAndPosition> your_pieces, std::vector<Chess::PieceAndPosition> opponent_pieces, 
Chess::Move last_move_played , fast16 parent_bound ) {
// ============================================= COMPUTING ALL MOVES ==================================================
    std::vector<Chess::Move> moves;
    ufast8 result_check;
    moves = Engine::Moves( b, your_pieces, last_move_played, &result_check );

// ============================================== CHECKING FOR MATE ===================================================
    if ( moves.size() == 0 ) {// No possibility : pat or checkmate
        if ( result_check == 255 ) {// PAT the great equalizer
            return 0;
        } else {
            return -INFINITY_CHIASS+current_depth;
        }
    }

    fast16 best_value;
    fast16 personnal_bound;

    personnal_bound = -INFINITY_CHIASS;
    fast16 current_value;
// The piece that's been taken, if ever, or {EMPTY, WHITE}, used to unplay the move
    Chess::Piece taken;

    if ( current_depth == expected_depth ) { // ==================== LEAF =============================================
        return Engine::ValueBoard( b, your_pieces, opponent_pieces );
    } else {// ============================================= NOT LEAF =================================================
// Play the first move, check if it's a mate, then check for the best value yet
// Do you have the update the your_pieces vector? Only if there's no check, to value the board

// First move =========================================================================================================
        // play the move ==============================================================================================
        
        Chess::Play( b , moves[0] , &taken, your_pieces, opponent_pieces);

        best_value = Engine::ValueMoveBlack( b, current_depth+1, expected_depth, 
                                        opponent_pieces, your_pieces, 
                                        moves[0] , personnal_bound );
        Chess::Unplay( b, moves[0], taken, your_pieces, opponent_pieces);
        // ============================================ BRANCH ESCAPING ===============================================
        if ( best_value >= parent_bound ) { // We're over the bound : the opponent won't choose this branch, we escape it
            return best_value;// We escape this branch
        }
        personnal_bound = best_value;
// END First move =====================================================================================================

// Rest of the moves ==================================================================================================
        for ( ufast8 i = 1 ; i < moves.size() ; i++ ) {

            Chess::Play( b , moves[i] , &taken, your_pieces, opponent_pieces);
            current_value = Engine::ValueMoveBlack( b, current_depth+1, expected_depth, 
                                        opponent_pieces, your_pieces, 
                                        moves[i] , personnal_bound );

            Chess::Unplay( b, moves[i], taken, your_pieces, opponent_pieces);
        // ============================================ BRANCH ESCAPING ===============================================
            if ( current_value > best_value ) {
// If the value is better than the better value yet, the opponent isn't gonna choose this branch anyway
                if ( current_value >= parent_bound ) { // We're over the bound : the opponent won't choose this branch, we escape it
                    return current_value;// We escape this branch
                }
                best_value = current_value;
                personnal_bound = best_value;    
            }

        }
// END Rest of the moves ==============================================================================================
    }
    return best_value;
}