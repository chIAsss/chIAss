#include "engine.hpp"

std::vector<ChIAss::Chess::Move> ChIAss::Engine::Moves( Chess::Board* b, const std::vector<Chess::PieceAndPosition>& your_pieces,
const Chess::Move last_move_played, ufast8* result_check ) {

    std::vector<Chess::Move> moves;
    *result_check = CheckCheck( b, last_move_played, your_pieces[0] );
    if ( *result_check == 255 ) {// the king isn't in check
        moves = AllPossibleMoves( b, your_pieces );
    } else {
        moves = AllPossibleMovesCheck( b, your_pieces, last_move_played, your_pieces[0], *result_check );
    }


//  Enough for now
    if (*result_check == 255 || *result_check == last_move_played.end) {
// The En passant rule ============================================================================
        if ( last_move_played.type == Chess::FIRST_MOVE && 
                    b->B[last_move_played.end].piece == Chess::PAWN ) {
            Chess::Move en_passant_move;
            ufast8 is_pinned_down;
            en_passant_move.type=Chess::EN_PASSANT;
            if ( last_move_played.end/8 == 3 ) {// The last move played was a white pawn dash
                if ( last_move_played.end%8 > 0 ) {
                    if (b->B[last_move_played.end-1] == (Chess::Piece) {Chess::PAWN, Chess::BLACK}) {
    //  Is pinned down
                        is_pinned_down = IsPinnedDown(b, {Chess::PAWN, Chess::BLACK, (Chess::square)(last_move_played.end-1)}, your_pieces[0]);
                        if (is_pinned_down == 1 || is_pinned_down == 4 ) {
                            en_passant_move.beg = (Chess::square) (last_move_played.end-1);
                            en_passant_move.end = (Chess::square) (last_move_played.end-8);
                            moves.push_back(en_passant_move);
                        }
                    }
                    if ( last_move_played.end%8 < 7 ) {
                        if (b->B[last_move_played.end+1] == (Chess::Piece) {Chess::PAWN, Chess::BLACK}) {
                            is_pinned_down = IsPinnedDown(b, {Chess::PAWN, Chess::BLACK, (Chess::square)(last_move_played.end+1)}, your_pieces[0]);
                            if (is_pinned_down == 1 || is_pinned_down == 5 ) {
                                en_passant_move.beg = (Chess::square) (last_move_played.end+1);
                                en_passant_move.end = (Chess::square) (last_move_played.end-8);
                                moves.push_back(en_passant_move);
                            }
                        }
                    }
                } else {// == 0
                    if (b->B[last_move_played.end+1] == (Chess::Piece) {Chess::PAWN, Chess::BLACK}) {
                        is_pinned_down = IsPinnedDown(b, {Chess::PAWN, Chess::BLACK, (Chess::square)(last_move_played.end+1)}, your_pieces[0]);
                        if (is_pinned_down == 1 || is_pinned_down == 5 ) {
                            en_passant_move.beg = (Chess::square) (last_move_played.end+1);
                            en_passant_move.end = (Chess::square) (last_move_played.end-8);
                            moves.push_back(en_passant_move);
                        }
                    }
                }
            } else if ( last_move_played.end/8 == 4 ) {// The last move played was a black pawn dash
                if ( last_move_played.end%8 > 0 ) {
                    if (b->B[last_move_played.end-1] == (Chess::Piece) {Chess::PAWN, Chess::WHITE}) {
                        is_pinned_down = IsPinnedDown(b, {Chess::PAWN, Chess::WHITE, (Chess::square)(last_move_played.end-1)}, your_pieces[0]);
                        if (is_pinned_down == 1 || is_pinned_down == 5 ) {
                            en_passant_move.beg = (Chess::square) (last_move_played.end-1);
                            en_passant_move.end = (Chess::square) (last_move_played.end+8);
                            moves.push_back(en_passant_move);
                        }
                    }
                    if ( last_move_played.end%8 < 7 ) {
                        if (b->B[last_move_played.end+1] == (Chess::Piece) {Chess::PAWN, Chess::WHITE}) {
                            is_pinned_down = IsPinnedDown(b, {Chess::PAWN, Chess::WHITE, (Chess::square)(last_move_played.end+1)}, your_pieces[0]);
                            if (is_pinned_down == 1 || is_pinned_down == 4 ) {
                                en_passant_move.beg = (Chess::square) (last_move_played.end+1);
                                en_passant_move.end = (Chess::square) (last_move_played.end+8);
                                moves.push_back(en_passant_move);
                            }
                        }
                    }
                } else {// == 0
                    if (b->B[last_move_played.end+1] == (Chess::Piece) {Chess::PAWN, Chess::WHITE}) {
                        is_pinned_down = IsPinnedDown(b, {Chess::PAWN, Chess::WHITE, (Chess::square)(last_move_played.end+1)}, your_pieces[0]);
                        if (is_pinned_down == 1 || is_pinned_down == 4 ) {
                            en_passant_move.beg = (Chess::square) (last_move_played.end+1);
                            en_passant_move.end = (Chess::square) (last_move_played.end+8);
                            moves.push_back(en_passant_move);
                        }
                    }
                }
            }
        }

    }



    return moves;
}
