#include "engine.hpp"

void ChIAss::Engine::AllMovesKingCanCastle(Chess::Board* b, const Chess::PieceAndPosition p, 
            std::vector<Chess::Move>& returned_moves) {
    Chess::Move added_move;
    added_move.type=Chess::FIRST_MOVE;
    added_move.beg=p.position;
	Chess::Board temporary = *b;
    Chess::Piece taken;

    if ( p.colour == Chess::WHITE ) {
        if ( b->B[ Chess::d1 ].piece == Chess::EMPTY ) {
// white queen side castle	
            added_move.end = Chess::d1 ;
            temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::d1 ] = {Chess::KING,Chess::WHITE};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                temporary.B[ Chess::d1 ] = {Chess::EMPTY,Chess::WHITE};
                returned_moves.push_back( added_move );
                if ( b->B[ Chess::c1 ].piece == Chess::EMPTY && 
                b->B[ Chess::b1 ].piece == Chess::EMPTY && 
                b->B[ Chess::a1 ].piece == Chess::ROOK_CAN_CASTLE ) {
                    added_move.end = Chess::c1 ;
                    temporary.B[ Chess::c1 ] = {Chess::KING,Chess::WHITE};
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( {Chess::a1,Chess::d1,Chess::CASTLE} );
                    }
                    temporary.B[ Chess::c1 ] = {Chess::EMPTY,Chess::WHITE};
                }
            } else {
                temporary.B[ Chess::d1 ] = {Chess::EMPTY,Chess::WHITE};
            }
        } else {
            if ( b->B[ Chess::d1 ].colour == Chess::BLACK ) {
                added_move.end = Chess::d1 ;
                taken = temporary.B[Chess::d1];
                temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
                temporary.B[ Chess::d1 ] = {Chess::KING,Chess::WHITE};
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    added_move.end = Chess::d1;
                    returned_moves.push_back( added_move );
                }
                temporary.B[ Chess::d1 ] = taken;
            }
        }

        if ( b->B[ Chess::d2 ].piece == Chess::EMPTY || b->B[ Chess::d2 ].colour == Chess::BLACK ) {
            added_move.end = Chess::d2 ;
            taken = temporary.B[Chess::d2];
            temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::d2 ] = {Chess::KING,Chess::WHITE};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                added_move.end = Chess::d2;
                returned_moves.push_back( added_move );
            }
            temporary.B[ Chess::d2 ] = taken;
        }
        if ( b->B[ Chess::e2 ].piece == Chess::EMPTY || b->B[ Chess::e2 ].colour == Chess::BLACK ) {
            added_move.end = Chess::e2 ;
            taken = temporary.B[Chess::e2];
            temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::e2 ] = {Chess::KING,Chess::WHITE};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                added_move.end = Chess::e2;
                returned_moves.push_back( added_move );
            }
            temporary.B[ Chess::e2 ] = taken;
        }
        if ( b->B[ Chess::f2 ].piece == Chess::EMPTY || b->B[ Chess::f2 ].colour == Chess::BLACK ) {
            added_move.end = Chess::f2 ;
            taken = temporary.B[Chess::f2];
            temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::f2 ] = {Chess::KING,Chess::WHITE};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                added_move.end = Chess::f2;
                returned_moves.push_back( added_move );
            }
            temporary.B[ Chess::f2 ] = taken;
        }

        if ( b->B[ Chess::f1 ].piece == Chess::EMPTY ) {
// Chess::WHITE king side castle	
            added_move.end = Chess::f1 ;
            temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::f1 ] = {Chess::KING,Chess::WHITE};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                temporary.B[ Chess::f1 ] = {Chess::EMPTY,Chess::WHITE};
                added_move.end = Chess::f1;
                returned_moves.push_back( added_move );
                if ( b->B[ Chess::g1 ].piece == Chess::EMPTY && b->B[ Chess::h1 ].piece == Chess::ROOK_CAN_CASTLE ) {
                    added_move.end = Chess::g1 ;
                    temporary.B[ Chess::g1 ] = {Chess::KING,Chess::WHITE};
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( {Chess::h1,Chess::f1,Chess::CASTLE} );
                    }
                    temporary.B[ Chess::g1 ] = {Chess::EMPTY,Chess::WHITE};
                }
            } else {
                temporary.B[ Chess::f1 ] = {Chess::EMPTY,Chess::WHITE};
            }
        } else {
            if ( b->B[ Chess::f1 ].colour == Chess::BLACK ) {
                added_move.end = Chess::f1;
                taken = temporary.B[Chess::f1];
                temporary.B[ Chess::e1 ] = {Chess::EMPTY,Chess::WHITE};
                temporary.B[ Chess::f1 ] = {Chess::KING,Chess::WHITE};
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    added_move.end = Chess::f1;
                    returned_moves.push_back( added_move );
                }
                temporary.B[ Chess::f1 ] = taken;
            }
        }
        temporary.B[ Chess::e1 ] = {Chess::KING_CAN_CASTLE,Chess::WHITE};



    } else { // black



        if ( b->B[ Chess::f8 ].piece == Chess::EMPTY ) {
// black king side castle	
            added_move.end = Chess::f8 ;
            temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::f8 ] = {Chess::KING,Chess::BLACK};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                temporary.B[ Chess::f8 ] = {Chess::EMPTY,Chess::WHITE};
                added_move.end = Chess::f8;
                returned_moves.push_back( added_move );
                if ( b->B[ Chess::g8 ].piece == Chess::EMPTY && b->B[ Chess::h8 ].piece == Chess::ROOK_CAN_CASTLE ) {
                    added_move.end = Chess::g8 ;
                    temporary.B[ Chess::g8 ] = {Chess::KING,Chess::BLACK};
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( {Chess::h8,Chess::f8,Chess::CASTLE} );
                    }
                    temporary.B[ Chess::g8 ] = {Chess::EMPTY,Chess::WHITE};
                }
            } else {
                temporary.B[ Chess::f8 ] = {Chess::EMPTY,Chess::WHITE};
            }
        } else {
            if ( b->B[ Chess::f8 ].colour == Chess::WHITE ) {
                added_move.end = Chess::f8 ;
                taken = temporary.B[Chess::f8];
                temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
                temporary.B[ Chess::f8 ] = {Chess::KING,Chess::BLACK};
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    added_move.end = Chess::f8;
                    returned_moves.push_back( added_move );
                }
                temporary.B[ Chess::f8 ] = taken;
            }
        }


        if ( b->B[ Chess::f7 ].colour == Chess::WHITE ) {
            added_move.end = Chess::f7 ;
            taken = temporary.B[Chess::f7];
            temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::f7 ] = {Chess::KING,Chess::BLACK};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                added_move.end = Chess::f7;
                returned_moves.push_back( added_move );
            }
            temporary.B[ Chess::f7 ] = taken;
        }
        if ( b->B[ Chess::e7 ].colour == Chess::WHITE ) {
            added_move.end = Chess::e7 ;
            taken = temporary.B[Chess::e7];
            temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::e7 ] = {Chess::KING,Chess::BLACK};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                added_move.end = Chess::e7;
                returned_moves.push_back( added_move );
            }
            temporary.B[ Chess::e7 ] = taken;
        }
        if ( b->B[ Chess::d7 ].colour == Chess::WHITE ) {
            added_move.end = Chess::d7 ;
            taken = temporary.B[Chess::d7];
            temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::d7 ] = {Chess::KING,Chess::BLACK};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                added_move.end = Chess::d7;
                returned_moves.push_back( added_move );
            }
            temporary.B[ Chess::d7 ] = taken;
        }


        if ( b->B[ Chess::d8 ].piece == Chess::EMPTY ) {
// black queen side castle	
            added_move.end = Chess::d8 ;
            temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
            temporary.B[ Chess::d8 ] = {Chess::KING,Chess::BLACK};
            if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                temporary.B[ Chess::d8 ] = {Chess::EMPTY,Chess::WHITE};
                added_move.end = Chess::d8;
                returned_moves.push_back( added_move );
                if ( b->B[ Chess::c8 ].piece == Chess::EMPTY && b->B[ Chess::b8 ].piece == Chess::EMPTY && b->B[ Chess::a8 ].piece == Chess::ROOK_CAN_CASTLE ) {
                    added_move.end = Chess::c8 ;
                    temporary.B[ Chess::c8 ] = {Chess::KING,Chess::BLACK};
                    if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                        returned_moves.push_back( {Chess::a8,Chess::d8,Chess::CASTLE} );
                    }
                    temporary.B[ Chess::c8 ] = {Chess::EMPTY,Chess::WHITE};
                }
            } else {
                temporary.B[ Chess::d8 ] = {Chess::EMPTY,Chess::WHITE};
            }
        } else {
            if ( b->B[ Chess::d8 ].colour == Chess::WHITE ) {
                added_move.end = Chess::d8;
                taken = temporary.B[Chess::d8];
                temporary.B[ Chess::e8 ] = {Chess::EMPTY,Chess::WHITE};
                temporary.B[ Chess::d8 ] = {Chess::KING,Chess::BLACK};
                if ( Chess::IsKingSafe( temporary , added_move.end ) ) {
                    added_move.end = Chess::d8;
                    returned_moves.push_back( added_move );
                }
                temporary.B[ Chess::d8 ] = taken;
            }
        }
        temporary.B[ Chess::e8 ] = {Chess::KING_CAN_CASTLE,Chess::BLACK};
    }
}