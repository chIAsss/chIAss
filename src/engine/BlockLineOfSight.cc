#include "engine.hpp"

void ChIAss::Engine::BlockLineOfSight(Chess::Board* b, const Chess::PieceAndPosition king, 
                                    const u8 square_checking, std::vector<Chess::Move>& returned) {

    fast8 step;

    if ( king.position/8 == square_checking/8 ) {
        step = 1;
    } else if ( king.position%8 == square_checking%8 ) {
        step = 8;
    } else if ( king.position%9 == square_checking%9 ) {
        step = 9;
    } else {//if ( king.position%7 == square_checking%7 ) {
        step = 7;
    }
    ufast8 valid_square;
    ufast8 current_square;
    Chess::Move added_move;
    added_move.type = Chess::type_of_move::NORMAL_MOVE;
    Chess::PieceAndPosition tempo;
    unsigned char is_pinned;

//  TODO this might be better with a switch : and 4 separate loops, each ignoring what it should
    //  + step
    if ( king.position > square_checking ) {
        for (valid_square=square_checking+step; valid_square != king.position; valid_square += step) {
            added_move.end = (Chess::square) valid_square;

            if ( step != 1 ) {
                //  horizontal -
                current_square = valid_square-1;
                while (current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 3 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square --;
                }

                //  horizontal +
                current_square = valid_square+1;
                while (current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 3 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square ++;
                }
            }


            if ( step != 8 ) {
                //  vertical -
                current_square = valid_square-8;
                if ( king.colour == Chess::WHITE ) {// pawn can intervene:
                    if (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if (b->B[current_square].colour == king.colour) { 
                                if ( b->B[current_square].piece == Chess::type_value::PAWN ||
                                b->B[current_square].piece == Chess::type_value::ROOK ||
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        added_move.type = Chess::type_of_move::FIRST_MOVE;
                                        returned.push_back(added_move);
                                        added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                    }
                                }
                            }
                        } else {
                            current_square -= 8;
                            if (current_square < 64) {
                                if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                    if (b->B[current_square].colour == king.colour) { 
                                        if ( b->B[current_square].piece == Chess::type_value::ROOK ||
                                        b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                returned.push_back(added_move);
                                            }
                                        } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                added_move.type = Chess::type_of_move::FIRST_MOVE;
                                                returned.push_back(added_move);
                                                added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                            }
                                        }
                                    }
                                } else {
                                    current_square -= 8;
                                    while (current_square < 64) {
                                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                            if ( b->B[current_square].colour == king.colour ) {
                                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                            b->B[current_square].colour, (Chess::square) current_square};
                                                    is_pinned = IsPinnedDown(b, tempo, king);
                                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                                        added_move.beg = (Chess::square) current_square;
                                                        returned.push_back(added_move);
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                        current_square -= 8;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    while (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if ( b->B[current_square].colour == king.colour ) {
                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                }
                            }
                            break;
                        }
                        current_square -= 8;
                    }
                }

                //  vertical +
                current_square = valid_square+8;
                if ( king.colour == Chess::BLACK ) {// Pawn can intervene
                    if (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if (b->B[current_square].colour == Chess::BLACK ) { 
                                if ( b->B[current_square].piece == Chess::type_value::PAWN ||
                                b->B[current_square].piece == Chess::type_value::ROOK ||
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        added_move.type = Chess::type_of_move::FIRST_MOVE;
                                        returned.push_back(added_move);
                                        added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                    }
                                }
                            }
                        } else {
                            current_square += 8;
                            if (current_square < 64) {
                                if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                    if (b->B[current_square].colour == Chess::BLACK) { 
                                        if ( b->B[current_square].piece == Chess::type_value::ROOK ||
                                        b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                returned.push_back(added_move);
                                            }
                                        } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                added_move.type = Chess::type_of_move::FIRST_MOVE;
                                                returned.push_back(added_move);
                                                added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                            }
                                        }
                                    }
                                } else {
                                    current_square += 8;
                                    while (current_square < 64) {
                                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                            if ( b->B[current_square].colour == Chess::BLACK ) {
                                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                            b->B[current_square].colour, (Chess::square) current_square};
                                                    is_pinned = IsPinnedDown(b, tempo, king);
                                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                                        added_move.beg = (Chess::square) current_square;
                                                       returned.push_back(added_move);
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                        current_square += 8;
                                    }
                                }
                            }
                        }
                    }
                } else {// White
                    while (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if ( b->B[current_square].colour == Chess::WHITE ) {
                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                       returned.push_back(added_move);
                                    }
                                }
                            }
                            break;
                        }
                        current_square += 8;
                    }
                }
            }


            if ( step != 7 ) {
                //  diagonal up left 
                current_square = valid_square+7;
                while (current_square<64 && current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 4 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square += 7;
                }

                //  diagonal down right 
                current_square = valid_square-7;
                while (current_square<64 && current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 4 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square -= 7;
                }
            }


            if ( step != 9 ) {
                //  diagonal up right 
                current_square = valid_square+9;
                while (current_square<64 && current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 5 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square += 9;
                }

                //  diagonal down left 
                current_square = valid_square-9;
                while (current_square<64 && current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 5 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square -= 9;
                }
            }

            // knight presence
            if ( valid_square%8 > 1 ) {
                if ( valid_square < 48 ) {
                    if ( b->B[ valid_square+6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+6 ].colour == king.colour) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+6].piece,
                                b->B[valid_square+6].colour, (Chess::square) (valid_square+6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+15].piece,
                                b->B[valid_square+15].colour, (Chess::square) (valid_square+15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+15);
                            returned.push_back(added_move);
                        }
                    }

                    if ( valid_square%8 < 6 ) {
                        if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+17 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                    b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+17);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }

                        if ( valid_square > 15 ) {
                            if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-6 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                        b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-6);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-15 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                        b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-15);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-17 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                        b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-17);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }

                        } else if ( valid_square > 7) {// valid_square <= 15
                            if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-6 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                        b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-6);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }
                        }

                    } else if ( valid_square%8 == 6 ) {
                        if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+17 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                    b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+17);
                                returned.push_back(added_move);
                            }
                        }

                        if ( valid_square > 15 ) {
                            if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-15 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                        b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-15);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-17 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                        b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-17);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }

                        } else if ( valid_square > 7) {// valid_square <= 15
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }
                        }
                    } else {// valid_square%8 == 7
                        if ( valid_square > 15 ) {
                            if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-17 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                        b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-17);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }

                        } else if ( valid_square > 7) {// valid_square <= 15
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }
                        }
                    }
                } else if ( valid_square < 56 ) {// valid_square >= 48 so valid_square > 15
                    if ( b->B[ valid_square+6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+6].piece,
                                b->B[valid_square+6].colour, (Chess::square) (valid_square+6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+6);
                            returned.push_back(added_move);
                        }
                    }

                    if ( valid_square%8 < 6 ) {
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    } else if (valid_square%8 == 6) {
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    }// valid_square% == 7
                    if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-10);
                            returned.push_back(added_move);
                        }
                    }
                } else {// valid_square >= 56 so valid_square > 15
                    if ( valid_square%8 < 6 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    } else if ( valid_square%8 == 6 ) {
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    }
                    if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-10);
                            returned.push_back(added_move);
                        }
                    }
                }
            } else if ( valid_square%8 == 1 ) {// so valid_square%8 < 6
                if ( valid_square < 48 ) {
                    if ( b->B[ valid_square+15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+15].piece,
                                b->B[valid_square+15].colour, (Chess::square) (valid_square+15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+15);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+10);
                            returned.push_back(added_move);
                        }
                    }
                    
                    if ( valid_square > 15 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-17 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                    b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-17);
                                returned.push_back(added_move);
                            }
                        }
                    } else if ( valid_square > 7 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                    }

                } else {
                    if ( valid_square < 56 ) {// valid_square >= 48 and valid_square < 56
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }
                    }
                    if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-15);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-17);
                            returned.push_back(added_move);
                        }
                    }
                }
            } else {// valid_square%8 == 0
                if ( valid_square < 48 ) {
                    if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+10);
                            returned.push_back(added_move);
                        }
                    }
                    if ( valid_square > 15 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    } else if ( valid_square > 7 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                    }
                } else {
                    if (valid_square < 56) {
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }
                    }
                    if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-15);
                            returned.push_back(added_move);
                        }
                    }
                }
            }
        }

















    } else {
        for (valid_square=square_checking-step; valid_square != king.position; valid_square -= step) {
            added_move.end = (Chess::square) valid_square;

            if ( step != 1 ) {
                //  horizontal -
                current_square = valid_square-1;
                while (current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 3 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square --;
                }

                //  horizontal +
                current_square = valid_square+1;
                while (current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 3 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square ++;
                }
            }


            if ( step != 8 ) {
                //  vertical -
                current_square = valid_square-8;
                if ( king.colour == Chess::WHITE ) {
                    if (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if (b->B[current_square].colour == king.colour) { 
                                if ( b->B[current_square].piece == Chess::type_value::PAWN ||
                                b->B[current_square].piece == Chess::type_value::ROOK ||
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        added_move.type = Chess::type_of_move::FIRST_MOVE;
                                        returned.push_back(added_move);
                                        added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                    }
                                }
                            }
                        } else {
                            current_square -= 8;
                            if (current_square < 64) {
                                if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                    if (b->B[current_square].colour == king.colour) { 
                                        if ( b->B[current_square].piece == Chess::type_value::ROOK ||
                                        b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                returned.push_back(added_move);
                                            }
                                        } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                added_move.type = Chess::type_of_move::FIRST_MOVE;
                                                returned.push_back(added_move);
                                                added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                            }
                                        }
                                    }
                                } else {
                                    current_square -= 8;
                                    while (current_square < 64) {
                                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                            if ( b->B[current_square].colour == king.colour ) {
                                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                            b->B[current_square].colour, (Chess::square) current_square};
                                                    is_pinned = IsPinnedDown(b, tempo, king);
                                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                                        added_move.beg = (Chess::square) current_square;
                                                        returned.push_back(added_move);
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                        current_square -= 8;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    while (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if ( b->B[current_square].colour == king.colour ) {
                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                }
                            }
                            break;
                        }
                        current_square -= 8;
                    }
                }

                //  vertical +
                current_square = valid_square+8;
                if ( king.colour == Chess::BLACK ) {
                    if (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if (b->B[current_square].colour == king.colour) { 
                                if ( b->B[current_square].piece == Chess::type_value::PAWN ||
                                b->B[current_square].piece == Chess::type_value::ROOK ||
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        added_move.type = Chess::type_of_move::FIRST_MOVE;
                                        returned.push_back(added_move);
                                        added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                    }
                                }
                            }
                        } else {
                            current_square += 8;
                            if (current_square < 64) {
                                if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                    if (b->B[current_square].colour == king.colour) { 
                                        if ( b->B[current_square].piece == Chess::type_value::ROOK ||
                                        b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                returned.push_back(added_move);
                                            }
                                        } else if ( b->B[current_square].piece == Chess::type_value::STARTING_PAWN ) {
                                            tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                    b->B[current_square].colour, (Chess::square) current_square};
                                            is_pinned = IsPinnedDown(b, tempo, king);
                                            if ( is_pinned == 1 || is_pinned == 2 ) {
                                                added_move.beg = (Chess::square) current_square;
                                                added_move.type = Chess::type_of_move::FIRST_MOVE;
                                                returned.push_back(added_move);
                                                added_move.type = Chess::type_of_move::NORMAL_MOVE;
                                            }
                                        }
                                    }
                                } else {
                                    current_square += 8;
                                    while (current_square < 64) {
                                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                                            if ( b->B[current_square].colour == king.colour ) {
                                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                                            b->B[current_square].colour, (Chess::square) current_square};
                                                    is_pinned = IsPinnedDown(b, tempo, king);
                                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                                        added_move.beg = (Chess::square) current_square;
                                                        returned.push_back(added_move);
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                        current_square += 8;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    while (current_square < 64) {
                        if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                            if ( b->B[current_square].colour == king.colour ) {
                                if ( b->B[current_square].piece == Chess::type_value::ROOK || 
                                b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                    tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                            b->B[current_square].colour, (Chess::square) current_square};
                                    is_pinned = IsPinnedDown(b, tempo, king);
                                    if ( is_pinned == 1 || is_pinned == 2 ) {
                                        added_move.beg = (Chess::square) current_square;
                                        returned.push_back(added_move);
                                    }
                                }
                            }
                            break;
                        }
                        current_square += 8;
                    }
                }
            }


            if ( step != 7 ) {
                //  diagonal up left 
                current_square = valid_square+7;
                while (current_square<64 && current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 4 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square += 7;
                }

                //  diagonal down right 
                current_square = valid_square-7;
                while (current_square<64 && current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 4 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square -= 7;
                }
            }


            if ( step != 9 ) {
                //  diagonal up right 
                current_square = valid_square+9;
                while (current_square<64 && current_square%8 != 0) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 5 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square += 9;
                }

                //  diagonal down left 
                current_square = valid_square-9;
                while (current_square<64 && current_square%8 != 7) {
                    if ( b->B[current_square].piece != Chess::type_value::EMPTY ) {
                        if ( b->B[current_square].colour == king.colour ) {
                            if ( b->B[current_square].piece == Chess::type_value::BISHOP || 
                            b->B[current_square].piece == Chess::type_value::QUEEN ) {
                                tempo = (Chess::PieceAndPosition) {b->B[current_square].piece, 
                                        b->B[current_square].colour, (Chess::square) current_square};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 || is_pinned == 5 ) {
                                    added_move.beg = (Chess::square) current_square;
                                    returned.push_back(added_move);
                                }
                            }
                        }
                        break;
                    }
                    current_square -= 9;
                }
            }

            // knight presence
            if ( valid_square%8 > 1 ) {
                if ( valid_square < 48 ) {
                    if ( b->B[ valid_square+6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+6].piece,
                                b->B[valid_square+6].colour, (Chess::square) (valid_square+6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+15].piece,
                                b->B[valid_square+15].colour, (Chess::square) (valid_square+15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+15);
                            returned.push_back(added_move);
                        }
                    }

                    if ( valid_square%8 < 6 ) {
                        if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+17 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                    b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+17);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }

                        if ( valid_square > 15 ) {
                            if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-6 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                        b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-6);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-15 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                        b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-15);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-17 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                        b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-17);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }

                        } else if ( valid_square > 7) {// valid_square <= 15
                            if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-6 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                        b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-6);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }
                        }

                    } else if ( valid_square%8 == 6 ) {
                        if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+17 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                    b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+17);
                                returned.push_back(added_move);
                            }
                        }

                        if ( valid_square > 15 ) {
                            if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-15 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                        b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-15);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-17 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                        b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-17);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }

                        } else if ( valid_square > 7) {// valid_square <= 15
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }
                        }
                    } else {// valid_square%8 == 7
                        if ( valid_square > 15 ) {
                            if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-17 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                        b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-17);
                                    returned.push_back(added_move);
                                }
                            }
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }

                        } else if ( valid_square > 7) {// valid_square <= 15
                            if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                                b->B[ valid_square-10 ].colour == king.colour ) {
                                tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                        b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                                is_pinned = IsPinnedDown(b, tempo, king);
                                if ( is_pinned == 1 ) {
                                    added_move.beg = (Chess::square) (valid_square-10);
                                    returned.push_back(added_move);
                                }
                            }
                        }
                    }
                } else if ( valid_square < 56 ) {// valid_square >= 48 so valid_square > 15
                    if ( b->B[ valid_square+6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+6].piece,
                                b->B[valid_square+6].colour, (Chess::square) (valid_square+6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+6);
                            returned.push_back(added_move);
                        }
                    }

                    if ( valid_square%8 < 6 ) {
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    } else if (valid_square%8 == 6) {
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    }// valid_square% == 7
                    if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-10);
                            returned.push_back(added_move);
                        }
                    }
                } else {// valid_square >= 56 so valid_square > 15
                    if ( valid_square%8 < 6 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    } else if ( valid_square%8 == 6 ) {
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    }
                    if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-10].piece,
                                b->B[valid_square-10].colour, (Chess::square) (valid_square-10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-10);
                            returned.push_back(added_move);
                        }
                    }
                }
            } else if ( valid_square%8 == 1 ) {// so valid_square%8 < 6
                if ( valid_square < 48 ) {
                    if ( b->B[ valid_square+15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+15].piece,
                                b->B[valid_square+15].colour, (Chess::square) (valid_square+15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+15);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+10);
                            returned.push_back(added_move);
                        }
                    }
                    
                    if ( valid_square > 15 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-17 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                    b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-17);
                                returned.push_back(added_move);
                            }
                        }
                    } else if ( valid_square > 7 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                    }

                } else {
                    if ( valid_square < 56 ) {// valid_square >= 48 and valid_square < 56
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }
                    }
                    if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-15);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-17].piece,
                                b->B[valid_square-17].colour, (Chess::square) (valid_square-17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-17);
                            returned.push_back(added_move);
                        }
                    }
                }
            } else {// valid_square%8 == 0
                if ( valid_square < 48 ) {
                    if ( b->B[ valid_square+17 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+17 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+17].piece,
                                b->B[valid_square+17].colour, (Chess::square) (valid_square+17)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+17);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square+10 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square+10);
                            returned.push_back(added_move);
                        }
                    }
                    if ( valid_square > 15 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                        if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-15 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                    b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-15);
                                returned.push_back(added_move);
                            }
                        }
                    } else if ( valid_square > 7 ) {
                        if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square-6 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                    b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square-6);
                                returned.push_back(added_move);
                            }
                        }
                    }
                } else {
                    if (valid_square < 56) {
                        if ( b->B[ valid_square+10 ].piece == Chess::type_value::KNIGHT && 
                            b->B[ valid_square+10 ].colour == king.colour ) {
                            tempo = (Chess::PieceAndPosition) {b->B[valid_square+10].piece,
                                    b->B[valid_square+10].colour, (Chess::square) (valid_square+10)};
                            is_pinned = IsPinnedDown(b, tempo, king);
                            if ( is_pinned == 1 ) {
                                added_move.beg = (Chess::square) (valid_square+10);
                                returned.push_back(added_move);
                            }
                        }
                    }
                    if ( b->B[ valid_square-6 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-6 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-6].piece,
                                b->B[valid_square-6].colour, (Chess::square) (valid_square-6)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-6);
                            returned.push_back(added_move);
                        }
                    }
                    if ( b->B[ valid_square-15 ].piece == Chess::type_value::KNIGHT && 
                        b->B[ valid_square-15 ].colour == king.colour ) {
                        tempo = (Chess::PieceAndPosition) {b->B[valid_square-15].piece,
                                b->B[valid_square-15].colour, (Chess::square) (valid_square-15)};
                        is_pinned = IsPinnedDown(b, tempo, king);
                        if ( is_pinned == 1 ) {
                            added_move.beg = (Chess::square) (valid_square-15);
                            returned.push_back(added_move);
                        }
                    }
                }
            }
        }
    }



}