#include "engine.hpp"

void ChIAss::Engine::AllMovesKnight(Chess::Board* b, const Chess::PieceAndPosition p, 
            std::vector<Chess::Move>& returned_moves, const Chess::PieceAndPosition king) {
    Chess::Move added_move;
    added_move.type=Chess::type_of_move::NORMAL_MOVE;
    added_move.beg=p.position;

    ufast8 is_pinned = Engine::IsPinnedDown( b , p , king );
    if ( is_pinned != 1 ) {// a pinned knight can never move
        return ;
    }

	// TODO PERFTEST : is != faster than <
	// Note : we always end by the % comparison, since it's more costly than the basic one
	// This might be improved by analyzing games and figuring out which tests are the most common
	/*// first square
	added_move.end =  (Chess::square) ( p.position + 6 );
	returned_moves.push_back( added_move );
	// second square 
	added_move.end =  (Chess::square) ( p.position + 1 )5;
	returned_moves.push_back( added_move );
	// third square 
	added_move.end =  (Chess::square) ( p.position + 1 )7;
	returned_moves.push_back( added_move );
	// fourth square 
	added_move.end =  (Chess::square) ( p.position + 1 )0;
	returned_moves.push_back( added_move );
	// fifth square 
	added_move.end =  (Chess::square) ( p.position - 6 );
	returned_moves.push_back( added_move );
	// sixth square 
	added_move.end =  (Chess::square) ( p.position - 1 )5;
	returned_moves.push_back( added_move );
	// seventh square 
	added_move.end =  (Chess::square) ( p.position - 1 )7;
	returned_moves.push_back( added_move );
	// eighth square 
	added_move.end =  (Chess::square) ( p.position - 1 )0;
	returned_moves.push_back( added_move ); */

	// 4 up squares
    if ( p.position < 48 ) {
        if ( p.position%8 > 1 ) {
            if ( b->B[ p.position + 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 6 ].colour != p.colour ) {
// first square
                added_move.end =  (Chess::square) ( p.position + 6 );
                returned_moves.push_back( added_move );
            }
            if ( b->B[ p.position + 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 15 ].colour != p.colour ) {
// second square 
                added_move.end =  (Chess::square) ( p.position + 15);
                returned_moves.push_back( added_move );
            }

            if ( p.position%8 < 6 ) {
                if ( b->B[ p.position + 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 17 ].colour != p.colour ) {
// third square
                    added_move.end =  (Chess::square) ( p.position + 17 );
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position + 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
// fourth square 
                    added_move.end =  (Chess::square) ( p.position + 10);
                    returned_moves.push_back( added_move );
                }
            } else if ( p.position%8 == 6 ) {
                if ( b->B[ p.position + 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 17 ].colour != p.colour ) {
// third square
                    added_move.end =  (Chess::square) ( p.position + 17);
                    returned_moves.push_back( added_move );
                }
            }

        } else {// p.position%8 <= 1
            if ( p.position%8 == 1 ) { //  if not > 1, then > 0 is equivalent to == 1
                if ( b->B[ p.position + 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 15 ].colour != p.colour ) {
// second square 
                    added_move.end =  (Chess::square) ( p.position + 15);
                    returned_moves.push_back( added_move );
                }
            }
            // p.position%8 <= 1 implies that p.position%8 < 6
            if ( b->B[ p.position + 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 17 ].colour != p.colour ) {
// third square
                added_move.end =  (Chess::square) ( p.position + 17);
                returned_moves.push_back( added_move );
            }
            if ( b->B[ p.position + 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
// fourth square 
                added_move.end =  (Chess::square) ( p.position + 10);
                returned_moves.push_back( added_move );
            }
        }



        if ( p.position > 15 ) {
            if ( p.position%8 > 1 ) {
                if ( p.position%8 < 6 ) {
                    if ( b->B[ p.position - 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
                        added_move.end =  (Chess::square) ( p.position - 6 );
                        returned_moves.push_back( added_move );
                    }
                    if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                        added_move.end =  (Chess::square) ( p.position - 15);
                        returned_moves.push_back( added_move );
                    }
                } else if ( p.position%8 == 6 ) {// if not > 1, then < 7 is equivalent to == 6
                    if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                        added_move.end =  (Chess::square) ( p.position - 15);
                        returned_moves.push_back( added_move );
                    }
                }
                if ( b->B[ p.position - 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
                    added_move.end =  (Chess::square) ( p.position - 17);
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
                    added_move.end =  (Chess::square) ( p.position - 10);
                    returned_moves.push_back( added_move );
                }

            } else {
                // p.position%8 <= 1 implies p.position%8 < 6
                if ( b->B[ p.position - 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
                    added_move.end =  (Chess::square) ( p.position - 6 );
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                    added_move.end =  (Chess::square) ( p.position - 15);
                    returned_moves.push_back( added_move );
                }
                if ( p.position%8 == 1 ) {// if not > 1, then > 0 is equivalent to == 1
                    if ( b->B[ p.position - 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square 
                        added_move.end =  (Chess::square) ( p.position - 17);
                        returned_moves.push_back( added_move );
                    
                    }
                }
            }


        } else if ( p.position > 7 ) { // only the 5th and 8th moves are possible
            if ( p.position%8 < 6 ) {
                if ( b->B[ p.position - 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
                    added_move.end =  (Chess::square) ( p.position - 6 );
                    returned_moves.push_back( added_move );
                }
                if ( p.position%8 > 1 ) {
                    if ( b->B[ p.position - 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
                        added_move.end =  (Chess::square) ( p.position - 10);
                        returned_moves.push_back( added_move );
                    }
                }
            } else {// p.position%8 >= 6, then it's also > 1 , the 8th move is necessarily an option
                if ( b->B[ p.position - 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
                    added_move.end =  (Chess::square) ( p.position - 10);
                    returned_moves.push_back( added_move );
                }
            }
        }





    } else { // position >= 48, so it's > 15, only have to check for 5th through 8th moves
        if ( p.position < 56 ) {
            if ( p.position > 49 ) {
                if ( b->B[ p.position + 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 6 ].colour != p.colour ) {
// first square
                    added_move.end =  (Chess::square) ( p.position + 6 );
                    returned_moves.push_back( added_move );
                }
                if ( p.position < 54 ) {
                    if ( b->B[ p.position + 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
// fourth square 
                        added_move.end =  (Chess::square) ( p.position + 10);
                        returned_moves.push_back( added_move );
                    }
                    if ( b->B[ p.position - 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
                        added_move.end =  (Chess::square) ( p.position - 6 );
                        returned_moves.push_back( added_move );
                    }
                    if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                        added_move.end =  (Chess::square) ( p.position - 15);
                        returned_moves.push_back( added_move );
                    }
                } else if ( p.position == 54 ) {// if not > 1, then < 7 is equivalent to == 6
                    if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                        added_move.end =  (Chess::square) ( p.position - 15);
                        returned_moves.push_back( added_move );
                    }
                }
                if ( b->B[ p.position - 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
                    added_move.end =  (Chess::square) ( p.position - 17);
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
                    added_move.end =  (Chess::square) ( p.position - 10);
                    returned_moves.push_back( added_move );
                }
            } else {// p.position < 50 && p.position >= 48
                if ( b->B[ p.position + 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position + 10 ].colour != p.colour ) {
// fourth square 
                    added_move.end =  (Chess::square) ( p.position + 10);
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
                    added_move.end =  (Chess::square) ( p.position - 6 );
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                    added_move.end =  (Chess::square) ( p.position - 15);
                    returned_moves.push_back( added_move );
                }
                if ( p.position == 49 ) {
                    if ( b->B[ p.position - 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
                        added_move.end =  (Chess::square) ( p.position - 17);
                        returned_moves.push_back( added_move );
                    }
                }
            }
        } else {// p.position >= 56 , 5 through 8
            if ( p.position < 62 ) {
                if ( b->B[ p.position - 6 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 6 ].colour != p.colour ) {
// fifth square 
                    added_move.end =  (Chess::square) ( p.position - 6 );
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                    added_move.end =  (Chess::square) ( p.position - 15);
                    returned_moves.push_back( added_move );
                }
            } else if ( p.position == 62 ) {// if not > 1, then < 7 is equivalent to == 6
                if ( b->B[ p.position - 15 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 15 ].colour != p.colour ) {
// sixth square 
                    added_move.end =  (Chess::square) ( p.position - 15);
                    returned_moves.push_back( added_move );
                }
            }
            if ( p.position > 57 ) {
                if ( b->B[ p.position - 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
                    added_move.end =  (Chess::square) ( p.position - 17);
                    returned_moves.push_back( added_move );
                }
                if ( b->B[ p.position - 10 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 10 ].colour != p.colour ) {
// eighth square 
                    added_move.end =  (Chess::square) ( p.position - 10);
                    returned_moves.push_back( added_move );
                }
            } else if ( p.position == 57 ) {
                if ( b->B[ p.position - 17 ].piece == Chess::type_value::EMPTY  ||  b->B[ p.position - 17 ].colour != p.colour ) {
// seventh square
                    added_move.end =  (Chess::square) ( p.position - 17);
                    returned_moves.push_back( added_move );
                }
            }
        }
    }
}