#include "engine.hpp"

ufast8 EnPassantCase( const ChIAss::Chess::Board b, const ChIAss::Chess::square king_position );

ufast8 ChIAss::Engine::CheckCheck( Chess::Board* b, 
			const Chess::Move last_move_played, const Chess::PieceAndPosition king ) {
	using namespace ChIAss::Chess;
	using namespace ChIAss::Engine;

	unsigned char checking_square;
	unsigned char returned_check_value = 255;


// The principe is always the same : 
// first, we check if the king is in line of sight from where the piece was before its move
// -> if it is, then there's a chance that this move causes a check, by freeing the line of sight of a piece that was behind
// If the king is in the line of sight of the starting case of the move
// then we first verify if there's a piece, that was "behind" ( the meaning of behind depends of the type of line : straight or diagonal )
// if a piece is behind, we verify if it could use that new line of sight
// then, if it can, we verify if the line of sight is actually free

// Finally we check if the piece checks the king after its move


//	2.6.1 : checking that the king is up-right|down-left must happen BEFORE checking if he's up-left|down-right
//		-> the %7 test is not perfect -> a0%7 == h8%7 but they're not up-left from one another
//			they're, however, down-right from one another, so since there's an "else if", you have to put the %9 test first

/*	printf("last move : %d %d\n", last_move_played.beg, last_move_played.end);
	printf("king position : %d\n", king.position);*/
	switch ( b->B[last_move_played.end].piece ) {
		case QUEEN:
			if ( last_move_played.end%8 == king.position%8 ) {// the queen is aligned up/down with the king
				if ( last_move_played.end > king.position ) {// the danger is UP from the king
					// checking for a line of sight to the king
					checking_square = last_move_played.end - 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end ;
							}
							break;
						}
						checking_square -= 8;
					}
				} else {// the danger is DOWN from the king
				// checking for a line of sight to the king
					checking_square = last_move_played.end + 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square += 8;
					}
				}
			} else if ( last_move_played.end/8 == king.position/8 ) {// the quen is aligned left/right with the king
				if ( last_move_played.end%8 > king.position%8 ) {// the queen is right, we check left from her
					checking_square = (unsigned char) ( last_move_played.end - 1 ) ;
			// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square --;
					}
				} else {// the queen is left, we check right from her
					checking_square = (unsigned char) ( last_move_played.end + 1 );
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square ++;
					}
				}
			} else if ( last_move_played.end%9 == king.position%9 ) { // up right / down left
				if ( last_move_played.end > king.position ) { // UP RIGHT
					checking_square = (unsigned char) (last_move_played.end - 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square -= 9;
					}
				} else { // DOWN LEFT
					checking_square = (unsigned char) ( last_move_played.end + 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square += 9;
					}
				}
			} else if ( last_move_played.end%7 == king.position%7 ) { // up left / down right
				if ( last_move_played.end > king.position ) { // UP LEFT
				checking_square = (unsigned char) ( last_move_played.end - 7 );
	// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
				while ( checking_square < 64 && checking_square%8 != 0 ) {
					if ( b->B[ checking_square ].piece != EMPTY ) {
						if ( checking_square == king.position ) {
							returned_check_value = last_move_played.end;
						}
						break;
					}
					checking_square -= 7;
				}
				} else { // DOWN RIGHT
					checking_square = (unsigned char) ( last_move_played.end + 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square += 7;
					}
				}

			}
			// a queen can free a line of sight ONLY if she's just been promoted
			if (last_move_played.type == PROMOTION_QUEEN) {
				goto every_lines;
			}
			break;

		case PAWN:
			if (last_move_played.type != EN_PASSANT) {
				if ( b->B[ last_move_played.end ].colour == WHITE ) {
					if ( last_move_played.end%8 != 0 && last_move_played.end + 7 == king.position ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
							// if the pawn just advanced and check, it can't have freed a line of sight
							return last_move_played.end;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = last_move_played.end;
							goto Straight_line_of_sight;
						}
					} 
					if ( last_move_played.end%8 != 7 && last_move_played.end + 9 == king.position ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
							// if the pawn just advanced and check, it can't have freed a line of sight
							return last_move_played.end;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = last_move_played.end;
							goto Straight_line_of_sight;
						}
					}
				} else {
					if ( last_move_played.end%8 != 7 &&  last_move_played.end == king.position + 7 ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
							// if the pawn just advanced and check, it can't have freed a line of sight
							return last_move_played.end;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = last_move_played.end;
							goto Straight_line_of_sight;
						}
					}
					if ( last_move_played.end%8 != 0 && last_move_played.end == king.position + 9  ) {
						if ( last_move_played.beg%8 == last_move_played.end%8 ) {
							// if the pawn just advanced and check, it can't have freed a line of sight
							return last_move_played.end;
						} else {// if the pawn took and checked, it might have freed a straight line of sight
							returned_check_value = last_move_played.end;
							goto Straight_line_of_sight;
						}
					}
				}
				goto every_lines;// K--P--R, K-|P-|R
			} else {
				return EnPassantCase(*b, king.position);
			}
			break;

		case ROOK_CAN_CASTLE:
		case ROOK:
			if ( last_move_played.end%8 == king.position%8 ) {// the rook is aligned up/down with the king
				if ( last_move_played.end > king.position ) {// the danger is UP from the king
					// checking for a line of sight to the king
					checking_square = last_move_played.end - 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square -= 8;
					}
				} else {// the danger is DOWN from the king
				// checking for a line of sight to the king
					checking_square = last_move_played.end + 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square += 8;
					}
				}
			} else if ( last_move_played.end/8 == king.position/8 ) {// the rook is aligned left/right with the king
				if ( last_move_played.end%8 > king.position%8 ) {// the rook is RIGHT, we check left from it
					checking_square = (unsigned char) ( last_move_played.end - 1 ) ;
			// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square --;
					}
				} else {// the rook is LEFT, we check right
					checking_square = (unsigned char) ( last_move_played.end + 1 );
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square ++;
					}
				}
			}

// when checking lines of sight, we start at move.beg
			//Diagonal_line_of_sight:
				if ( last_move_played.beg%9 == king.position%9 ) {// diagonal up right
					if ( last_move_played.beg > king.position ) {// the danger is UP from the king
						// Down left
						checking_square = (unsigned char) ( last_move_played.beg - 9 );
						while ( checking_square < 64 && checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
									// Up right
									checking_square = (unsigned char) (last_move_played.beg + 9 );
									
									while ( checking_square < 64 && checking_square%8 != 0 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square += 9;
									}
								}
								break;
							}
							checking_square -= 9;
						}
					} else {// the danger is DOWN from the king
						// Up right
						checking_square = (unsigned char) ( last_move_played.beg + 9 );
						while ( checking_square < 64 && checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
									// Down left
									checking_square = (unsigned char) (last_move_played.beg - 9 );
			
									while ( checking_square < 64 && checking_square%8 != 7 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square -= 9;
									}


								}
								break;
							}
							checking_square += 9;
						}
					}
				} else if ( last_move_played.beg%7 == king.position%7 ) {// diagonal up left
					if ( last_move_played.beg > king.position ) {// the danger is UP from the king
					// Down right
						checking_square = (unsigned char) ( last_move_played.beg - 7 );
						// as long as the case is in the board : not too high, 
						// not too much on the left ( which would make it pop on the right )
						while ( checking_square < 64 && checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
								// Up left
									checking_square = (unsigned char) ( last_move_played.beg + 7 );
									// as long as the case is in the board : not too low (which would make it appear too high), 
									// not too much on the right ( which would make it pop on the left )
									while ( checking_square < 64 && checking_square%8 != 7 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square += 7;
									}
								}
								break;
							}
							checking_square -= 7;
						}
					} else {// the danger is DOWN from the king
						// Up left
						checking_square = (unsigned char) ( last_move_played.beg + 7 );	
						// as long as the case is in the board : not too low (which would make it appear too high), 
						// not too much on the right ( which would make it pop on the left )
						while ( checking_square < 64 && checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
									// UP LEFT
									checking_square = (unsigned char) ( last_move_played.beg - 7 );
									while ( checking_square < 64 && checking_square%8 != 0 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square -= 7;
									}
								}
								break;
							}
							checking_square += 7;
						}
					}
				}
			return returned_check_value;
			break;

		case BISHOP:
			if ( last_move_played.end%9 == king.position%9 ) { // up right / down left
				if ( last_move_played.end > king.position ) { // UP RIGHT
					checking_square = (unsigned char) (last_move_played.end - 9 );
// as long as the case is in the board : not too high, not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square -= 9;
					}
				} else { // DOWN LEFT
					checking_square = (unsigned char) ( last_move_played.end + 9 );
		// as long as the case is in the board : not too low (which would make it appear too high), not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square += 9;
					}
				}
			} else if ( last_move_played.end%7 == king.position%7 ) { // up left / down right
				if ( last_move_played.end > king.position ) { // UP LEFT
					checking_square = (unsigned char) ( last_move_played.end - 7 );
// as long as the case is in the board : not too high, not too much on the left ( which would make it pop on the right )
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square -= 7;
					}
				} else { // DOWN RIGHT
					checking_square = (unsigned char) ( last_move_played.end + 7 );
// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								returned_check_value = last_move_played.end;
							}
							break;
						}
						checking_square += 7;
					}
				}

			}
			// when checking lines of sight, we start at move.beg
			//	These are part of the bishop case, and used by other pieces
			Straight_line_of_sight:
				if ( last_move_played.beg%8 == king.position%8 ) {// vertical line
					// we check from the moving piece to the border
					if ( last_move_played.beg > king.position ) {// the piece is up the king
						checking_square = last_move_played.beg - 8;
						// as long as the case is in the board : not too high
						while ( checking_square < 64 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {

									checking_square = last_move_played.beg + 8;
									while ( checking_square < 64 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square += 8;
									}
								}
								break;
							}
							checking_square -= 8;
						}
					} else {
						checking_square = last_move_played.beg + 8;
						// as long as the case is in the board : not too high
						while ( checking_square < 64 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
									checking_square = last_move_played.beg - 8;

									while ( checking_square < 64 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour && 
												( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square -= 8;
									}
								}
								break;
							}
							checking_square += 8;
						}
					}
				} else if ( last_move_played.beg/8 == king.position/8 ) {// horizontal line

					if ( last_move_played.beg%8 > king.position%8 ) {// we're looking for a threat situated right from the king
						checking_square = (unsigned char) ( last_move_played.beg - 1 ) ;
						while ( checking_square%8 != 7 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
									checking_square = (unsigned char) ( last_move_played.beg + 1 );

									while ( checking_square%8 != 0 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square ++;
									}
								}
								break;
							}
							checking_square --;
						}
					} else {// we're looking for a threat situated left from the king
						checking_square = (unsigned char) ( last_move_played.beg + 1 ) ;
				// as long as the case is in the board : not too much on the right ( which would make it pop on the left )
						while ( checking_square%8 != 0 ) {
							if ( b->B[ checking_square ].piece != EMPTY ) {
								if ( checking_square == king.position ) {
									checking_square = (unsigned char) ( last_move_played.beg - 1 );

									while ( checking_square%8 != 7 ) {
										if ( b->B[ checking_square ].piece != EMPTY ) {
											if ( b->B[ checking_square ].colour != king.colour &&
												( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
												if ( returned_check_value != 255 ) {
													return 200;
												} else {
													return checking_square;
												}
											}
											break;
										}
										checking_square --;
									}
								}
								break;
							}
							checking_square ++;
						}
					}
				}

			return returned_check_value;
			break;


		case KNIGHT:
			if ( last_move_played.end%8 - king.position%8 == 1 ||
			king.position%8 - last_move_played.end%8 == 1 ) {// the knight is RIGHT/LEFT from the king
				if ( last_move_played.end/8 - king.position/8 == 2 || 
				king.position/8 - last_move_played.end/8 == 2 ) {// the knight can be UP/DOWN from the king
					returned_check_value = 100+last_move_played.end;
				}
			} else if ( last_move_played.end%8 - king.position%8 == 2 ||
			king.position%8 - last_move_played.end%8 == 2 ) {
				if ( last_move_played.end/8 - king.position/8 == 1 || 
				king.position/8 - last_move_played.end/8 == 1 ) {// the knight can be UP/DOWN from the king
					returned_check_value = 100+last_move_played.end;
				}
			}
			// The knight might have freed any line of sight
			//goto every_lines;

		case KING:
		case KING_CAN_CASTLE:
// the king can't put in check himself, he can only put another piece in position of a check
// checking for every lines of sight
		every_lines:
			if ( last_move_played.beg%8 == king.position%8 ) {// vertical line
		// we check from the moving piece to the border
				if ( last_move_played.beg > king.position ) {// the piece is up the king
					checking_square = last_move_played.beg - 8;
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

								checking_square = last_move_played.beg + 8;
								while ( checking_square < 64 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square += 8;
								}
							}
							break;
						}
						checking_square -= 8;
					}
				} else {

					checking_square = last_move_played.beg + 8;
					// as long as the case is in the board : not too high
					while ( checking_square < 64 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

							// checking for a line of sight to the king
								checking_square = last_move_played.beg - 8;
								while ( checking_square < 64 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour && 
											( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square -= 8;
								}
							}
							break;
						}
						checking_square += 8;
					}
				}
			} else if ( last_move_played.beg/8 == king.position/8 ) {// horizontal line
				if ( last_move_played.beg%8 > king.position%8 ) {// we're looking for a threat situated right from the king
					checking_square = (unsigned char) ( last_move_played.beg - 1 ) ;
					while ( checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

								checking_square = (unsigned char) ( last_move_played.beg + 1 );
								while ( checking_square%8 != 0 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square ++;
								}
							}
							break;
						}
						checking_square --;
					}
				} else {// we're looking for a threat situated left from the king
					checking_square = (unsigned char) ( last_move_played.beg + 1 ) ;
					while ( checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

								checking_square = (unsigned char) ( last_move_played.beg - 1 );
								while ( checking_square%8 != 7 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece >= ROOK_CAN_CASTLE && b->B[ checking_square ].piece <= QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square --;
								}
							}
							break;
						}
						checking_square ++;
					}
				}

			} else if ( last_move_played.beg%9 == king.position%9 ) {// diagonal up right
				if ( last_move_played.beg > king.position ) {// the danger is UP from the king

					// Down left
					checking_square = (unsigned char) ( last_move_played.beg - 9 );
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

								// Up right
								checking_square = (unsigned char) (last_move_played.beg + 9 );
								while ( checking_square < 64 && checking_square%8 != 0 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square += 9;
								}
							}
							break;
						}
						checking_square -= 9;
					}
				} else {// the danger is DOWN from the king
					// Up right
					checking_square = (unsigned char) ( last_move_played.beg + 9 );
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

								// Down left
								checking_square = (unsigned char) (last_move_played.beg - 9 );
								while ( checking_square < 64 && checking_square%8 != 7 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square -= 9;
								}
							}
							break;
						}
						checking_square += 9;
					}
				}
			} else if ( last_move_played.beg%7 == king.position%7 ) {// diagonal up left
				if ( last_move_played.beg > king.position ) {// the danger is UP from the king
					// Down right
					checking_square = (unsigned char) ( last_move_played.beg - 7 );
					while ( checking_square < 64 && checking_square%8 != 0 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {
								// Up left
								checking_square = (unsigned char) ( last_move_played.beg + 7 );
			// as long as the case is in the board : not too low (which would make it appear too high), not too much on the right ( which would make it pop on the left )
								while ( checking_square < 64 && checking_square%8 != 7 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square += 7;
								}
							}
							break;
						}
						checking_square -= 7;
					}
				} else {// the danger is DOWN from the king
					// Up left
					checking_square = (unsigned char) ( last_move_played.beg + 7 );	
					while ( checking_square < 64 && checking_square%8 != 7 ) {
						if ( b->B[ checking_square ].piece != EMPTY ) {
							if ( checking_square == king.position ) {

								// Down right
								checking_square = (unsigned char) ( last_move_played.beg - 7 );
								while ( checking_square < 64 && checking_square%8 != 0 ) {
									if ( b->B[ checking_square ].piece != EMPTY ) {
										if ( b->B[ checking_square ].colour != king.colour &&
											( b->B[ checking_square ].piece == BISHOP || b->B[ checking_square ].piece == QUEEN ) ) {// we could have a check !!
											if ( returned_check_value != 255 ) {
												return 200;
											} else {
												return checking_square;
											}
										}
										break;
									}
									checking_square -= 7;
								}
							}
							break;
						}
						checking_square += 7;
					}
				}

			}
		return returned_check_value;

		default:
			return 255;
	}
	return returned_check_value;
}


/*
	* @brief This function is pretty much the same as IsKingSafe, expect that it returns the position of the danger, if ever
	* It is used because En Passant is a particularly weird move.
*/
ufast8 EnPassantCase( const ChIAss::Chess::Board b, const ChIAss::Chess::square king_position ) {
	using namespace ChIAss::Chess;
	ufast8 returned = 255;
// UP
	u8 checking_square = king_position + 8;
	while ( checking_square < 64 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					returned = checking_square;
				}
			}
			break;
		}
		checking_square += 8;
	}
// DOWN
	checking_square = king_position - 8;
	while ( checking_square < 64 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					returned = checking_square;
				}
			}
			break;
		}
		checking_square -= 8;
	}
// RIGHT
	checking_square = king_position + 1;
	while ( checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					if ( returned == 255 ) {
						returned = checking_square;
					} else {
						return 200;
					}
				}
			}
			break;
		}
		checking_square ++;
	}
// LEFT
	checking_square = king_position - 1;
	while ( checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					if ( returned == 255 ) {
						returned = checking_square;
					} else {
						return 200;
					}
				}
			}
			break;
		}
		checking_square --;
	}

// UP LEFT
	checking_square = king_position + 7;
	while ( checking_square < 64 && checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					if ( returned == 255 ) {
						returned = checking_square;
					} else {
						return 200;
					}
				}
			}
			break;
		}
		checking_square += 7;
	}
// DOWN RIGHT
	checking_square = king_position - 7;
	while ( checking_square < 64 && checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					if ( returned == 255 ) {
						returned = checking_square;
					} else {
						return 200;
					}
				}
			}
			break;
		}
		checking_square -= 7;
	}
// UP RIGHT
	checking_square = king_position + 9;
	while ( checking_square < 64 && checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					if ( returned == 255 ) {
						returned = checking_square;
					} else {
						return 200;
					}
				}
			}
			break;
		}
		checking_square += 9;
	}
// DOWN LEFT
	checking_square = king_position - 9;
	while ( checking_square < 64 && checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					if ( returned == 255 ) {
						returned = checking_square;
					} else {
						return 200;
					}
				}
			}
			break;
		}
		checking_square -= 9;
	}


// checking for PAWN
	if ( b.B[king_position].colour == WHITE ) {
        if ( king_position < 48 ) {
            if ( king_position%8 > 0 ) {
                if ( b.B[ king_position + 7 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position + 7 ].piece <= type_value::PAWN &&
                b.B[ king_position + 7 ].colour == BLACK ) {
					if ( returned == 255 ) {
						returned = king_position + 7;
					} else {
						return 200;
					}
                }
                if ( king_position%8 < 7 ) {
                    if ( b.B[ king_position + 9 ].piece >= type_value::STARTING_PAWN && 
                    b.B[ king_position + 9 ].piece <= type_value::PAWN &&
                    b.B[ king_position + 9 ].colour == BLACK ) {
					if ( returned == 255 ) {
						returned = king_position + 9;
					} else {
						return 200;
					}
                    }
                }
            } else {
                if ( b.B[ king_position + 9 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position + 9 ].piece <= type_value::PAWN &&
                b.B[ king_position + 9 ].colour == BLACK ) {
					if ( returned == 255 ) {
						returned = king_position + 9;
					} else {
						return 200;
					}
                }
            }
        }
    } else {
        if ( king_position > 15 ) {
            if ( king_position%8 > 0 ) {
                if ( b.B[ king_position - 9 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position - 9 ].piece <= type_value::PAWN &&
                b.B[ king_position - 9 ].colour == WHITE ) {
					if ( returned == 255 ) {
						returned = king_position - 9;
					} else {
						return 200;
					}
                }
                if ( king_position%8 < 7 ) {
                    if ( b.B[ king_position - 7 ].piece >= type_value::STARTING_PAWN && 
                    b.B[ king_position - 7 ].piece <= type_value::PAWN &&
                    b.B[ king_position - 7 ].colour == WHITE ) {
						if ( returned == 255 ) {
							returned = king_position - 7;
						} else {
							return 200;
						}
                    }
                }
            } else {// %8 == 0
                if ( b.B[ king_position - 7 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position - 7 ].piece <= type_value::PAWN &&
                b.B[ king_position - 7 ].colour == WHITE ) {
					if ( returned == 255 ) {
						returned = king_position - 7;
					} else {
						return 200;
					}
                }
            }
        }
	}
	return returned;
}