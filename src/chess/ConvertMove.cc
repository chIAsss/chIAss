#include "chess.hpp"

void ChIAss::Chess::ConvertMove(ChIAss::Chess::Move& m,  const Board b) {
	m.type = NORMAL_MOVE;
	if ((b.B[m.beg].piece == KING_CAN_CASTLE || 
		b.B[m.beg].piece == ROOK_CAN_CASTLE || 
		b.B[m.beg].piece == STARTING_PAWN) &&
		(m.type != CASTLE) ) {
		m.type = FIRST_MOVE;
	}// First move

	// The castle move is actually the ROOK's move...
	if (b.B[m.beg].piece == type_value::KING_CAN_CASTLE) {
		if (b.B[m.beg].colour == WHITE ) {
            if (m.end==c1) {
                m.beg = a1;
                m.end = d1;
    			m.type = CASTLE;
            }
            if (m.end==g1) {
                m.beg = h1;
                m.end = f1;
    			m.type = CASTLE;
            }
        } else {
            if (m.end==c8) {
                m.beg = a8;
                m.end = d8;
    			m.type = CASTLE;
            }
            if (m.end==g8) {
                m.beg = h8;
                m.end = f8;
    			m.type = CASTLE;
            }
        }
	}// Castling

	if (b.B[m.beg].piece == type_value::PAWN) {
		if (b.B[m.beg].colour == WHITE) {
			if (b.B[m.end].piece == type_value::EMPTY &&
				(m.end-m.beg == 7 || m.end-m.beg == 9)) {
				m.type = type_of_move::EN_PASSANT;
			}
		}
		if (b.B[m.beg].colour==BLACK) {
			if (b.B[m.end].piece == type_value::EMPTY &&
				(m.beg-m.end == 7 || m.beg-m.end == 9)) {
				m.type = type_of_move::EN_PASSANT;
			}
		}
	}// En passant


	if ( b.B[m.beg].piece == PAWN ) {
		if (b.B[m.beg].colour == WHITE) {
            if (m.end >= 56) {
                m.type = PROMOTION_QUEEN;
            }
        } else {
            if (m.end <= 7) {
                m.type = PROMOTION_QUEEN;
            }
        }
    }// default promotion is Queen
}
