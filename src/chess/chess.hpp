#ifndef CHESS_HPP
#define CHESS_HPP

#include <cstdio>
// used for the unit testing with CUTE
#include <iostream>
// memset
#include <cstring>
#include <vector>

#include <cstdint>
typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef int_fast8_t  fast8;
typedef int_fast16_t fast16;
typedef int_fast32_t fast32;
typedef int_fast64_t fast64;
typedef uint_fast8_t  ufast8;
typedef uint_fast16_t ufast16;
typedef uint_fast32_t ufast32;
typedef uint_fast64_t ufast64;

//sleep
#include <unistd.h>


namespace ChIAss {
	namespace Chess {

/*
 * Values corresponding to the different pieces
 * The multiples values are here to evade tests when looking for AllPossibleMoves : 
 * You don't have to check if the pawn can "dash" if it's already moved
 * This only works with "starting" types, that can easily be changed
 * Otherwise, this adds tests at each move to check if the type has to be changed
 */
enum type_value : ufast8  { EMPTY=0, STARTING_PAWN=1, PAWN=2,
BISHOP=3, 
KNIGHT=4, 
ROOK_CAN_CASTLE=5, ROOK=6, 
QUEEN=7, 
KING_CAN_CASTLE=8, KING=9 };
enum square : ufast8 {  a1=0, b1=1, c1=2, d1=3, e1=4, f1=5, g1=6, h1=7,
a2= 8, b2= 9, c2=10, d2=11, e2=12, f2=13, g2=14, h2=15,
a3=16, b3=17, c3=18, d3=19, e3=20, f3=21, g3=22, h3=23,
a4=24, b4=25, c4=26, d4=27, e4=28, f4=29, g4=30, h4=31,
a5=32, b5=33, c5=34, d5=35, e5=36, f5=37, g5=38, h5=39,
a6=40, b6=41, c6=42, d6=43, e6=44, f6=45, g6=46, h6=47,
a7=48, b7=49, c7=50, d7=51, e7=52, f7=53, g7=54, h7=55,
a8=56, b8=57, c8=58, d8=59, e8=60, f8=61, g8=62, h8=63,
not_a_square=65};
enum colour : ufast8 { WHITE, BLACK };
enum type_of_move : ufast8 {NORMAL_MOVE=0, PROMOTION_QUEEN=1, PROMOTION_KNIGHT=2, FIRST_MOVE, CASTLE, EN_PASSANT};
/*
 * Basic structure corresponding to a piece of chess
 * the id of each piece is described in type_value
 * */
typedef struct _piece {
	type_value piece:7;
	ufast8 colour:1;
} Piece;
inline bool operator==(const Piece& p1, const Piece& p2) {
	return p1.piece == p2.piece && p1.colour == p2.colour;
}
std::ostream& operator<<(std::ostream& os, const Piece& p);

typedef struct _piece_position {
	type_value piece:7;
	ufast8 colour:1;
	square position;
} PieceAndPosition;
inline bool operator==(const PieceAndPosition& p1, const PieceAndPosition& p2) {
	return p1.piece == p2.piece && p1.colour == p2.colour && p1.position == p2.position;
}
std::ostream& operator<<(std::ostream& os, const PieceAndPosition& p);

/*
 * \brief Basically a board of chess
 * The 0th element is the a1 case
 * The 1st element is the a2
 * It goes on, until the 63th, which is h8
 * You access the e7 case with chess_board[e7]
 */
class Board {
	public:
		Piece B[64];
};
inline bool operator==(const Board& b1, const Board& b2) {
	for ( size_t i = 0 ; i < 64 ; i++ ) {
		if ( b1.B[i].piece != b2.B[i].piece || b1.B[i].colour != b2.B[i].colour ) {
//			printf("%d,%d != %d,%d \n", b1.B[i].piece, b1.B[i].colour==WHITE , b2.B[i].piece, b2.B[i].colour==WHITE);
			return false;
		}
	}
	return true;
}
std::ostream& operator<<(std::ostream& os, const Board& m);


//	TODO PERFTEST : shuffle the arguments, (beg type end) or (type beg end)
class Move {
	public:
		square beg;
		square end;
		type_of_move type;
};
//	TODO PERFTEST
/*class Move {
	public:
		square beg:6;
		square end:6;
		type_of_move type:4;

};*/
inline bool operator==(const Move& m1, const Move& m2) {
	return m1.beg == m2.beg && m1.end == m2.end;// && m1.type == m2.type;
}
inline bool operator!=(const Move& m1, const Move& m2) {
	return m1.beg != m2.beg || m1.end != m2.end;// || m1.type != m2.type;
}
inline bool operator==(const std::vector<Move>& v1, const std::vector<Move>& v2) {
	if (v1.size() != v2.size()) {
		return false;
	}
	for (size_t i=0;i<v1.size();i++) {
		size_t j;
		for (j=0;j<v2.size();j++) {
			if (v1[i]==v2[j]) {
				break;
			}
		}
		for (j=0;j<v1.size();j++) {
			if (v2[i]==v1[j]) {
				break;
			}
		}
		if (j==v2.size()) {
			return false;
		}
	}
	return true;
}
std::ostream& operator<<(std::ostream& os, const Move& m);


void EmptyBoard(Board* b);
void InitBoard(Board* b);
void SetPosition(Board* b, const std::vector<PieceAndPosition> pieces);
void SetPosition(Board* b, const std::vector<PieceAndPosition> pieces_w , const std::vector<PieceAndPosition> pieces_b );

void PrintBoard( const Board b );


void UpdateBoard( Board* b , const Move m );
void UpdateBoard( Board* b , const std::vector<Move> m );

/* 
 * @brief Convert a a move to its proper type. For example, the first move of a pawn, or a castle move.
 * This is necessary for ChIAss functionning.
 * @param m the move to convert.
 * @param b the board on which the move is played, to infer the type of move.
 */
void ConvertMove(ChIAss::Chess::Move& m,  const Board b);

void Play( Board* b , const Move m);
void Play( Board* b , const Move m , Piece* taken);
void Unplay( Board* b, const Move m, const Piece taken );

void Play( Board* b , const Move m , Piece* taken,
std::vector<PieceAndPosition>& your_pieces, std::vector<PieceAndPosition>& opponent_pieces);
void Unplay( Board* b, const Move m, const Piece taken, 
std::vector<PieceAndPosition>& your_pieces, std::vector<PieceAndPosition>& opponent_pieces);

/*
	* @brief function telling if the king is in check
	* @param b the board
	* @param king_position the square of the king
	* @return true or false
*/
bool IsKingSafe( const Board b , const square king_position );

/*
	* @brief Check if a move, given in standard form, is legal
	* @param b the board
	* @param m the move to check given in (first case, second case) format
	* @param last_move_played the last move that's been played, when it's important (for En-Passant for example)
	* @return true if the move is legal, false if it's illegal
*/
bool IsMoveLegal(Board b, Move* m, const Move last_played);

	}
}

#endif // CHESS_HPP