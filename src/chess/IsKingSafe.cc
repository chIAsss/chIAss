#include "chess.hpp"

bool ChIAss::Chess::IsKingSafe( const Board b , const square king_position ) {
// UP
	u8 checking_square = king_position + 8;
	while ( checking_square < 64 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square += 8;
	}
// DOWN
	checking_square = king_position - 8;
	while ( checking_square < 64 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square -= 8;
	}
// RIGHT
	checking_square = king_position + 1;
	while ( checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square ++;
	}
// LEFT
	checking_square = king_position - 1;
	while ( checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece >= type_value::ROOK_CAN_CASTLE &&
				b.B[ checking_square ].piece <= type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square --;
	}

// UP LEFT
	checking_square = king_position + 7;
	while ( checking_square < 64 && checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square += 7;
	}
// DOWN RIGHT
	checking_square = king_position - 7;
	while ( checking_square < 64 && checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square -= 7;
	}
// UP RIGHT
	checking_square = king_position + 9;
	while ( checking_square < 64 && checking_square%8 != 0 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square += 9;
	}
// DOWN LEFT
	checking_square = king_position - 9;
	while ( checking_square < 64 && checking_square%8 != 7 ) {
		if ( b.B[ checking_square ].piece != type_value::EMPTY ) {
			if ( b.B[ checking_square ].colour != b.B[king_position].colour ) {
				if ( b.B[ checking_square ].piece == type_value::BISHOP ||
				b.B[ checking_square ].piece == type_value::QUEEN ) {
					return false;
				}
			}
			break;
		}
		checking_square -= 9;
	}


// checking for KNIGHT
    if ( king_position%8 > 1 ) {
	    if ( king_position < 48 ) {
            if ( b.B[ king_position + 6 ].piece == type_value::KNIGHT && b.B[ king_position + 6 ].colour != b.B[king_position].colour ) {
                return false;// first square
            }
            if ( b.B[ king_position + 15 ].piece == type_value::KNIGHT && b.B[ king_position + 15 ].colour != b.B[king_position].colour ) {
                return false;// second square 
            }

            if ( king_position%8 < 6 ) {
                if ( b.B[ king_position + 17 ].piece == type_value::KNIGHT && b.B[ king_position + 17 ].colour != b.B[king_position].colour ) {
                    return false;// third square
                }
                if ( b.B[ king_position + 10 ].piece == type_value::KNIGHT && b.B[ king_position + 10 ].colour != b.B[king_position].colour ) {
                    return false;// fourth square 
                }

                if ( king_position > 15 ) {
                    if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                        return false;// fifth square 
                    }
                    if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                        return false;// sixth square 
                    }
                    if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                        return false;// seventh square
                    }
                    if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                        return false;// eighth square 
                    }

                } else if ( king_position > 7) {// king_position <= 15
                    if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                        return false;// fifth square 
                    }
                    if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                        return false;// eighth square 
                    }
                }

            } else if ( king_position%8 == 6 ) {
                if ( b.B[ king_position + 17 ].piece == type_value::KNIGHT && b.B[ king_position + 17 ].colour != b.B[king_position].colour ) {
                    return false;// third square
                }

                if ( king_position > 15 ) {
                    if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                        return false;// sixth square 
                    }
                    if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                        return false;// seventh square
                    }
                    if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                        return false;// eighth square 
                    }

                } else if ( king_position > 7) {// king_position <= 15
                    if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                        return false;// eighth square 
                    }
                }
            } else {// king_position%8 == 7
                if ( king_position > 15 ) {
                    if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                        return false;// seventh square
                    }
                    if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                        return false;// eighth square 
                    }

                } else if ( king_position > 7) {// king_position <= 15
                    if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                        return false;// eighth square 
                    }
                }
            }
        } else if ( king_position < 56 ) {// king_position >= 48 so king_position > 15
            if ( b.B[ king_position + 6 ].piece == type_value::KNIGHT && b.B[ king_position + 6 ].colour != b.B[king_position].colour ) {
                return false;// first square
            }

            if ( king_position%8 < 6 ) {
                if ( b.B[ king_position + 10 ].piece == type_value::KNIGHT && b.B[ king_position + 10 ].colour != b.B[king_position].colour ) {
                    return false;// fourth square 
                }
                if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                    return false;// fifth square 
                }
                if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                    return false;// sixth square 
                }
            } else if (king_position%8 == 6) {
                if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                    return false;// sixth square 
                }
            }// king_position% == 7
            if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                return false;// seventh square
            }
            if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                return false;// eighth square 
            }
        } else {// king_position >= 56 so king_position > 15
            if ( king_position%8 < 6 ) {
                if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                    return false;// fifth square 
                }
                if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                    return false;// sixth square 
                }
            } else if ( king_position%8 == 6 ) {
                if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                    return false;// sixth square 
                }
            }
            if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                return false;// seventh square
            }
            if ( b.B[ king_position - 10 ].piece == type_value::KNIGHT && b.B[ king_position - 10 ].colour != b.B[king_position].colour ) {
                return false;// eighth square 
            }
        }
    } else if ( king_position%8 == 1 ) {// so king_position%8 < 6
	    if ( king_position < 48 ) {
            if ( b.B[ king_position + 15 ].piece == type_value::KNIGHT && b.B[ king_position + 15 ].colour != b.B[king_position].colour ) {
                return false;// second square 
            }
            if ( b.B[ king_position + 17 ].piece == type_value::KNIGHT && b.B[ king_position + 17 ].colour != b.B[king_position].colour ) {
                return false;// third square
            }
            if ( b.B[ king_position + 10 ].piece == type_value::KNIGHT && b.B[ king_position + 10 ].colour != b.B[king_position].colour ) {
                return false;// fourth square 
            }
            
            if ( king_position > 15 ) {
                if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                    return false;// fifth square 
                }
                if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                    return false;// sixth square 
                }
                if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                    return false;// seventh square
                }
            } else if ( king_position > 7 ) {
                if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                    return false;// fifth square 
                }
            }

        } else {
            if ( king_position < 56 ) {// king_position >= 48 and king_position < 56
                if ( b.B[ king_position + 10 ].piece == type_value::KNIGHT && b.B[ king_position + 10 ].colour != b.B[king_position].colour ) {
                    return false;// fourth square 
                }
            }
            if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                return false;// fifth square 
            }
            if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                return false;// sixth square 
            }
            if ( b.B[ king_position - 17 ].piece == type_value::KNIGHT && b.B[ king_position - 17 ].colour != b.B[king_position].colour ) {
                return false;// seventh square
            }
        }
    } else {// king_position%8 == 0
	    if ( king_position < 48 ) {
            if ( b.B[ king_position + 17 ].piece == type_value::KNIGHT && b.B[ king_position + 17 ].colour != b.B[king_position].colour ) {
                return false;// third square
            }
            if ( b.B[ king_position + 10 ].piece == type_value::KNIGHT && b.B[ king_position + 10 ].colour != b.B[king_position].colour ) {
                return false;// fourth square 
            }
            if ( king_position > 15 ) {
                if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                    return false;// fifth square 
                }
                if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                    return false;// sixth square 
                }
            } else if ( king_position > 7 ) {
                if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                    return false;// fifth square 
                }
            }
        } else {
            if (king_position < 56) {
                if ( b.B[ king_position + 10 ].piece == type_value::KNIGHT && b.B[ king_position + 10 ].colour != b.B[king_position].colour ) {
                    return false;// fourth square 
                }
            }
            if ( b.B[ king_position - 6 ].piece == type_value::KNIGHT && b.B[ king_position - 6 ].colour != b.B[king_position].colour ) {
                return false;// fifth square 
            }
            if ( b.B[ king_position - 15 ].piece == type_value::KNIGHT && b.B[ king_position - 15 ].colour != b.B[king_position].colour ) {
                return false;// sixth square 
            }
        }
    }


// checking for PAWN
	if ( b.B[king_position].colour == WHITE ) {
        if ( king_position < 48 ) {
            if ( king_position%8 > 0 ) {
                if ( b.B[ king_position + 7 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position + 7 ].piece <= type_value::PAWN &&
                b.B[ king_position + 7 ].colour == BLACK ) {
                    return false;
                }
                if ( king_position%8 < 7 ) {
                    if ( b.B[ king_position + 9 ].piece >= type_value::STARTING_PAWN && 
                    b.B[ king_position + 9 ].piece <= type_value::PAWN &&
                    b.B[ king_position + 9 ].colour == BLACK ) {
                        return false;
                    }
                }
            } else {
                if ( b.B[ king_position + 9 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position + 9 ].piece <= type_value::PAWN &&
                b.B[ king_position + 9 ].colour == BLACK ) {
                    return false;
                }
            }
        }
    } else {
        if ( king_position > 15 ) {
            if ( king_position%8 > 0 ) {
                if ( b.B[ king_position - 9 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position - 9 ].piece <= type_value::PAWN &&
                b.B[ king_position - 9 ].colour == WHITE ) {
                    return false;
                }
                if ( king_position%8 < 7 ) {
                    if ( b.B[ king_position - 7 ].piece >= type_value::STARTING_PAWN && 
                    b.B[ king_position - 7 ].piece <= type_value::PAWN &&
                    b.B[ king_position - 7 ].colour == WHITE ) {
                        return false;
                    }
                }
            } else {// %8 == 0
                if ( b.B[ king_position - 7 ].piece >= type_value::STARTING_PAWN && 
                b.B[ king_position - 7 ].piece <= type_value::PAWN &&
                b.B[ king_position - 7 ].colour == WHITE ) {
                    return false;
                }
            }
        }
	}

// Checking for KING

    if ( king_position%8 > 0 ) {
        if ( b.B[ king_position - 1 ].piece >= type_value::KING_CAN_CASTLE ) {
            return false;
        }
        if ( king_position < 56 ) {
            if ( b.B[ king_position + 7 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            if ( b.B[ king_position + 8 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            if ( king_position > 7 ) {
                if ( b.B[ king_position - 8 ].piece >= type_value::KING_CAN_CASTLE ) {
                    return false;
                }
                if ( b.B[ king_position - 9 ].piece >= type_value::KING_CAN_CASTLE ) {
                    return false;
                }
                if ( king_position%8 < 7 ) {
                    if ( b.B[ king_position - 7 ].piece >= type_value::KING_CAN_CASTLE ) {
                        return false;
                    }
                    if ( b.B[ king_position + 1 ].piece >= type_value::KING_CAN_CASTLE ) {
                        return false;
                    }
                    if ( b.B[ king_position + 9 ].piece >= type_value::KING_CAN_CASTLE ) {
                        return false;
                    }
                }
            } else {
                if ( king_position%8 < 7 ) {
                    if ( b.B[ king_position + 1 ].piece >= type_value::KING_CAN_CASTLE ) {
                        return false;
                    }
                    if ( b.B[ king_position + 9 ].piece >= type_value::KING_CAN_CASTLE ) {
                        return false;
                    }
                }
            }
        } else {
            if ( b.B[ king_position - 8 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            if ( b.B[ king_position - 9 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            if ( king_position%8 < 7 ) {
                if ( b.B[ king_position - 7 ].piece >= type_value::KING_CAN_CASTLE ) {
                    return false;
                }
                if ( b.B[ king_position + 1 ].piece >= type_value::KING_CAN_CASTLE ) {
                    return false;
                }
            }
        }
    } else {
        if ( b.B[ king_position + 1 ].piece >= type_value::KING_CAN_CASTLE ) {
            return false;
        }
        if ( king_position < 56 ) {
            if ( b.B[ king_position + 8 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            if ( b.B[ king_position + 9 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            
            if ( king_position > 7 ) {
                if ( b.B[ king_position - 8 ].piece >= type_value::KING_CAN_CASTLE ) {
                    return false;
                }
                if ( b.B[ king_position - 7 ].piece >= type_value::KING_CAN_CASTLE ) {
                    return false;
                }
            }
        } else {
            if ( b.B[ king_position - 8 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
            if ( b.B[ king_position - 7 ].piece >= type_value::KING_CAN_CASTLE ) {
                return false;
            }
        }
    }


	return true;
}