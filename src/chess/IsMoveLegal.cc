#include "chess.hpp"

bool ChIAss::Chess::IsMoveLegal(Board b, Move* m, const Move last_played) {
    if ( m->beg == m->end || m->beg > 63 || m->end > 63 ) {
        return false;
    }
    m->type = NORMAL_MOVE;
    ufast8 checking_square;

    switch ( b.B[m->beg].piece ) {
//  Not opti : who cares?
        case type_value::STARTING_PAWN:
            m->type = FIRST_MOVE;
            if ( b.B[m->beg].colour == WHITE ) {
                if ( m->end - m->beg == 16 && b.B[m->beg+8].piece==type_value::EMPTY && b.B[m->beg+16].piece==type_value::EMPTY ) {
                    return true;
                }
            } else {
                if ( m->beg - m->end == 16 && b.B[m->beg-8].piece==type_value::EMPTY && b.B[m->beg-16].piece==type_value::EMPTY ) {
                    return true;
                }
            }
        case type_value::PAWN:
            if ( b.B[m->beg].colour == WHITE ) {
                if ( m->end/8 == 7 ) {
                    m->type = PROMOTION_QUEEN;
                }
                if ( m->end - m->beg == 8 && b.B[m->beg+8].piece==type_value::EMPTY ) {
                    return true;
                }
                if ( m->beg%8 > 0 && m->end-m->beg==7 ) {
                    if (b.B[m->end].colour==BLACK) {
                        return true;
                    }
                    if (last_played.type==FIRST_MOVE && last_played.end==m->beg-1 && 
                        b.B[last_played.end].piece==type_value::PAWN) {
                        m->type = EN_PASSANT;
                        return true;
                    }
                }

                if ( m->beg%8 < 7 && m->end-m->beg==9 ) {
                    if (b.B[m->end].colour==BLACK) {
                        return true;
                    }
                    if (last_played.type==FIRST_MOVE && last_played.end==m->beg+1 && 
                        b.B[last_played.end].piece==type_value::PAWN) {
                        m->type = EN_PASSANT;
                        return true;
                    }
                }
            } else {
                if ( m->end/8 == 0 ) {
                    m->type = PROMOTION_QUEEN;
                }
                if ( m->beg - m->end == 8 && b.B[m->beg-8].piece == type_value::EMPTY ) {
                    return true;
                }
                if ( m->beg%8>0 && m->beg-m->end==9 ) {
                    if (b.B[m->end].colour==WHITE && b.B[m->end].piece!=type_value::EMPTY) {
                        return true;
                    }
                    if (last_played.type==FIRST_MOVE && last_played.end==m->beg-1 && 
                        b.B[last_played.end].piece==type_value::PAWN) {
                        m->type = EN_PASSANT;
                        return true;
                    }
                }

                if ( m->beg%8<7 && m->beg-m->end==7 ) {
                    if (b.B[m->end].colour==WHITE && b.B[m->end].piece!=type_value::EMPTY) {
                        return true;
                    }
                    if (last_played.type==FIRST_MOVE && last_played.end==m->beg+1 && 
                        b.B[last_played.end].piece==type_value::PAWN) {
                        m->type = EN_PASSANT;
                        return true;
                    }
                }
            }
            return false;
        break;
        case type_value::KNIGHT:
            if ( b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                if ( m->end%8-m->beg%8==1 && m->end/8-m->beg/8==2 && m->beg%8<7 && m->beg<48 ) {
                    return true;
                }
                if ( m->end%8-m->beg%8==2 && m->end/8-m->beg/8==1 && m->beg%8<6 && m->beg<56 ) {
                    return true;
                }

                if ( m->end%8-m->beg%8==2 && m->beg/8-m->end/8==1 && m->beg%8<6 && m->beg>7 ) {
                    return true;
                }
                if ( m->end%8-m->beg%8==1 && m->beg/8-m->end/8==2 && m->beg%8<7 && m->beg>15 ) {
                    return true;
                }

                if ( m->beg%8-m->end%8==1 && m->beg/8-m->end/8==2 && m->beg%8>0 && m->beg>15 ) {
                    return true;
                }
                if ( m->beg%8-m->end%8==2 && m->beg/8-m->end/8==1 && m->beg%8>1 && m->beg>7 ) {
                    return true;
                }

                if ( m->beg%8-m->end%8==2 && m->end/8-m->beg/8==1 && m->beg%8>1 && m->beg<56 ) {
                    return true;
                }
                if ( m->beg%8-m->end%8==1 && m->end/8-m->beg/8==2 && m->beg%8>0 && m->beg<48 ) {
                    return true;
                }
            }
            return false;
        break;
        break;
        case type_value::ROOK_CAN_CASTLE:
            m->type = FIRST_MOVE;
        case type_value::ROOK:
            if (m->end > m->beg ) {
//  UP
                if (m->end%8 == m->beg%8) {
                    checking_square = m->beg + 8;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square += 8;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                } else if (m->end/8 == m->beg/8) {
//  RIGHT
                    checking_square = m->beg + 1;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square ++;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                }
            } else {
//  DOWN
                if (m->end%8 == m->beg%8) {
                    checking_square = m->beg - 8;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square -= 8;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                } else if (m->end/8 == m->beg/8) {
//  LEFT
                    checking_square = m->beg - 1;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square --;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                }
            }
            return false;
        break;
        case type_value::QUEEN:
            if (m->end > m->beg ) {
//  UP
                if (m->end%8 == m->beg%8) {
                    checking_square = m->beg + 8;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square += 8;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                } else if (m->end/8 == m->beg/8) {
//  RIGHT
                    checking_square = m->beg + 1;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square ++;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                }
            } else {
//  DOWN
                if (m->end%8 == m->beg%8) {
                    checking_square = m->beg - 8;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square -= 8;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                } else if (m->end/8 == m->beg/8) {
//  LEFT
                    checking_square = m->beg - 1;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square --;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                }
            }
//  NO BREAK OR RETURN HERE
        case type_value::BISHOP:
            if (m->end > m->beg ) {
//  UP RIGHT
                if (m->end%8-m->beg%8 == m->end/8-m->beg/8) {
                    checking_square = m->beg + 9;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square += 9;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }

                } else if (m->beg%8-m->end%8 == m->end/8-m->beg/8) {
//  UP LEFT
                    checking_square = m->beg + 7;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square += 7;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }

                }
            } else {
//  DOWN LEFT
                if (m->beg%8-m->end%8 == m->beg/8-m->end/8) {
                    checking_square = m->beg - 9;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square -= 9;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }

                } else if (m->end%8-m->beg%8 == m->beg/8-m->end/8) {
//  DOWN RIGHT
                    checking_square = m->beg - 7;
                    while ( checking_square != m->end ) {
                        if (b.B[checking_square].piece != type_value::EMPTY) {
                            return false;
                        }
                        checking_square -= 7;
                    }
                    if (b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                        return true;
                    }
                }
            }
            return false;
        case type_value::KING_CAN_CASTLE:
            m->type = CASTLE;
            if (b.B[m->beg].colour == WHITE) {
                if (m->end == c1) {
                    if (b.B[d1].piece==type_value::EMPTY && b.B[c1].piece==type_value::EMPTY &&
                    b.B[b1].piece==type_value::EMPTY && b.B[a1].piece==type_value::ROOK_CAN_CASTLE) {
                        m->beg = a1;
                        m->end = d1;
                        return true;
                    }
                }
                if (m->end == g1) {
                    if (b.B[f1].piece==type_value::EMPTY && b.B[g1].piece==type_value::EMPTY && b.B[h1].piece==type_value::ROOK_CAN_CASTLE) {
                        m->beg = h1;
                        m->end = f1;
                        return true;
                    }
                }
            } else {
                if (m->end == c8) {
                    if (b.B[d8].piece==type_value::EMPTY && b.B[c8].piece==type_value::EMPTY &&
                    b.B[b8].piece==type_value::EMPTY && b.B[a8].piece==type_value::ROOK_CAN_CASTLE) {
                        m->beg = a8;
                        m->end = d8;
                        return true;
                    }
                } 
                if (m->end == g8) {
                    if (b.B[f8].piece==type_value::EMPTY && b.B[g8].piece==type_value::EMPTY && b.B[h8].piece==type_value::ROOK_CAN_CASTLE) {
                        m->beg = h8;
                        m->end = f8;
                        return true;
                    }
                }
            }
            m->type = FIRST_MOVE;
        case type_value::KING:
            if ( (m->beg/8-m->end/8 == 1 || m->beg/8 == m->end/8 || m->end/8-m->beg/8==1) &&
            (m->beg%8-m->end%8 == 1 || m->beg%8 == m->end%8 || m->end%8-m->beg%8==1) ) {
                if ( b.B[m->end].piece == type_value::EMPTY || b.B[m->end].colour != b.B[m->beg].colour ) {
                    Piece taken;
                    Play(&b, *m, &taken);
                    if ( IsKingSafe(b, m->end) ) {
                        Unplay(&b, *m, taken);
                        return true;
                    }
                    Unplay(&b, *m, taken);
                }
            }
            return false;
        break;
        default:// type_value::EMPTY
            return false;
    }

    return false;
}