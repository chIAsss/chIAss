#include "chess.hpp"


void ChIAss::Chess::Unplay( Board* b, const Move m, const Piece taken ) {
	b->B[m.beg] = b->B[m.end];
	switch ( m.type ) {
		case NORMAL_MOVE:// Normal move
		break;
		case PROMOTION_QUEEN:// Promotion QUEEN
		case PROMOTION_KNIGHT:// KNIGHT promotion
			b->B[m.beg].piece = PAWN;
		break;
		case FIRST_MOVE:
			b->B[m.beg].piece = (type_value) (b->B[m.beg].piece-1);
		break;
		case CASTLE:// Castle or first ROOK/KING move
			b->B[m.end] = { EMPTY, WHITE };
			switch ( m.end ) {
				case d1:// WHITE LEFT 
					b->B[e1] = { KING_CAN_CASTLE, WHITE };
					b->B[a1] = { ROOK_CAN_CASTLE, WHITE };
					b->B[c1] = { EMPTY, WHITE };
					return;
				case f1:// WHITE RIGHT 
					b->B[e1] = { KING_CAN_CASTLE, WHITE };
					b->B[h1] = { ROOK_CAN_CASTLE, WHITE };
					b->B[g1] = { EMPTY, WHITE };
					return;
				case d8:// BLACK LEFT 
					b->B[e8] = { KING_CAN_CASTLE, BLACK };
					b->B[a8] = { ROOK_CAN_CASTLE, BLACK };
					b->B[c8] = { EMPTY, WHITE };
					return;
				case f8:// BLACK RIGHT 
					b->B[e8] = { KING_CAN_CASTLE, BLACK };
					b->B[h8] = { ROOK_CAN_CASTLE, BLACK };
					b->B[g8] = { EMPTY, WHITE };
					return;
				default:
					printf("Error in Unplay Castle %d\n",m.end);
					break;
			}
		break;
		case EN_PASSANT:// En Passant
			if (b->B[m.beg].colour == WHITE ) {
				b->B[m.end-8] = {PAWN, BLACK};
			} else {
				b->B[m.end+8] = {PAWN, WHITE};
			}
			b->B[m.end] = {EMPTY, WHITE};
			return;
		default :
			fprintf(stderr, "Error during Unplay function : m = (%d,%d,%d)\n", m.beg, m.end, m.type);
	}
	b->B[m.end] = taken;
}


 
void ChIAss::Chess::Unplay( Board* b, const Move m, const Piece taken, 
			std::vector<PieceAndPosition>& your_pieces, 
			std::vector<PieceAndPosition>& opponent_pieces) {
	b->B[m.beg] = b->B[m.end];
	size_t index_played;
	for (size_t i=0;i<your_pieces.size();i++) {
		if (your_pieces[i].position==m.end) {
			index_played=i;
			your_pieces[i].position=m.beg;
			break;
		}
	}

	PieceAndPosition filler;
	filler.piece = taken.piece;
	filler.colour = taken.colour;
	filler.position = m.end;

	switch ( m.type ) {
		case NORMAL_MOVE:
		break;
		case PROMOTION_QUEEN:
		case PROMOTION_KNIGHT:
			b->B[m.beg].piece = PAWN;
			your_pieces[index_played].piece = PAWN;
		break;
		case FIRST_MOVE:
			b->B[m.beg].piece = (type_value) (b->B[m.beg].piece-1);
			your_pieces[index_played].piece = b->B[m.beg].piece;
		break;
		case CASTLE:
			your_pieces[0].piece = KING_CAN_CASTLE;
			your_pieces[index_played].piece = ROOK_CAN_CASTLE;
			b->B[m.end] = { EMPTY, WHITE };
			switch ( m.end ) {
				case d1 :// WHITE LEFT 
					b->B[a1] = { ROOK_CAN_CASTLE, WHITE };
					b->B[e1] = { KING_CAN_CASTLE, WHITE };
					b->B[c1] = { EMPTY, WHITE };
					
					your_pieces[0].position = e1;
					return;
				case f1 :// WHITE RIGHT 
					b->B[h1] = { ROOK_CAN_CASTLE, WHITE };
					b->B[e1] = { KING_CAN_CASTLE, WHITE };
					b->B[g1] = { EMPTY, WHITE };
					
					your_pieces[0].position = e1;
					return;
				case d8 :// BLACK LEFT 
					b->B[a8] = { ROOK_CAN_CASTLE, BLACK };
					b->B[e8] = { KING_CAN_CASTLE, BLACK };
					b->B[c8] = { EMPTY, WHITE };

					your_pieces[0].position = e8;
					return;
				case f8 :// BLACK RIGHT 
					b->B[h8] = { ROOK_CAN_CASTLE, BLACK };
					b->B[e8] = { KING_CAN_CASTLE, BLACK };
					b->B[g8] = { EMPTY, WHITE };

					your_pieces[0].position = e8;
					return;
				default:
					printf("Error in Play Castle %d\n",m.end);
					break;
			}
		break;
		case EN_PASSANT:// En Passant
			if (b->B[m.beg].colour == WHITE ) {
				filler.position = (square) (m.end-8);
			} else {
				filler.position = (square) (m.end+8);
			}
			b->B[m.end] = {EMPTY,WHITE};
			break;
		default :
			fprintf(stderr, "Error during Unplay (vector) function : m = (%d,%d,%d)\n", m.beg, m.end, m.type);
	}
	b->B[filler.position] = taken;
	if (taken.piece != EMPTY) {
		opponent_pieces.push_back(filler);
	}
}