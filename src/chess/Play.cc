#include "chess.hpp"

/*
	* \brief Plays a move on a board, which is not meant to be un-played
	This function is meant to be called a single time at each turn.
	The type of move might not be provided, the function will autocomplete it...
	* @param b pointer to the board on which to play the move
	* @param m the move to be played
*/
void ChIAss::Chess::Play( Board* b , const Move m) {
	b->B[m.end] = b->B[m.beg];
	b->B[m.beg] = {type_value::EMPTY,WHITE};
	switch ( m.type ) {
		case type_of_move::NORMAL_MOVE:// Normal move
//			b->B[m.end] = b->B[m.beg];
//			b->B[m.beg] = {type_value::EMPTY,WHITE};
		break;
		case type_of_move::PROMOTION_QUEEN:// Promotion QUEEN
			b->B[m.end].piece = type_value::QUEEN;
		break;
		case type_of_move::PROMOTION_KNIGHT:// KNIGHT promotion
			b->B[m.end].piece = type_value::KNIGHT;
		break;
		case type_of_move::FIRST_MOVE:
			b->B[m.end].piece = (type_value) (b->B[m.end].piece+1);
		break;
		case type_of_move::CASTLE:// Castle or first ROOK/KING move
			switch ( m.end ) {
				case d1:// WHITE LEFT 
					b->B[e1] = { type_value::EMPTY, WHITE };
					b->B[c1] = { type_value::KING, WHITE};
					b->B[d1] = { type_value::ROOK, WHITE };
					if ( b->B[h1].piece == type_value::ROOK_CAN_CASTLE ) {
						b->B[h1].piece = type_value::ROOK;
					}
					return;
				case f1:// WHITE RIGHT 
					b->B[e1] = { type_value::EMPTY, WHITE };
					b->B[g1] = { type_value::KING, WHITE};
					b->B[f1] = { type_value::ROOK, WHITE };
					if ( b->B[a1].piece == type_value::ROOK_CAN_CASTLE ) {
						b->B[a1].piece = type_value::ROOK;
					}
					return;
				case d8:// BLACK LEFT 
					b->B[e8] = { type_value::EMPTY, WHITE };
					b->B[c8] = { type_value::KING, BLACK};
					b->B[d8] = { type_value::ROOK, BLACK };
					if ( b->B[h8].piece == type_value::ROOK_CAN_CASTLE ) {
						b->B[h8].piece = type_value::ROOK;
					}
					return;
				case f8:// BLACK RIGHT 
					b->B[e8] = { type_value::EMPTY, WHITE };
					b->B[g8] = { type_value::KING, BLACK};
					b->B[f8] = { type_value::ROOK, BLACK };
					if ( b->B[a8].piece == type_value::ROOK_CAN_CASTLE ) {
						b->B[a8].piece = type_value::ROOK;
					}
					return;
				default:
					printf("Error in Play Castle %d\n",m.end);
					break;
			}
		break;
		case type_of_move::EN_PASSANT:// En Passant
			if (b->B[m.end].colour ==WHITE ) {
				b->B[m.end-8]={type_value::EMPTY, WHITE};
			} else {
				b->B[m.end+8]={type_value::EMPTY, WHITE};
			}
		break;
		default :
			fprintf(stderr, "Error during Play function : m = (%d,%d,%d)\n", m.beg, m.end, m.type);
	}
}
/*
	* \brief Plays a move on a board
	* @param b pointer to the board on which to play the move
	* @param m the move to be played
	* @param taken A piece, corresponding to the piece taken, if ever 
*/
void ChIAss::Chess::Play( Board* b, const Move m, Piece* taken) {
	*taken = b->B[m.end];
	b->B[m.end] = b->B[m.beg];
	b->B[m.beg] = {type_value::EMPTY,WHITE};
	switch ( m.type ) {
		case type_of_move::NORMAL_MOVE:// Normal move
//			b->B[m.end] = b->B[m.beg];
//			b->B[m.beg] = {type_value::EMPTY,WHITE};
		break;
		case type_of_move::PROMOTION_QUEEN:// Promotion QUEEN
			b->B[m.end].piece = type_value::QUEEN;
		break;
		case type_of_move::PROMOTION_KNIGHT:// KNIGHT promotion
			b->B[m.end].piece = type_value::KNIGHT;
		break;
		case type_of_move::FIRST_MOVE:
			b->B[m.end].piece = (type_value) (b->B[m.end].piece+1);
		break;
		case type_of_move::CASTLE:// Castle or first ROOK/KING move
			switch ( m.end ) {
				case d1:// WHITE LEFT 
					b->B[e1] = { type_value::EMPTY, WHITE };
					b->B[c1] = { type_value::KING, WHITE};
					b->B[d1] = { type_value::ROOK, WHITE };
					return;
				case f1:// WHITE RIGHT 
					b->B[e1] = { type_value::EMPTY, WHITE };
					b->B[g1] = { type_value::KING, WHITE};
					b->B[f1] = { type_value::ROOK, WHITE };
					return;
				case d8:// BLACK LEFT 
					b->B[e8] = { type_value::EMPTY, WHITE };
					b->B[c8] = { type_value::KING, BLACK};
					b->B[d8] = { type_value::ROOK, BLACK };
					return;
				case f8:// BLACK RIGHT 
					b->B[e8] = { type_value::EMPTY, WHITE };
					b->B[g8] = { type_value::KING, BLACK};
					b->B[f8] = { type_value::ROOK, BLACK };
					return;
				default:
					printf("Error in Play Castle %d\n",m.end);
					break;
			}
		break;
		case type_of_move::EN_PASSANT:// En Passant
			if (b->B[m.end].colour ==WHITE ) {
				*taken={type_value::PAWN,BLACK};
				b->B[m.end-8]={type_value::EMPTY, WHITE};
			} else {
				*taken={type_value::PAWN,WHITE};
				b->B[m.end+8]={type_value::EMPTY, WHITE};
			}
		break;
		default :
			fprintf(stderr, "Error during Play function : m = (%d,%d,%d)\n", m.beg, m.end, m.type);
	}
}


void ChIAss::Chess::Play( Board* b, const Move m, Piece* taken,
		std::vector<PieceAndPosition>& your_pieces, std::vector<PieceAndPosition>& opponent_pieces) {
	*taken = b->B[m.end];
//	debug
#ifdef CHIASS_DEBUG
	if (taken->piece >= KING_CAN_CASTLE) {
		printf("taking the king...\n");
		PrintBoard(*b);
		std::cout<<"\n"<<m<<"\n";
		exit(EXIT_FAILURE);
	}
#endif //CHIASS_DEBUG
	b->B[m.end] = b->B[m.beg];
	b->B[m.beg] = {type_value::EMPTY,WHITE};
	
	size_t index_played=-1;
	for (size_t i=0;i<your_pieces.size();i++) {
		if (your_pieces[i].position==m.beg) {
			index_played=i;
			your_pieces[i].position=m.end;
			break;
		}
	}

	switch ( m.type ) {
		case type_of_move::NORMAL_MOVE:// Normal move
//			b->B[m.end] = b->B[m.beg];
//			b->B[m.beg] = {type_value::EMPTY,WHITE};
		break;
		case type_of_move::PROMOTION_QUEEN:
			b->B[m.end].piece = type_value::QUEEN;
			your_pieces[index_played].piece = type_value::QUEEN;
		break;
		case type_of_move::PROMOTION_KNIGHT:
			b->B[m.end].piece = type_value::KNIGHT;
			your_pieces[index_played].piece = type_value::KNIGHT;
		break;
		case type_of_move::FIRST_MOVE:
			b->B[m.end].piece = (type_value) (b->B[m.end].piece+1);
			your_pieces[index_played].piece = (type_value) (b->B[m.end].piece);
		break;
		case type_of_move::CASTLE:
			your_pieces[0].piece = type_value::KING;
			your_pieces[index_played].piece = type_value::ROOK;
			switch ( m.end ) {
				case d1:// WHITE LEFT 
					b->B[e1] = { type_value::EMPTY, WHITE };
					b->B[c1] = { type_value::KING, WHITE};
					b->B[d1] = { type_value::ROOK, WHITE };
					
					your_pieces[0].position=c1;
					return;
				case f1:// WHITE RIGHT 
					b->B[e1] = { type_value::EMPTY, WHITE };
					b->B[g1] = { type_value::KING, WHITE};
					b->B[f1] = { type_value::ROOK, WHITE };

					your_pieces[0].position=g1;
					return;
				case d8:// BLACK LEFT 
					b->B[e8] = { type_value::EMPTY, WHITE };
					b->B[c8] = { type_value::KING, BLACK};
					b->B[d8] = { type_value::ROOK, BLACK };

					your_pieces[0].position=c8;
					return;
				case f8:// BLACK RIGHT 
					b->B[e8] = { type_value::EMPTY, WHITE };
					b->B[g8] = { type_value::KING, BLACK};
					b->B[f8] = { type_value::ROOK, BLACK };

					your_pieces[0].position=g8;
					return;
				default:
					printf("Error in Play Castle %d\n",m.end);
					break;
			}
		break;
		case type_of_move::EN_PASSANT:// En Passant
			ufast8 taken_piece_position;
			if (b->B[m.end].colour == WHITE ) {
				*taken={type_value::PAWN, BLACK};
				taken_piece_position = (square) (m.end-8);
			} else {
				*taken={type_value::PAWN, WHITE};
				taken_piece_position = (square) (m.end+8);
			}
			b->B[taken_piece_position]={type_value::EMPTY, WHITE};
			for (size_t i=0;i<opponent_pieces.size();i++) {
				if (opponent_pieces[i].position==taken_piece_position) {
					opponent_pieces[i]=opponent_pieces.back();
					opponent_pieces.pop_back();
					break;
				}
			}
			return;
		default :
			fprintf(stderr, "Error during Play (vector) function : m = (%d,%d,%d)\n", m.beg, m.end, m.type);
			fprintf(stderr, "\t pieces (%d,%d)\n", b->B[m.end].piece, taken->piece);
	}
	if (taken->piece != type_value::EMPTY) {
		for (size_t i=0;i<opponent_pieces.size();i++) {
			if (opponent_pieces[i].position==m.end) {
				opponent_pieces[i]=opponent_pieces.back();
				opponent_pieces.pop_back();
				break;
			}
		}
	}
}