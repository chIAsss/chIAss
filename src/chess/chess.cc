#include "chess.hpp"

void ChIAss::Chess::EmptyBoard(ChIAss::Chess::Board* b) {
	memset( b->B , 0 , 64 ); 
}
/*
 * Passing a pointer is necessary since C doesn't treat array like pointers (: (: (: (: (. (.
 */
void ChIAss::Chess::InitBoard(ChIAss::Chess::Board* b) {
	memset( b->B , 0 , 64 ); 
	b->B[ChIAss::Chess::square::a1] = {ChIAss::Chess::type_value::ROOK_CAN_CASTLE, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::b1] = {ChIAss::Chess::type_value::KNIGHT, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::c1] = {ChIAss::Chess::type_value::BISHOP, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::d1] = {ChIAss::Chess::type_value::QUEEN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::e1] = {ChIAss::Chess::type_value::KING_CAN_CASTLE, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::f1] = {ChIAss::Chess::type_value::BISHOP, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::g1] = {ChIAss::Chess::type_value::KNIGHT, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::h1] = {ChIAss::Chess::type_value::ROOK_CAN_CASTLE, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::a2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::b2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::c2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::d2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::e2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::f2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::g2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};
	b->B[ChIAss::Chess::square::h2] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::WHITE};

// Black
	b->B[ChIAss::Chess::square::a8] = {ChIAss::Chess::type_value::ROOK_CAN_CASTLE, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::b8] = {ChIAss::Chess::type_value::KNIGHT, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::c8] = {ChIAss::Chess::type_value::BISHOP, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::d8] = {ChIAss::Chess::type_value::QUEEN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::e8] = {ChIAss::Chess::type_value::KING_CAN_CASTLE, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::f8] = {ChIAss::Chess::type_value::BISHOP, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::g8] = {ChIAss::Chess::type_value::KNIGHT, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::h8] = {ChIAss::Chess::type_value::ROOK_CAN_CASTLE, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::a7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::b7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::c7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::d7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::e7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::f7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::g7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
	b->B[ChIAss::Chess::square::h7] = {ChIAss::Chess::type_value::STARTING_PAWN, ChIAss::Chess::colour::BLACK};
}


/*
 * The Board is automatically cleaned, so you don't have to call EmptyBoard before setting the positions
 */
void ChIAss::Chess::SetPosition(ChIAss::Chess::Board* b, const std::vector<ChIAss::Chess::PieceAndPosition> pieces ) {
	memset( b->B , 0 , 64 ); 
	for ( ChIAss::Chess::PieceAndPosition p : pieces ) {
		b->B[ p.position ] = { p.piece , p.colour };	
	}
}
/*
 * The Board is automatically cleaned, so you don't have to call EmptyBoard before setting the positions
 */
void ChIAss::Chess::SetPosition(ChIAss::Chess::Board* b, const std::vector<ChIAss::Chess::PieceAndPosition> pieces_w, 
			const std::vector<ChIAss::Chess::PieceAndPosition> pieces_b ) {
	memset( b->B , 0 , 64 ); 
	for ( ChIAss::Chess::PieceAndPosition p : pieces_w ) {
		b->B[ p.position ] = { p.piece , p.colour };	
	} 
	for ( ChIAss::Chess::PieceAndPosition p : pieces_b ) {
		b->B[ p.position ] = { p.piece , p.colour };	
	}
}



void ChIAss::Chess::PrintBoard( const ChIAss::Chess::Board b ) {
	for ( char i = 7 ; i >= 0 ; i-- ) {
		for ( char j = 0 ; j < 8 ; j++ ) {
			switch ( b.B[ 8*i + j ].piece ) {
				case ChIAss::Chess::type_value::ROOK_CAN_CASTLE:
					fprintf( stdout , "Rc" );
					break;
				case ChIAss::Chess::type_value::ROOK:
					fprintf( stdout , "R" );
					break;
				case ChIAss::Chess::type_value::KNIGHT:
					fprintf( stdout , "N" );
					break;
				case ChIAss::Chess::type_value::BISHOP:
					fprintf( stdout , "B" );
					break;
				case ChIAss::Chess::type_value::QUEEN:
					fprintf( stdout , "Q" );
					break;
				case ChIAss::Chess::type_value::KING:
					fprintf( stdout , "K" );
					break;
				case ChIAss::Chess::type_value::KING_CAN_CASTLE:
					fprintf( stdout , "Kc" );
					break;
				case ChIAss::Chess::type_value::PAWN:
					fprintf( stdout , "p" );
					break;
				case ChIAss::Chess::type_value::STARTING_PAWN:
					fprintf( stdout , "P" );
					break;
				default :
					fprintf( stdout , "[]");
			}
			if ( b.B[ 8*i + j ].piece != ChIAss::Chess::type_value::EMPTY ) {
				switch ( b.B[ 8*i + j ].colour ) {
					case ChIAss::Chess::colour::WHITE:
						fprintf( stdout , "w" );
						break;
					case ChIAss::Chess::colour::BLACK:
						fprintf( stdout , "b" );
						break;
				}
			}
			fprintf( stdout , "\t");

		}
		fprintf( stdout , "\n" );
	}
}

std::ostream& ChIAss::Chess::operator<<(std::ostream& os, const ChIAss::Chess::Board& b) {
	for ( char i = 7 ; i >= 0 ; i-- ) {
		for ( char j = 0 ; j < 8 ; j++ ) {
			switch ( b.B[ 8*i + j ].piece ) {
				case ChIAss::Chess::type_value::ROOK_CAN_CASTLE:
				case ChIAss::Chess::type_value::ROOK:
					os << "R";
					break;
				case ChIAss::Chess::type_value::KNIGHT:
					os << "N";
					break;
				case ChIAss::Chess::type_value::BISHOP:
					os << "B";
					break;
				case ChIAss::Chess::type_value::QUEEN:
					os << "Q";
					break;
				case ChIAss::Chess::type_value::KING:
					os << "K";
					break;
				case ChIAss::Chess::type_value::KING_CAN_CASTLE:
					os << "Kc";
					break;
				case ChIAss::Chess::type_value::PAWN:
					os << "p";
					break;
				case ChIAss::Chess::type_value::STARTING_PAWN:
					os << "P";
					break;
				default :
					os << "[]";
			}
			if ( b.B[ 8*i + j ].piece != ChIAss::Chess::type_value::EMPTY ) {
				switch ( b.B[ 8*i + j ].colour ) {
					case ChIAss::Chess::colour::WHITE:
						os << "w";
						break;
					case ChIAss::Chess::colour::BLACK:
						os << "b";
						break;
				}
			}
			os << "\t";
		}
		os << "\n";
	}
	return os;
}

void ChIAss::Chess::UpdateBoard( ChIAss::Chess::Board* b , const ChIAss::Chess::Move m ) {
	b->B[ m.end ] = b->B[ m.beg ];
	b->B[ m.beg ] = { ChIAss::Chess::type_value::EMPTY, ChIAss::Chess::colour::WHITE };
}
void ChIAss::Chess::UpdateBoard( ChIAss::Chess::Board* b , const std::vector<ChIAss::Chess::Move> m ) {
	for ( ChIAss::Chess::Move mov : m ) {
		b->B[ mov.end ] = b->B[ mov.beg ];
		b->B[ mov.beg ] = { ChIAss::Chess::type_value::EMPTY, ChIAss::Chess::colour::WHITE };
	}
}


std::ostream& ChIAss::Chess::operator<<(std::ostream& os, const ChIAss::Chess::Move& m) {	
	os << static_cast<char>( m.beg%8 +97 ) << (int) m.beg/8 +1<<',' << static_cast<char>( m.end%8+97 ) << (int)m.end/8 +1;
    if (m.type != ChIAss::Chess::type_of_move::NORMAL_MOVE) {
		os<<":"<<m.type;
	}
	return os;
}

std::ostream& ChIAss::Chess::operator<<(std::ostream& os, const ChIAss::Chess::Piece& p) {
	switch (p.piece) {
		case ChIAss::Chess::type_value::STARTING_PAWN:
			os<<"STARTING_PAWN, ";
			break;
		case ChIAss::Chess::type_value::PAWN:
			os<<"PAWN, ";
			break;
		case ChIAss::Chess::type_value::BISHOP:
			os<<"BISHOP, ";
			break;
		case ChIAss::Chess::type_value::KNIGHT:
			os<<"KNIGHT, ";
			break;
		case ChIAss::Chess::type_value::ROOK_CAN_CASTLE:
			os<<"ROOK_CAN_CASTLE, ";
			break;
		case ChIAss::Chess::type_value::ROOK: 
			os<<"ROOK, ";
			break;
		case ChIAss::Chess::type_value::QUEEN:
			os<<"QUEEN, ";
			break;
		case ChIAss::Chess::type_value::KING:
			os<<"KING, ";
			break;
		case ChIAss::Chess::type_value::KING_CAN_CASTLE:
			os<<"KING_CAN_CASTLE, ";
			break;
		case ChIAss::Chess::type_value::EMPTY:
			os<<"EMPTY, ";
			break;
		default: // error
			os<<"ERROR_TYPE, ";
	}
	if (p.colour == ChIAss::Chess::colour::BLACK) {
		os<<"Black, ";
	} else {
		os<<"White, ";
	}
    return os;
}

std::ostream& ChIAss::Chess::operator<<(std::ostream& os, const ChIAss::Chess::PieceAndPosition& p) {
	switch (p.piece) {
		case ChIAss::Chess::type_value::STARTING_PAWN:
			os<<"STARTING_PAWN, ";
			break;
		case ChIAss::Chess::type_value::PAWN:
			os<<"PAWN, ";
			break;
		case ChIAss::Chess::type_value::BISHOP:
			os<<"BISHOP, ";
			break;
		case ChIAss::Chess::type_value::KNIGHT:
			os<<"KNIGHT, ";
			break;
		case ChIAss::Chess::type_value::ROOK_CAN_CASTLE:
			os<<"ROOK_CAN_CASTLE, ";
			break;
		case ChIAss::Chess::type_value::ROOK: 
			os<<"ROOK, ";
			break;
		case ChIAss::Chess::type_value::QUEEN:
			os<<"QUEEN, ";
			break;
		case ChIAss::Chess::type_value::KING:
			os<<"KING, ";
			break;
		case ChIAss::Chess::type_value::KING_CAN_CASTLE:
			os<<"KING_CAN_CASTLE, ";
			break;
		case ChIAss::Chess::type_value::EMPTY:
			os<<"EMPTY, ";
			break;
		default: // error
			os<<"ERROR_TYPE, ";
	}
	if (p.colour == ChIAss::Chess::colour::BLACK) {
		os<<"BLACK, ";
	} else {
		os<<"WHITE, ";
	}
	os<<static_cast<char>( p.position%8 +97 ) << (int) p.position/8 +1;
    return os;
}