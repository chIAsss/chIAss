#ifndef INTERFACE_CHIASS_HPP
#define INTERFACE_CHIASS_HPP
/* 
Basic functions used to display a board of chess
YOU NEED : SDL2
The sprites of the pieces in the same folder, as Chess_Pieces_Sprite.png
The format should be : 1400px * 400 px, white pieces above

DISCLAIMER : This module is meant to be used by the chIAss_chess_engine
Therefore, it's not standardized nor is it any good
You might be able to use and/or adapt some function to work with another engine
*/
#include "chess.hpp"

//  Those are the standard SDL2 functions
#include <SDL2/SDL.h>
//  This is for the use of PNG for the game pieces
#include <SDL2/SDL_image.h>
//  This is to write on the screen
#include <SDL2/SDL_ttf.h>
//  This is to have separate handling the window
#include <SDL2/SDL_thread.h>

// This is a standard value for the size of the board
#define BOARD_DIMENSIONS 800 // the board is 800x800 px
// This is a standard value for the size of the square composing the board
// Basicaly, it's BOARD_DIMENSIONS/8
#define SQUARE_DIMENSIONS 100 // the squares of the board are 100x100 px

namespace ChIAss {
    namespace GUI {

extern SDL_Renderer* renderer;
extern SDL_Window* display_window;
extern SDL_Rect sprite_square;
extern SDL_Rect black_pawn_square;
extern SDL_Rect white_pawn_square;
extern SDL_Rect black_knight_square;
extern SDL_Rect white_knight_square;
extern SDL_Rect black_bishop_square;
extern SDL_Rect white_bishop_square;
extern SDL_Rect black_rook_square;
extern SDL_Rect white_rook_square;
extern SDL_Rect black_queen_square;
extern SDL_Rect white_queen_square;
extern SDL_Rect black_king_square;
extern SDL_Rect white_king_square;
extern SDL_Surface* pieces_surface;
extern SDL_Texture* pieces_texture;

extern bool player_is_playing;
extern Chess::colour player_turn;
extern bool window_is_opened;

typedef struct _ChIAss_move_data {
    Chess::Board* _board;
    Chess::colour _ChIAss_colour;
    ufast8 _depth;
    Chess::Move _last_move_played;
    Chess::Move _played;
    bool ChIAss_is_playing;
} ChIAssMoveData;

typedef struct _set_position_data {
    Chess::Board* _board;
    Chess::colour _ChIAss_colour;
    ufast8 returned_value; 
} SetPositionData;

/*
    * @brief Initializes the interface used by ChIAss
*/
int InitInterface(void);

/*
    * @brief Closes the interface used by ChIAss
*/
void CloseInterface(void);

/*
    * @brief Displays a board, without any piece on it
*/
void DisplayCleanBoard(void);

/*
    * @brief Displays the board in the initial position for a game of chess
    * @param board_side The colour corresponding to the point of view of the player
*/
void DisplayInitialBoard(const Chess::colour board_side);

/*
    * @brief Displays the specified board in a window, with white's point of view
    * @param Board* b the board to display
*/
void DisplayBoard(Chess::Board* b);

/*
    * @brief Displays the specified board in a window, with respect to a point of view
    * @param b The board to display
    * @param board_side The colour corresponding to the point of view of the player
*/
void DisplayBoard(const Chess::Board b, const Chess::colour board_side);

/*
    * @brief Displays a move that's been played, with white's point of view
    * @param b the board
    * @param Move m to display
*/
void DisplayMove( Chess::Board* b, const Chess::Move m);

/*
    * @brief Highlights the given square
    * @param square The board's square to highlight
    * @param board_side The colour for the POV
    * @param colour_r the Red component of the colour
    * @param colour_g the Green component of the colour
    * @param colour_b the Blue component of the colour
*/
void HighlightSquare(const Chess::square target, const Chess::colour board_side, 
                const ufast8 colour_r, const ufast8 colour_g, const ufast8 colour_b);

/*
    * @brief Removes the highlight on the given square
    * @param square The board's square to clean
    * @param board_side The colour for the POV
*/
void UnhighlightSquare(const Chess::square target, const Chess::colour board_side);

/*
    * @brief Updates the display of the given square
    * @param b the board to know which state the square is in
    * @param square The board's square to clean
    * @param board_side The colour for the POV
*/
void UpdateSquare_GUI(const Chess::Board b, const Chess::square target, 
                    const Chess::colour board_side);

/*
    * @brief Enables to move pieces through the graphic interface
    * @param b the board
    * @return 0 if the window has been closed without any move being played, 1 otherwise
*/
int GraphicUpdateBoard( Chess::Board* b );

/*
    * @brief Permits to play an actual move on the board. The move has to be legal.
    * @param b The board.
    * @param played A pointer to the move that is played through this function.
    * @param player_colour The colour playing the move.
    * @param last_played The last move played, if ever, in case of an En Passant.
    * @return 0 if the window has been closed without any move being played, 1 otherwise
*/
int GraphicInterfaceGetMove( Chess::Board* b, Chess::Move* played, 
    const Chess::colour player_colour, const Chess::Move last_played );

int InteractionWindow(void* n);

/*
    * @brief Enables to set up a position on the board, so that ChIAss can play it.
    * @param b The board.
    * @param ChIAss_colour The colour that ChIAss will play. Also, the orientation of the board
    * @return -1 if the window has been closed, 1 if ChIAss has to play, 2 to get an "analysis" by ChIAss
*/
int SetPosition_GUI( Chess::Board* b, Chess::colour* ChIAss_colour, 
    Chess::Move* last_move_played );


int ChIAssMove_GUI(void* data);

/*
    * @brief Find the best move according to ChIAss in a given position, 
    * then exports the comparisons between this board and all other boards to a txt file
    * @param b The board on which ChIAss is playing
    * @param col The colour ChIAss is playing
    * @param expected_depth The depth at which ChIAss is looking for the best move
    * @param last_move_played The last move that's been played, necessary to know if ChIAss in in check
*/
int ChIAssAnalytics( Chess::Board* b, const Chess::colour col, 
    const ufast8 expected_depth, const Chess::Move last_move_played );
int ChIAssAnalytics_GUI(void* data);


    }   //  namespace GUI
}   //  namespace ChIAss

#endif // INTERFACE_CHIASS_HPP