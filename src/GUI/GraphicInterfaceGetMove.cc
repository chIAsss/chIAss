#include "GUI.hpp"

int ChIAss::GUI::GraphicInterfaceGetMove( ChIAss::Chess::Board* b, ChIAss::Chess::Move* played, 
            const ChIAss::Chess::colour player_colour, const ChIAss::Chess::Move last_played ) {
    SDL_Event event;
    event.type = 0;

    uint16_t x_position_mouse;
    uint16_t y_position_mouse;

    uint8_t case_letter = 9;
    uint8_t case_number = 9;
    ChIAss::Chess::Move m;
    m.beg = ChIAss::Chess::not_a_square;

//  There's a small lag at start because it needs to loop through all the mouse movement events

    while ( window_is_opened ) {
        if (SDL_PollEvent(&event)) {// if an event is detected
            switch (event.type) { // Depending on the event
                case SDL_QUIT: // If the window has been closed
                    return 0;
                case SDL_MOUSEBUTTONDOWN: // If the mouse has been clicked
                    if (player_colour == player_turn) {
                    if ( event.button.button==SDL_BUTTON_LEFT ) {
                        // check if you're playing
                        x_position_mouse = event.motion.x;
                        y_position_mouse = event.motion.y;
                        if ( x_position_mouse >= 400 && x_position_mouse < 1200 && y_position_mouse <= 800 ) {
                            if ( ( player_colour==ChIAss::Chess::WHITE && ( case_letter != ((x_position_mouse-400)/100) ||
                            case_number != 7 - (y_position_mouse/100) ) ) || 
                            ( player_colour==ChIAss::Chess::BLACK && ( case_letter != 7 - ((x_position_mouse-400)/100) ||
                            case_number != y_position_mouse/100 ) ) ) {

                                if (player_colour==ChIAss::Chess::WHITE) {
                                    case_number = 7 - (y_position_mouse/100);
                                    case_letter = ((x_position_mouse-400)/100);
                                } else {
                                    case_number = y_position_mouse/100;
                                    case_letter = 7 - ((x_position_mouse-400)/100);
                                }
//  Set the move
                                if ( m.beg == ChIAss::Chess::not_a_square ) {    // Sets the beginning of the move
                                    m.beg = (ChIAss::Chess::square) (8*case_number + case_letter);
                                    if ( b->B[m.beg].piece == ChIAss::Chess::type_value::EMPTY && b->B[m.beg].colour==player_colour ) {
                                        case_letter = 9;
                                        case_number = 9;
                                        m.beg = ChIAss::Chess::not_a_square;
                                    } else {
                                        HighlightSquare(m.beg, player_colour, 210, 0, 0);
                                    }
                                } else {    // Sets the end of the move
                                    m.end = (ChIAss::Chess::square) (8*case_number + case_letter);
//  Check legality of the move, before returning
                                    if ( IsMoveLegal(*b, &m, last_played) ) {
                                        *played = m;
                                        return 1;
//  If the move isn't legal : reset
                                    } else {
                                        UnhighlightSquare(m.beg, player_colour);
                                        case_letter = 9;
                                        case_number = 9;
                                        m.beg = ChIAss::Chess::not_a_square;
                                    }
                                }
//  Cursor isn't on the board : reset
                            } else {
                                if ( m.beg != ChIAss::Chess::not_a_square ) { 
                                    UnhighlightSquare(m.beg, player_colour);
                                    case_letter = 9;
                                    case_number = 9;
                                    m.beg = ChIAss::Chess::not_a_square;
                                }
                            }
                        } else {
                            if ( m.beg != ChIAss::Chess::not_a_square ) { 
                                UnhighlightSquare(m.beg, player_colour);
                                case_letter = 9;
                                case_number = 9;
                                m.beg = ChIAss::Chess::not_a_square;
                            }
                        }
                        // premove
                    }
                    }
                    break;
            }
        }
        SDL_Delay(10);
    }
    return 2;
}