#include "GUI.hpp"
#include "engine.hpp"


int ChIAss::GUI::ChIAssMove_GUI(void* data) {
    ChIAss::GUI::ChIAssMoveData* data_ = (ChIAss::GUI::ChIAssMoveData*) data;
    data_->_played = ChIAss::Engine::ChIAssMove( data_->_board,
                                    data_->_ChIAss_colour,
                                    data_->_depth,
                                    data_->_last_move_played);

    data_->ChIAss_is_playing = false;
    if ( data_->_played != (ChIAss::Chess::Move) {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square}) {
        ChIAss::GUI::UnhighlightSquare(data_->_last_move_played.beg, data_->_ChIAss_colour);
        ChIAss::GUI::UnhighlightSquare(data_->_last_move_played.end, data_->_ChIAss_colour);
        ChIAss::GUI::HighlightSquare(data_->_played.beg, data_->_ChIAss_colour, 210, 0, 0);
        ChIAss::GUI::HighlightSquare(data_->_played.end, data_->_ChIAss_colour, 210, 0, 0);
        return 0;
    }
    return -1;
}