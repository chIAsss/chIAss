#include "GUI.hpp"

int ChIAss::GUI::GraphicUpdateBoard( ChIAss::Chess::Board* b ) {
    SDL_Event event;
// Just to shut the warnings
    SDL_PollEvent(&event);

    uint16_t x_position_mouse;
    uint16_t y_position_mouse;

    uint8_t case_letter = 9;
    uint8_t case_number = 9;
    ChIAss::Chess::Move m;
    m.beg = ChIAss::Chess::not_a_square;

    bool still_hasnt_played=true;
    while ( still_hasnt_played ) {
        if (SDL_PollEvent(&event)) {// if an event is detected
            switch (event.type) { // Depending on the event
                case SDL_QUIT: // If the window has been closed
                    still_hasnt_played = false;
                    return 0;
                case SDL_MOUSEBUTTONDOWN: // If the mouse has been clicked
                    if ( event.button.button==SDL_BUTTON_LEFT ) {
                        // check if you're playing
                        x_position_mouse = event.motion.x;
                        y_position_mouse = event.motion.y;
                        if ( x_position_mouse >= 400 && x_position_mouse < 1200 && y_position_mouse <= 800 ) {
                            if ( case_letter != ((x_position_mouse-400)/100) ||
                            case_number != 7 - (y_position_mouse/100) ) {

                                case_letter = ((x_position_mouse-400)/100);
                                case_number = 7 - (y_position_mouse/100);
                                if ( m.beg == ChIAss::Chess::not_a_square ) {
                                    m.beg = (ChIAss::Chess::square) (8*case_number + case_letter);
                                    if ( b->B[m.beg].piece == ChIAss::Chess::type_value::EMPTY ) {
                                        case_letter = 9;
                                        case_number = 9;
                                        m.beg = ChIAss::Chess::not_a_square;
                                    }
                                } else {
                                    m.end = (ChIAss::Chess::square) (8*case_number + case_letter);
//printf("(%d,%d)\n", m.beg, m.end);
                                    DisplayMove(b, m);
                                    UpdateBoard(b, m);
//                                    still_hasnt_played = false;
//                                    return 1;
                                    case_letter = 9;
                                    case_number = 9;
                                    m.beg = ChIAss::Chess::not_a_square;
                                }
                            } else {
                                case_letter = 9;
                                case_number = 9;
                                m.beg = ChIAss::Chess::not_a_square;
                            }

                        }
                        // premove
                    }
                    break;
            }
        }
    }
    return 2;
}