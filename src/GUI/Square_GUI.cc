#include "GUI.hpp"



void ChIAss::GUI::HighlightSquare(const ChIAss::Chess::square target, const ChIAss::Chess::colour board_side, 
                    const ufast8 colour_r, const ufast8 colour_g, const ufast8 colour_b) {
    SDL_SetRenderDrawColor(renderer, colour_r, colour_g, colour_b, SDL_ALPHA_OPAQUE);
    SDL_Rect board_square = {0, 0, 0, 0};

    if (board_side==ChIAss::Chess::WHITE) {
        board_square.x = 400+(target%8)*SQUARE_DIMENSIONS;
        board_square.y = 700-(target/8)*SQUARE_DIMENSIONS;
    } else {
        board_square.x = 1100-(target%8)*SQUARE_DIMENSIONS;
        board_square.y = (target/8)*SQUARE_DIMENSIONS;
    }

    board_square.w = SQUARE_DIMENSIONS;
    board_square.h = SQUARE_DIMENSIONS;
    SDL_RenderDrawRect(renderer, &board_square);
    board_square.w = SQUARE_DIMENSIONS-2;
    board_square.h = SQUARE_DIMENSIONS-2;
    board_square.x += 1;
    board_square.y += 1;
    SDL_RenderDrawRect(renderer, &board_square);
    SDL_RenderPresent( renderer );
}



void ChIAss::GUI::UnhighlightSquare(const ChIAss::Chess::square target, const ChIAss::Chess::colour board_side) {
    SDL_Rect board_square = {0, 0, 0, 0};
	if ( (target/8)%2 != (target%8)%2 ) {
    	SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
	} else {
    	SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
	}
    if (board_side==ChIAss::Chess::WHITE) {
        board_square.x = 400+(target%8)*SQUARE_DIMENSIONS;
        board_square.y = 700-(target/8)*SQUARE_DIMENSIONS;
    } else {
        board_square.x = 1100-(target%8)*SQUARE_DIMENSIONS;
        board_square.y = (target/8)*SQUARE_DIMENSIONS;
    }
    board_square.w = SQUARE_DIMENSIONS;
    board_square.h = SQUARE_DIMENSIONS;
    SDL_RenderDrawRect(renderer, &board_square);
    board_square.w = SQUARE_DIMENSIONS-2;
    board_square.h = SQUARE_DIMENSIONS-2;
    board_square.x += 1;
    board_square.y += 1;
    SDL_RenderDrawRect(renderer, &board_square);
    SDL_RenderPresent( renderer );
}



void ChIAss::GUI::UpdateSquare_GUI(const ChIAss::Chess::Board b, const ChIAss::Chess::square target, const ChIAss::Chess::colour board_side) {
    SDL_Rect board_square = {0, 0, 0, 0};
	if ( (target/8)%2 != (target%8)%2 ) {
    	SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
	} else {
    	SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
	}
    if (board_side==ChIAss::Chess::WHITE) {
        board_square.x = 400+(target%8)*SQUARE_DIMENSIONS;
        board_square.y = 700-(target/8)*SQUARE_DIMENSIONS;
    } else {
        board_square.x = 1100-(target%8)*SQUARE_DIMENSIONS;
        board_square.y = (target/8)*SQUARE_DIMENSIONS;
    }
    board_square.w = SQUARE_DIMENSIONS;
    board_square.h = SQUARE_DIMENSIONS;
    SDL_RenderDrawRect(renderer, &board_square);
    board_square.w = SQUARE_DIMENSIONS-2;
    board_square.h = SQUARE_DIMENSIONS-2;
    board_square.x += 1;
    board_square.y += 1;
    SDL_RenderDrawRect(renderer, &board_square);

    if ( b.B[target].piece != ChIAss::Chess::type_value::EMPTY ) {
        SDL_Rect piece_square = {400, 0, SQUARE_DIMENSIONS, SQUARE_DIMENSIONS};
        SDL_Rect sprite_square = {400, 0, 200, 200};

        sprite_square.y = (b.B[target].colour) ? black_pawn_square.y : white_pawn_square.y;
        switch (b.B[target].piece) {
            case ChIAss::Chess::type_value::STARTING_PAWN:
            case ChIAss::Chess::type_value::PAWN:
                sprite_square.x =  white_pawn_square.x;
                break;
            case ChIAss::Chess::type_value::KNIGHT:
                sprite_square.x =  white_knight_square.x;
                break;
            case ChIAss::Chess::type_value::BISHOP:
                sprite_square.x =  white_bishop_square.x;
                break;
            case ChIAss::Chess::type_value::ROOK_CAN_CASTLE:
            case ChIAss::Chess::type_value::ROOK:
                sprite_square.x =  white_rook_square.x;
                break;
            case ChIAss::Chess::type_value::QUEEN:
                sprite_square.x =  white_queen_square.x;
                break;
            case ChIAss::Chess::type_value::KING_CAN_CASTLE:
            case ChIAss::Chess::type_value::KING:
                sprite_square.x =  white_king_square.x;
                break;
            default: 
                break;
        }
        SDL_RenderCopy(renderer, pieces_texture, &sprite_square, &piece_square);
    }
    SDL_RenderPresent( renderer );
}