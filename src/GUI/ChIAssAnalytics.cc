#include "engine.hpp"
#include "GUI.hpp"
#include <fstream>

void export_boards(std::ofstream& export_file, const ChIAss::Chess::colour ChIAss_colour, ChIAss::Chess::Board* b1, ChIAss::Chess::Board* b2) {
    using namespace ChIAss::Chess;
    
    char empty_str[] = "0.,0.,0.,0.,0.";
    char ChIAss_king_str[] = "1.,0.,0.,0.,0.";
    char ChIAss_pawn_str[] = "0.,1.,0.,0.,0.";
    char ChIAss_knight_str[] = "0.,0.,1.,0.,0.";
    char ChIAss_bishop_str[] = "0.,0.,0.,1.,0.";
    char ChIAss_rook_str[] = "0.,0.,0.,0.,1.";
    char ChIAss_queen_str[] = "0.,0.,0.,1.,1.";
    char opponent_king_str[] = "-1.,0.,0.,0.,0.";
    char opponent_pawn_str[] = "0.,-1.,0.,0.,0.";
    char opponent_knight_str[] = "0.,0.,-1.,0.,0.";
    char opponent_bishop_str[] = "0.,0.,0.,-1.,0.";
    char opponent_rook_str[] = "0.,0.,0.,0.,-1.";
    char opponent_queen_str[] = "0.,0.,0.,-1.,-1.";
    export_file <<"[";

    export_file <<"[";
    switch (b1->B[0].piece) {
        case EMPTY:
            export_file<<empty_str;
        break;
        case KING_CAN_CASTLE:
        case KING:
            if (b1->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_king_str;
            } else {
                export_file<<opponent_king_str;
            }
        break;
        case PAWN:
        case STARTING_PAWN:
            if (b1->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_pawn_str;
            } else {
                export_file<<opponent_pawn_str;
            }
        break;
        case KNIGHT:
            if (b1->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_knight_str;
            } else {
                export_file<<opponent_knight_str;
            }
        break;
        case BISHOP:
            if (b1->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_bishop_str;
            } else {
                export_file<<opponent_bishop_str;
            }
        break;
        case ROOK_CAN_CASTLE:
        case ROOK:
            if (b1->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_rook_str;
            } else {
                export_file<<opponent_rook_str;
            }
        break;
        case QUEEN:
            if (b1->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_queen_str;
            } else {
                export_file<<opponent_queen_str;
            }
        break;
    }
    export_file <<",";
    switch (b2->B[0].piece) {
        case EMPTY:
            export_file<<empty_str;
        break;
        case KING_CAN_CASTLE:
        case KING:
            if (b2->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_king_str;
            } else {
                export_file<<opponent_king_str;
            }
        break;
        case PAWN:
        case STARTING_PAWN:
            if (b2->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_pawn_str;
            } else {
                export_file<<opponent_pawn_str;
            }
        break;
        case KNIGHT:
            if (b2->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_knight_str;
            } else {
                export_file<<opponent_knight_str;
            }
        break;
        case BISHOP:
            if (b2->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_bishop_str;
            } else {
                export_file<<opponent_bishop_str;
            }
        break;
        case ROOK_CAN_CASTLE:
        case ROOK:
            if (b2->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_rook_str;
            } else {
                export_file<<opponent_rook_str;
            }
        break;
        case QUEEN:
            if (b2->B[0].colour == ChIAss_colour) {
                export_file<<ChIAss_queen_str;
            } else {
                export_file<<opponent_queen_str;
            }
        break;
    }
    export_file <<"]";
    
    for ( ufast8 i = 1 ; i < 64 ; i++ ) {
        export_file << ",";
        export_file <<"[";
        switch (b1->B[i].piece) {
            case EMPTY:
                export_file<<empty_str;
            break;
            case KING_CAN_CASTLE:
            case KING:
                if (b1->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_king_str;
                } else {
                    export_file<<opponent_king_str;
                }
            break;
            case PAWN:
            case STARTING_PAWN:
                if (b1->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_pawn_str;
                } else {
                    export_file<<opponent_pawn_str;
                }
            break;
            case KNIGHT:
                if (b1->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_knight_str;
                } else {
                    export_file<<opponent_knight_str;
                }
            break;
            case BISHOP:
                if (b1->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_bishop_str;
                } else {
                    export_file<<opponent_bishop_str;
                }
            break;
            case ROOK_CAN_CASTLE:
            case ROOK:
                if (b1->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_rook_str;
                } else {
                    export_file<<opponent_rook_str;
                }
            break;
            case QUEEN:
                if (b1->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_queen_str;
                } else {
                    export_file<<opponent_queen_str;
                }
            break;
        }
        export_file <<",";
        switch (b2->B[i].piece) {
            case EMPTY:
                export_file<<empty_str;
            break;
            case KING_CAN_CASTLE:
            case KING:
                if (b2->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_king_str;
                } else {
                    export_file<<opponent_king_str;
                }
            break;
            case PAWN:
            case STARTING_PAWN:
                if (b2->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_pawn_str;
                } else {
                    export_file<<opponent_pawn_str;
                }
            break;
            case KNIGHT:
                if (b2->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_knight_str;
                } else {
                    export_file<<opponent_knight_str;
                }
            break;
            case BISHOP:
                if (b2->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_bishop_str;
                } else {
                    export_file<<opponent_bishop_str;
                }
            break;
            case ROOK_CAN_CASTLE:
            case ROOK:
                if (b2->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_rook_str;
                } else {
                    export_file<<opponent_rook_str;
                }
            break;
            case QUEEN:
                if (b2->B[i].colour == ChIAss_colour) {
                    export_file<<ChIAss_queen_str;
                } else {
                    export_file<<opponent_queen_str;
                }
            break;
        }
        export_file <<"]";
    }
    export_file <<"]; ";
}

int ChIAss::GUI::ChIAssAnalytics( Chess::Board* b, const Chess::colour col, const ufast8 expected_depth, const Chess::Move last_move_played ) {
    using namespace ChIAss::Chess;
    std::ofstream export_file;
    export_file.open("doc/Data/export.txt", std::ofstream::app);

    // We create the vectors of pieces
    std::vector<PieceAndPosition> ChIAss_pieces;
    ChIAss_pieces.reserve(16);
    std::vector<PieceAndPosition> opponent_pieces;
    opponent_pieces.reserve(16);

    PieceAndPosition added_piece;

    for ( ufast8 i = 0 ; i < 64 ; i++ ) {
		if ( b->B[i].piece != EMPTY ) {
            added_piece.piece = b->B[i].piece;
            added_piece.colour = b->B[i].colour;
            added_piece.position = (square) i;
            if ( b->B[i].colour == col ) {
                ChIAss_pieces.push_back( added_piece );
            } else {
                opponent_pieces.push_back( added_piece );
            }
            // the king has to be in first place in the vector of pieces
            if ( b->B[i].piece >= KING_CAN_CASTLE ) {
                PieceAndPosition temporary;
                if ( b->B[i].colour == col ) {
                    temporary = ChIAss_pieces[0];
                    ChIAss_pieces[0] = ChIAss_pieces.back();
                    ChIAss_pieces.back() = temporary;
                } else {
                    temporary = opponent_pieces[0];
                    opponent_pieces[0] = opponent_pieces.back();
                    opponent_pieces.back() = temporary;
                }
            }
        }
    }

//  ========== shared variables ==========
    std::vector<Move> all_possible_moves;
    ufast8 result_check;
    all_possible_moves = ChIAss::Engine::Moves( b, ChIAss_pieces, last_move_played, &result_check );
    if ( all_possible_moves.size() == 0 ) {
        return -1;
    }
    Move best_move;
    fast16 best_value;


    if ( col == WHITE ) {
        best_value = -INFINITY_CHIASS;
    } else {
        best_value = INFINITY_CHIASS;
    }
//  Just in case there's only 1 move possible, then a single thread is enough
    #pragma omp parallel num_threads(std::min((int)omp_get_max_threads(), (int)all_possible_moves.size()))
    {
    //  ========== thread-specific variables ==========
        fast16 current_value;
        Piece taken;// The piece that's been taken, if ever, or {EMPTY, WHITE}, used to unplay the move
        Board thread_board;
        //  Loading the vector of pieces, for each thread
        std::vector<PieceAndPosition> thread_ChIAss_pieces;
        std::vector<PieceAndPosition> thread_opponent_pieces;
        thread_opponent_pieces.reserve(16);
        for ( ufast8 i = 0 ; i < 64 ; i++ ) {
            thread_board.B[i] = b->B[i];
            PieceAndPosition thread_added_piece;
            if ( b->B[i].piece != EMPTY ) {
                thread_added_piece.piece = b->B[i].piece;
                thread_added_piece.colour = b->B[i].colour;
                thread_added_piece.position = (square) i;
                if ( b->B[i].colour == col ) {
                    thread_ChIAss_pieces.push_back( thread_added_piece );
                } else {
                    thread_opponent_pieces.push_back( thread_added_piece );
                }
                // the king has to be in first place in the vector of pieces
                if ( b->B[i].piece >= KING_CAN_CASTLE ) {
                    PieceAndPosition temporary;
                    if ( b->B[i].colour == col ) {
                        temporary = thread_ChIAss_pieces[0];
                        thread_ChIAss_pieces[0] = thread_ChIAss_pieces.back();
                        thread_ChIAss_pieces.back() = temporary;
                    } else {
                        temporary = thread_opponent_pieces[0];
                        thread_opponent_pieces[0] = thread_opponent_pieces.back();
                        thread_opponent_pieces.back() = temporary;
                    }
                }
            }
        }

        if ( col == WHITE ) {
    //  ======================================== ChIAss plays WHITE ===================================
            ChIAss::Chess::Play( &thread_board , all_possible_moves[omp_get_thread_num()] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);
            current_value = ChIAss::Engine::ValueMove( &thread_board, 1, expected_depth, 
                                            thread_opponent_pieces, thread_ChIAss_pieces, 
                                            all_possible_moves[omp_get_thread_num()] , best_value );
            ChIAss::Chess::Unplay( &thread_board, all_possible_moves[omp_get_thread_num()], taken, thread_ChIAss_pieces, thread_opponent_pieces);
            
            #pragma omp critical
            {
                if ( current_value > best_value ) {
                    best_move = all_possible_moves[omp_get_thread_num()];
                    best_value=current_value;
                }
            }


            
            #pragma omp for
            for ( ufast8 i=omp_get_num_threads() ; i<(int)all_possible_moves.size() ; i++ ) {
                ChIAss::Chess::Play( &thread_board , all_possible_moves[i] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);

                current_value = ChIAss::Engine::ValueMove( &thread_board, 1, expected_depth, 
                                                thread_opponent_pieces, thread_ChIAss_pieces, 
                                                all_possible_moves[i] , best_value );

                ChIAss::Chess::Unplay( &thread_board, all_possible_moves[i], taken, thread_ChIAss_pieces, thread_opponent_pieces);
                
                #pragma omp critical
                {
                    if ( current_value > best_value ) {
                        best_move = all_possible_moves[i];
                        best_value=current_value;
                    }
                }
            }



        } else {
    //  ======================================== ChIAss plays BLACK ===================================
            ChIAss::Chess::Play( &thread_board , all_possible_moves[omp_get_thread_num()] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);
            current_value = ChIAss::Engine::ValueMove( &thread_board, 1, expected_depth, 
                                            thread_opponent_pieces, thread_ChIAss_pieces, 
                                            all_possible_moves[omp_get_thread_num()] , best_value );
            ChIAss::Chess::Unplay( &thread_board, all_possible_moves[omp_get_thread_num()], taken, thread_ChIAss_pieces, thread_opponent_pieces);
            #pragma omp critical
            {
                if ( current_value < best_value ) {
                    best_move = all_possible_moves[omp_get_thread_num()];
                    best_value=current_value;
                }
            }



            #pragma omp for
            for ( ufast8 i=omp_get_num_threads() ; i<(int)all_possible_moves.size() ; i++ ) {
                ChIAss::Chess::Play( &thread_board , all_possible_moves[i] , &taken, thread_ChIAss_pieces, thread_opponent_pieces);

                current_value = ChIAss::Engine::ValueMove( &thread_board, 1, expected_depth, 
                                                thread_opponent_pieces, thread_ChIAss_pieces, 
                                                all_possible_moves[i] , best_value );

                ChIAss::Chess::Unplay( &thread_board, all_possible_moves[i], taken, thread_ChIAss_pieces, thread_opponent_pieces);
                #pragma omp critical
                {
                    if ( current_value < best_value ) {
                        best_move = all_possible_moves[i];
                        best_value=current_value;
                    }
                }
            }
        }
    }

    Board best_board;
    Piece taken;
    best_board = *b;
    ChIAss::Chess::Play(&best_board, best_move, &taken);
    
    for (ufast8 i=0; i < all_possible_moves.size(); i++) {
        if (all_possible_moves[i] != best_move) {
            ChIAss::Chess::Play(b, all_possible_moves[i], &taken);
            export_boards( export_file, col, &best_board, b);
            export_file<<"0\n";
            export_boards( export_file, col, b, &best_board);
            export_file<<"1\n";
            ChIAss::Chess::Unplay(b, all_possible_moves[i], taken);
        }
    }
    export_file.close();
    printf("analysis done.\n");
    return 0;
}