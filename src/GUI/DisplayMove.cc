#include <GUI.hpp>

void ChIAss::GUI::DisplayMove( ChIAss::Chess::Board* b, const ChIAss::Chess::Move m) {
// TODO : add the usual checks
    SDL_Texture* pieces_texture;
    pieces_texture = SDL_CreateTextureFromSurface(renderer, pieces_surface);

	sprite_square.y = (b->B[m.beg].colour) ? black_pawn_square.y : white_pawn_square.y;
	switch (b->B[m.beg].piece) {
        case ChIAss::Chess::type_value::STARTING_PAWN:
        case ChIAss::Chess::type_value::PAWN:
            sprite_square.x =  white_pawn_square.x;
            break;
        case ChIAss::Chess::type_value::KNIGHT:
            sprite_square.x =  white_knight_square.x;
            break;
        case ChIAss::Chess::type_value::BISHOP:
            sprite_square.x =  white_bishop_square.x;
            break;
        case ChIAss::Chess::type_value::ROOK_CAN_CASTLE:
        case ChIAss::Chess::type_value::ROOK:
            sprite_square.x =  white_rook_square.x;
            break;
        case ChIAss::Chess::type_value::QUEEN:
            sprite_square.x =  white_queen_square.x;
            break;
        case ChIAss::Chess::type_value::KING_CAN_CASTLE:
        case ChIAss::Chess::type_value::KING:
            sprite_square.x =  white_king_square.x;
            break;
        default: 
            break;
    }

	SDL_Rect board_square = {0, 0, SQUARE_DIMENSIONS, SQUARE_DIMENSIONS};

	if ( (m.beg/8)%2 != (m.beg%8)%2 ) {
    	SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
	} else {
    	SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
	}
	board_square.x =  400+(m.beg%8)*SQUARE_DIMENSIONS;
	board_square.y =  (7-m.beg/8)*SQUARE_DIMENSIONS;
	SDL_RenderFillRect(renderer, &board_square);

	if ( (m.end/8)%2 != (m.end%8)%2 ) {
    	SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
	} else {
    	SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
	}
	board_square.x =  400+(m.end%8)*SQUARE_DIMENSIONS;
	board_square.y =  (7-m.end/8)*SQUARE_DIMENSIONS;
	SDL_RenderFillRect(renderer, &board_square);
    SDL_RenderCopy(renderer, pieces_texture, &sprite_square, &board_square);


    SDL_RenderPresent( renderer );
    SDL_DestroyTexture(pieces_texture);
    SDL_FreeSurface(pieces_surface);
}