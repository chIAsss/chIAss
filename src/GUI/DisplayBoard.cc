#include "GUI.hpp"

using namespace ChIAss::Chess;

void ChIAss::GUI::DisplayCleanBoard(void) {
    SDL_Rect square = {0, 0, 0, 0};
    square.x =  400;
    square.y = 0;
    square.w = BOARD_DIMENSIONS;
    square.h = BOARD_DIMENSIONS;
    SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(renderer, &square);

    square.w = SQUARE_DIMENSIONS;
    square.h = SQUARE_DIMENSIONS;
    SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
    for ( uint8_t i = 0 ; i < 8 ; i+=1 ) {
        for ( uint8_t j = 1-i%2 ; j < 8 ; j+=2) {
            square.x =  400+i*SQUARE_DIMENSIONS;
            square.y = j*SQUARE_DIMENSIONS;
            SDL_RenderFillRect(renderer, &square);
        }
    }
    SDL_RenderPresent(renderer);
}


// TODO : return error code
void ChIAss::GUI::DisplayInitialBoard(const colour board_side) {
    DisplayCleanBoard();

    SDL_Rect piece_square = {400, 0, SQUARE_DIMENSIONS, SQUARE_DIMENSIONS};

    // PAWNS
    //  BLACK
    if (board_side == WHITE) {
        piece_square.y = 100;
    } else {
        piece_square.y = 600;
    }
    piece_square.x = 400;
    for ( uint8_t i = 0 ; i < 8 ; i++ ) {
        SDL_RenderCopy(renderer, pieces_texture, &black_pawn_square, &piece_square);
        piece_square.x += 100;
    }
    //  WHITE
    if (board_side == WHITE) {
        piece_square.y = 600;
    } else {
        piece_square.y = 100;
    }
    piece_square.x = 400;
    for ( uint8_t i = 0 ; i < 8 ; i++ ) {
        SDL_RenderCopy(renderer, pieces_texture, &white_pawn_square, &piece_square);
        piece_square.x += 100;
    }

    // KNIGHTS
    if (board_side == WHITE) {
        piece_square.y = 0;
    } else {
        piece_square.y = 700;
    }
    piece_square.x = 500;
    SDL_RenderCopy(renderer, pieces_texture, &black_knight_square, &piece_square);
    piece_square.x = 1000;
    SDL_RenderCopy(renderer, pieces_texture, &black_knight_square, &piece_square);
    if (board_side == WHITE) {
        piece_square.y = 700;
    } else {
        piece_square.y = 0;
    }
    piece_square.x = 500;
    SDL_RenderCopy(renderer, pieces_texture, &white_knight_square, &piece_square);
    piece_square.x = 1000;
    SDL_RenderCopy(renderer, pieces_texture, &white_knight_square, &piece_square);

    // BISHOPS
    if (board_side == WHITE) {
        piece_square.y = 0;
    } else {
        piece_square.y = 700;
    }
    piece_square.x = 600;
    SDL_RenderCopy(renderer, pieces_texture, &black_bishop_square, &piece_square);
    piece_square.x = 900;
    SDL_RenderCopy(renderer, pieces_texture, &black_bishop_square, &piece_square);
    if (board_side == WHITE) {
        piece_square.y = 700;
    } else {
        piece_square.y = 0;
    }
    piece_square.x = 600;
    SDL_RenderCopy(renderer, pieces_texture, &white_bishop_square, &piece_square);
    piece_square.x = 900;
    SDL_RenderCopy(renderer, pieces_texture, &white_bishop_square, &piece_square);

    // ROOKS
    if (board_side == WHITE) {
        piece_square.y = 0;
    } else {
        piece_square.y = 700;
    }
    piece_square.x = 400;
    SDL_RenderCopy(renderer, pieces_texture, &black_rook_square, &piece_square);
    piece_square.x = 1100;
    SDL_RenderCopy(renderer, pieces_texture, &black_rook_square, &piece_square);
    if (board_side == WHITE) {
        piece_square.y = 700;
    } else {
        piece_square.y = 0;
    }
    piece_square.x = 400;
    SDL_RenderCopy(renderer, pieces_texture, &white_rook_square, &piece_square);
    piece_square.x = 1100;
    SDL_RenderCopy(renderer, pieces_texture, &white_rook_square, &piece_square);

    if (board_side == WHITE) {
        piece_square.y = 0;
    } else {
        piece_square.y = 700;
    }
    piece_square.x = 700;
    SDL_RenderCopy(renderer, pieces_texture, &black_queen_square, &piece_square);
    piece_square.x = 800;
    SDL_RenderCopy(renderer, pieces_texture, &black_king_square, &piece_square);
    if (board_side == WHITE) {
        piece_square.y = 700;
    } else {
        piece_square.y = 0;
    }
    piece_square.x = 800;
    SDL_RenderCopy(renderer, pieces_texture, &white_king_square, &piece_square);
    piece_square.x = 700;
    SDL_RenderCopy(renderer, pieces_texture, &white_queen_square, &piece_square);

    SDL_RenderPresent( renderer );
}


void ChIAss::GUI::DisplayBoard(const Board b, const colour board_side) {
    DisplayCleanBoard();

    SDL_Rect piece_square = {400, 0, SQUARE_DIMENSIONS, SQUARE_DIMENSIONS};
    SDL_Rect sprite_square = {400, 0, 200, 200};

    for ( uint8_t i = 0; i < 64 ; i++ ) {
        if ( b.B[i].piece != EMPTY ) {
            if (board_side == WHITE) {
                piece_square.y = 700 - 100*(i/8);
                piece_square.x = 400 + 100*(i%8);
            } else {
                piece_square.y = 100*(i/8);
                piece_square.x = 1100 - 100*(i%8);
            }
            
            sprite_square.y = (b.B[i].colour) ? black_pawn_square.y : white_pawn_square.y;
            switch (b.B[i].piece) {
                case STARTING_PAWN:
                case PAWN:
                    sprite_square.x =  white_pawn_square.x;
                    break;
                case KNIGHT:
                    sprite_square.x =  white_knight_square.x;
                    break;
                case BISHOP:
                    sprite_square.x =  white_bishop_square.x;
                    break;
                case ROOK_CAN_CASTLE:
                case ROOK:
                    sprite_square.x =  white_rook_square.x;
                    break;
                case QUEEN:
                    sprite_square.x =  white_queen_square.x;
                    break;
                case KING_CAN_CASTLE:
                case KING:
                    sprite_square.x =  white_king_square.x;
                    break;
                default: 
                    break;
            }
            SDL_RenderCopy(renderer, pieces_texture, &sprite_square, &piece_square);
        }
    }
    SDL_RenderPresent( renderer );
}



void ChIAss::GUI::HighlightSquare(const square target, const colour board_side, 
            const ufast8 colour_r, const ufast8 colour_g, const ufast8 colour_b) {
    SDL_SetRenderDrawColor(renderer, colour_r, colour_g, colour_b, SDL_ALPHA_OPAQUE);
    SDL_Rect board_square = {0, 0, 0, 0};

    if (board_side==WHITE) {
        board_square.x = 400+(target%8)*SQUARE_DIMENSIONS;
        board_square.y = 700-(target/8)*SQUARE_DIMENSIONS;
    } else {
        board_square.x = 1100-(target%8)*SQUARE_DIMENSIONS;
        board_square.y = (target/8)*SQUARE_DIMENSIONS;
    }

    board_square.w = SQUARE_DIMENSIONS;
    board_square.h = SQUARE_DIMENSIONS;
    SDL_RenderDrawRect(renderer, &board_square);
    board_square.w = SQUARE_DIMENSIONS-2;
    board_square.h = SQUARE_DIMENSIONS-2;
    board_square.x += 1;
    board_square.y += 1;
    SDL_RenderDrawRect(renderer, &board_square);
    SDL_RenderPresent( renderer );
}



void ChIAss::GUI::UnhighlightSquare(const square target, const colour board_side) {
    SDL_Rect board_square = {0, 0, 0, 0};
	if ( (target/8)%2 != (target%8)%2 ) {
    	SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
	} else {
    	SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
	}
    if (board_side==WHITE) {
        board_square.x = 400+(target%8)*SQUARE_DIMENSIONS;
        board_square.y = 700-(target/8)*SQUARE_DIMENSIONS;
    } else {
        board_square.x = 1100-(target%8)*SQUARE_DIMENSIONS;
        board_square.y = (target/8)*SQUARE_DIMENSIONS;
    }
    board_square.w = SQUARE_DIMENSIONS;
    board_square.h = SQUARE_DIMENSIONS;
    SDL_RenderDrawRect(renderer, &board_square);
    board_square.w = SQUARE_DIMENSIONS-2;
    board_square.h = SQUARE_DIMENSIONS-2;
    board_square.x += 1;
    board_square.y += 1;
    SDL_RenderDrawRect(renderer, &board_square);
    SDL_RenderPresent( renderer );
}



void ChIAss::GUI::UpdateSquare_GUI(const Board b, const square target, const colour board_side) {
    SDL_Rect board_square = {0, 0, 0, 0};
	if ( (target/8)%2 != (target%8)%2 ) {
    	SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
	} else {
    	SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
	}
    if (board_side==WHITE) {
        board_square.x = 400+(target%8)*SQUARE_DIMENSIONS;
        board_square.y = 700-(target/8)*SQUARE_DIMENSIONS;
    } else {
        board_square.x = 1100-(target%8)*SQUARE_DIMENSIONS;
        board_square.y = (target/8)*SQUARE_DIMENSIONS;
    }
    board_square.w = SQUARE_DIMENSIONS;
    board_square.h = SQUARE_DIMENSIONS;
    SDL_RenderFillRect(renderer, &board_square);

    if ( b.B[target].piece != EMPTY ) {
        SDL_Rect sprite_square = {400, 0, 200, 200};

        sprite_square.y = (b.B[target].colour) ? black_pawn_square.y : white_pawn_square.y;
        switch (b.B[target].piece) {
            case STARTING_PAWN:
            case PAWN:
                sprite_square.x =  white_pawn_square.x;
                break;
            case KNIGHT:
                sprite_square.x =  white_knight_square.x;
                break;
            case BISHOP:
                sprite_square.x =  white_bishop_square.x;
                break;
            case ROOK_CAN_CASTLE:
            case ROOK:
                sprite_square.x =  white_rook_square.x;
                break;
            case QUEEN:
                sprite_square.x =  white_queen_square.x;
                break;
            case KING_CAN_CASTLE:
            case KING:
                sprite_square.x =  white_king_square.x;
                break;
            default: 
                break;
        }
        SDL_RenderCopy(renderer, pieces_texture, &sprite_square, &board_square);
    }
    SDL_RenderPresent( renderer );
}