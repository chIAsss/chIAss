#include "GUI.hpp"


int ChIAss::GUI::SetPosition_GUI( ChIAss::Chess::Board* b, ChIAss::Chess::colour* ChIAss_colour, 
                    ChIAss::Chess::Move* last_move_played ) {
    ChIAss::GUI::DisplayBoard(*b, *ChIAss_colour);
    SDL_Event event;
// Just to shut the warnings
    SDL_PollEvent(&event);
    uint16_t x_position_mouse;
    uint16_t y_position_mouse;
    uint8_t case_letter = 9;
    uint8_t case_number = 9;
    ChIAss::Chess::Piece selected_piece = {ChIAss::Chess::EMPTY, ChIAss::Chess::WHITE};

    bool not_done=true;
//  Displays the pieces on the side of the board
    SDL_Rect piece_square = {0, 0, SQUARE_DIMENSIONS, SQUARE_DIMENSIONS};
    SDL_RenderCopy(renderer, pieces_texture, &black_pawn_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &black_knight_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &black_bishop_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &black_rook_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &black_queen_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &black_king_square, &piece_square);

    piece_square.y = 0;
    piece_square.x += 100;
    SDL_RenderCopy(renderer, pieces_texture, &white_pawn_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &white_knight_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &white_bishop_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &white_rook_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &white_queen_square, &piece_square);
    piece_square.y += 100;
    SDL_RenderCopy(renderer, pieces_texture, &white_king_square, &piece_square);


//  Displays the two squares of colour, to choose the orientation of the board, and which colour ChIAss is playing
    SDL_Rect colored_square = {0, 600, SQUARE_DIMENSIONS, SQUARE_DIMENSIONS};
    SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(renderer, &colored_square);
    colored_square.x = 100;
    SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(renderer, &colored_square);

    if (*ChIAss_colour == ChIAss::Chess::BLACK) {
        colored_square.x = 0;
    } else {
        colored_square.x = 100;
    }
    SDL_SetRenderDrawColor(renderer, 210, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawRect(renderer, &colored_square);
    SDL_RenderPresent( renderer );






//  Displays the instructions texts
//  TODO : load ttf in the init
    TTF_Init();
    TTF_Font* font_text = TTF_OpenFont("doc/Fonts/trebuc.ttf", 26);
    if (font_text == NULL) {
        fprintf(stderr, "Error, couldn't load the font\n");
    }
    SDL_Color sdl_colour_black = {0, 0, 0};
    SDL_Surface* surface_text = TTF_RenderText_Blended_Wrapped(font_text, "Right click to remove a piece\n\
    C to clear\n\
    D for the default position\n\
    E to export\n\
    P to see ChIAss's move\n\
    A to export analysis\n\
    M to set the last move played", sdl_colour_black, 400); 
    SDL_Texture* texture_text = SDL_CreateTextureFromSurface(renderer, surface_text);
    piece_square.x = 50;
    piece_square.y = 700;
    piece_square.w = 300;
    piece_square.h = 120;
    SDL_RenderCopy(renderer, texture_text, NULL, &piece_square);
    
    SDL_RenderPresent( renderer );
    while ( not_done ) {
        if (SDL_PollEvent(&event)) {// if an event is detected
            switch (event.type) { // Depending on the event
                case SDL_QUIT: // If the window has been closed
                    not_done = false;
                    return -1;
                break;
                case SDL_MOUSEBUTTONDOWN: // If the mouse has been clicked
                    x_position_mouse = event.motion.x;
                    y_position_mouse = event.motion.y;
                    if ( event.button.button==SDL_BUTTON_LEFT ) {
                        if ( x_position_mouse <= 200 && y_position_mouse <= 700 ) {// selecting a piece
                            selected_piece.colour = (x_position_mouse <= 100) ? ChIAss::Chess::BLACK : ChIAss::Chess::WHITE;
                            switch (y_position_mouse/100) {
                                case 0:
                                    selected_piece.piece = ChIAss::Chess::PAWN;
                                    break;
                                case 1:
                                    selected_piece.piece = ChIAss::Chess::KNIGHT;
                                    break;
                                case 2:
                                    selected_piece.piece = ChIAss::Chess::BISHOP;
                                    break;
                                case 3:
                                    selected_piece.piece = ChIAss::Chess::ROOK;
                                    break;
                                case 4:
                                    selected_piece.piece = ChIAss::Chess::QUEEN;
                                    break;
                                case 5:
                                    selected_piece.piece = ChIAss::Chess::KING;
                                    break;
                                default:
                                    *ChIAss_colour = (ChIAss::Chess::colour) selected_piece.colour;
                                    colored_square.x = 0;
                                    SDL_SetRenderDrawColor(renderer, 50, 50, 50, SDL_ALPHA_OPAQUE);
                                    SDL_RenderDrawRect(renderer, &colored_square);
                                    colored_square.x = 100;
                                    SDL_SetRenderDrawColor(renderer, 205, 205, 205, SDL_ALPHA_OPAQUE);
                                    SDL_RenderDrawRect(renderer, &colored_square);
                                    if (*ChIAss_colour == ChIAss::Chess::BLACK) {
                                        colored_square.x = 0;
                                    } else {
                                        colored_square.x = 100;
                                    }
                                    SDL_SetRenderDrawColor(renderer, 210, 0, 0, SDL_ALPHA_OPAQUE);
                                    SDL_RenderDrawRect(renderer, &colored_square);
                                    SDL_RenderPresent( renderer );
                                    ChIAss::GUI::DisplayBoard(*b, *ChIAss_colour);

                                    if (last_move_played->beg != (ChIAss::Chess::square) 64) {
                                        ChIAss::GUI::HighlightSquare(last_move_played->beg, *ChIAss_colour, 0, 210, 0);
                                    }
                                    if (last_move_played->end != (ChIAss::Chess::square) 64) {
                                        ChIAss::GUI::HighlightSquare(last_move_played->end, *ChIAss_colour, 0, 210, 0);
                                    }
                                    break;
                            }

                        } else if ( x_position_mouse >= 4*SQUARE_DIMENSIONS && x_position_mouse < 12*SQUARE_DIMENSIONS &&
                                    y_position_mouse <= 8*SQUARE_DIMENSIONS) {// selecting a square
                            if (*ChIAss_colour==ChIAss::Chess::WHITE) {
                                case_number = 7 - (y_position_mouse/100);
                                case_letter = ((x_position_mouse-400)/100);
                            } else {
                                case_number = y_position_mouse/100;
                                case_letter = 7 - ((x_position_mouse-400)/100);
                            }

                            if ( selected_piece.piece > ChIAss::Chess::EMPTY && selected_piece.piece != 127 ) {
                                b->B[(8*case_number + case_letter)] = selected_piece;
                                ChIAss::GUI::UpdateSquare_GUI(*b, (ChIAss::Chess::square) (8*case_number + case_letter), *ChIAss_colour);
                            } else if ( selected_piece.piece == 127 ) {
                                if (last_move_played->beg == (ChIAss::Chess::square) 64) {
                                    last_move_played->beg = (ChIAss::Chess::square) (8*case_number + case_letter);
                                    ChIAss::GUI::HighlightSquare(last_move_played->beg, *ChIAss_colour, 0, 210, 0);
                                } else if (last_move_played->end == (ChIAss::Chess::square) 64) {
                                    last_move_played->end = (ChIAss::Chess::square) (8*case_number + case_letter);
                                    ChIAss::GUI::HighlightSquare(last_move_played->beg, *ChIAss_colour, 0, 210, 0);
                                    ChIAss::GUI::HighlightSquare(last_move_played->end, *ChIAss_colour, 0, 210, 0);
                                } else {
                                    ChIAss::GUI::UnhighlightSquare(last_move_played->beg, *ChIAss_colour);
                                    ChIAss::GUI::UnhighlightSquare(last_move_played->end, *ChIAss_colour);
                                    last_move_played->beg = (ChIAss::Chess::square) 64;
                                    last_move_played->end = (ChIAss::Chess::square) 64;
                                }
                            }
                        }
                    } else if ( event.button.button==SDL_BUTTON_RIGHT ) {
                        if ( x_position_mouse >= 4*SQUARE_DIMENSIONS && x_position_mouse < 12*SQUARE_DIMENSIONS &&
                            y_position_mouse <= 8*SQUARE_DIMENSIONS) {// selecting a square
                            if (*ChIAss_colour==ChIAss::Chess::WHITE) {
                                case_number = 7 - (y_position_mouse/100);
                                case_letter = ((x_position_mouse-400)/100);
                            } else {
                                case_number = y_position_mouse/100;
                                case_letter = 7 - ((x_position_mouse-400)/100);
                            }

                            if ( b->B[(8*case_number + case_letter)].piece != ChIAss::Chess::EMPTY ) {
                                b->B[(8*case_number + case_letter)] = {ChIAss::Chess::EMPTY, ChIAss::Chess::WHITE};
                                ChIAss::GUI::UpdateSquare_GUI(*b, (ChIAss::Chess::square) (8*case_number + case_letter), *ChIAss_colour);
                            }
                        }
                    }
                break;
                case SDL_KEYDOWN:
                    if ( event.key.repeat != 1 ) {
//  TODO : Correct Scancodes for AZERTY keyboards
                        switch (event.key.keysym.scancode) {
                            case SDL_SCANCODE_C:// C is pressed : the board is cleared
                                ChIAss::Chess::EmptyBoard(b);
                                ChIAss::GUI::DisplayBoard(*b, *ChIAss_colour);
                                if (last_move_played->beg != (ChIAss::Chess::square) 64) {
                                    ChIAss::GUI::UnhighlightSquare(last_move_played->beg, *ChIAss_colour);
                                }
                                if (last_move_played->end != (ChIAss::Chess::square) 64) {
                                    ChIAss::GUI::UnhighlightSquare(last_move_played->end, *ChIAss_colour);
                                }
                                last_move_played->beg = (ChIAss::Chess::square) 64;
                                last_move_played->end = (ChIAss::Chess::square) 64;
                            break;
                            case SDL_SCANCODE_D:// D is pressed : the starting board is set
                                ChIAss::Chess::InitBoard(b);
                                ChIAss::GUI::DisplayBoard(*b, *ChIAss_colour);
                            break;
                            case SDL_SCANCODE_P:// P is pressed : the position is set, ChIAss will play
                                not_done = false;
                                return 1;
                            case 20://SDL_SCANCODE_A:// A is pressed : the position is set, ChIAss will play
                                not_done = false;
                                return 2;
                            break;
                            case 51:// M is pressed : Setting the last move
                                selected_piece.piece = (ChIAss::Chess::type_value) 127;
                            break;
                            default:
//                                printf("%d\n", event.key.keysym.scancode);
                            continue;
                        }
                    }
                break;
            }
        }
    }
    return 0;
}