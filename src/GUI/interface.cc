#include "GUI.hpp"


SDL_Renderer* ChIAss::GUI::renderer=NULL;
SDL_Window* ChIAss::GUI::display_window = NULL;
SDL_Rect ChIAss::GUI::sprite_square = {400, 0, 200, 200};
SDL_Rect ChIAss::GUI::black_pawn_square = {1000, 200, 200, 200};
SDL_Rect ChIAss::GUI::white_pawn_square = {1000, 0, 200, 200};
SDL_Rect ChIAss::GUI::black_knight_square = {600, 200, 200, 200};
SDL_Rect ChIAss::GUI::white_knight_square = {600, 0, 200, 200};
SDL_Rect ChIAss::GUI::black_bishop_square = {400, 200, 200, 200};
SDL_Rect ChIAss::GUI::white_bishop_square = {400, 0, 200, 200};
SDL_Rect ChIAss::GUI::black_rook_square = {800, 200, 200, 200};
SDL_Rect ChIAss::GUI::white_rook_square = {800, 0, 200, 200};
SDL_Rect ChIAss::GUI::black_queen_square = {200, 200, 200, 200};
SDL_Rect ChIAss::GUI::white_queen_square = {200, 0, 200, 200};
SDL_Rect ChIAss::GUI::black_king_square = {0, 200, 200, 200};
SDL_Rect ChIAss::GUI::white_king_square = {0, 0, 200, 200};
SDL_Surface* ChIAss::GUI::pieces_surface = NULL;
SDL_Texture* ChIAss::GUI::pieces_texture = NULL;
ChIAss::Chess::colour ChIAss::GUI::player_turn;
bool ChIAss::GUI::window_is_opened;
bool ChIAss::GUI::player_is_playing;

int ChIAss::GUI::InitInterface(void) {
    using namespace ChIAss::Chess;
    // ======================================= SDL 2 Init =============================================
    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return -1;
    }
    if ( IMG_Init(IMG_INIT_JPG) == -1 ) {
        SDL_Log("Unable to initialize SDL_Image: %s", SDL_GetError());
        return -1;
    }
    display_window = SDL_CreateWindow("Chess Game against : ChIAss",1,1,1600,1000,SDL_WINDOW_SHOWN);//||SDL_WINDOW_FULLSCREEN_DESKTOP);
    if (display_window == NULL) {
        SDL_Log("Unable to create a window...: %s", SDL_GetError());
        return -1;
    }
    renderer = SDL_CreateRenderer(display_window,-1,SDL_RENDERER_SOFTWARE);
    if (renderer == NULL) {
        SDL_Log("Unable to create a renderer...: %s", SDL_GetError());
        return -1;
    }

    pieces_surface = IMG_Load("doc/Skins/1.png");
    if (pieces_surface==NULL) {
        SDL_Log("Unable to load doc/Skins/1.png...: %s", SDL_GetError());
        return -1;
    }
    pieces_texture = SDL_CreateTextureFromSurface(renderer, pieces_surface);
    if (pieces_texture==NULL) {
        SDL_Log("Unable to create piece_texture...: %s", SDL_GetError());
        return -1;
    }


    SDL_Texture* logo_texture = SDL_CreateTextureFromSurface(renderer, IMG_Load("doc/ChIAss_logo.png"));
    if (logo_texture==NULL) {
        SDL_Log("Unable to load doc/ChIAss_logo.png or create a texture of it...: %s", SDL_GetError());
        return -1;
    }
    SDL_RenderCopy(renderer, logo_texture, NULL, NULL);
    SDL_RenderPresent(renderer);
    SDL_Delay(1000);
    SDL_DestroyTexture(logo_texture);

    SDL_SetRenderDrawColor(renderer, 0x91, 0x67, 0x2c, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    window_is_opened = true;
    return 0;
}

int ChIAss::GUI::InteractionWindow(void* n) {
    SDL_Event event;
// Just to shut the warnings
    SDL_PollEvent(&event);

    while ( !player_is_playing ) {
        if (SDL_PollEvent(&event) ) {
            switch (event.type) {
                case SDL_QUIT: // if an event is detected
                    SDL_DestroyWindow(display_window);
                    display_window = NULL;
                    printf("Closing Window\n");
                    return 1;
                case SDL_MOUSEBUTTONDOWN: // If the mouse has been clicked
                    break;
            }
        }
        SDL_Delay(10);
    }
    return 0;
}

void ChIAss::GUI::CloseInterface(void) {
    SDL_DestroyTexture(pieces_texture);
    SDL_FreeSurface(pieces_surface);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(display_window);
    IMG_Quit();
    SDL_Quit();
}
