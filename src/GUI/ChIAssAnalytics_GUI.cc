#include "GUI.hpp"
#include "engine.hpp"


int ChIAss::GUI::ChIAssAnalytics_GUI(void* data) {
    ChIAss::GUI::ChIAssMoveData* data_ = (ChIAss::GUI::ChIAssMoveData*) data;
    ChIAssAnalytics(data_->_board,
                    data_->_ChIAss_colour,
                    data_->_depth,
                    data_->_last_move_played);

    data_->ChIAss_is_playing = false;
    return 0;
}