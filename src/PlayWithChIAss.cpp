#include "engine.hpp"
#include "GUI.hpp"
#include "opening/opening.hpp"

/*#include <chrono>
std::chrono::steady_clock::time_point beg;
std::chrono::steady_clock::time_point end;*/

ufast8 depth=6;
i8 victory = -1;

int main(void) {
    ChIAss::Chess::Board board;
    ChIAss::Chess::InitBoard( &board );
    if ( ChIAss::GUI::InitInterface() != 0 ) {
        exit(EXIT_FAILURE);
    }

    victory = -1;

    ChIAss::Chess::colour ChIAss_colour = ChIAss::Chess::WHITE;
    ChIAss::Chess::colour player_colour = (ChIAss::Chess::colour) !ChIAss_colour;
    ChIAss::GUI::player_turn = ChIAss::Chess::WHITE;
    ChIAss::GUI::player_is_playing = (ChIAss::GUI::player_turn==player_colour);
    int round_counter=0;

    ChIAss::GUI::DisplayBoard(board, player_colour);

    ChIAss::Chess::Move played;
    ChIAss::Chess::Move last_played;


    SDL_Thread *thread_window = NULL;

//  Opening phase
    if (0 != ChIAss::Opening::LoadOpening((char*) "doc/Openings/opening_white.txt")) {
        printf("Could not load the opening...\n");
        return -1;
    }
    while ( ChIAss::Engine::Open(board, &played) && victory == -1 ) {
        ChIAss::Chess::Play(&board, played);
        if ( ChIAss::GUI::display_window != NULL ) {
            ChIAss::GUI::DisplayBoard(board, player_colour);
        } else {
            victory = 1;
            continue;
        }


        ChIAss::GUI::player_turn = player_colour;
        last_played = played;
        played = {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square,ChIAss::Chess::NORMAL_MOVE};
        if ( ChIAss::GUI::GraphicInterfaceGetMove( &board, &played, player_colour, last_played) == 0 ) {
            victory = 1;
            break;
            continue;
        }

        ChIAss::Chess::Play(&board, played);
        if ( ChIAss::GUI::display_window != NULL ) {
            ChIAss::GUI::DisplayBoard(board, player_colour);
        } else {
            victory = 1;
            continue;
        }
        last_played = played;


        round_counter ++;
    }

    while (victory == -1) {
        printf("ChIAss playing...\n");
        ChIAss::GUI::player_is_playing = false;
        thread_window = SDL_CreateThread( ChIAss::GUI::InteractionWindow, "InteractionWindow", NULL );
        played = ChIAss::Engine::ChIAssMove(&board, ChIAss_colour, depth, last_played);
        printf("ChIAss played!\n");
        if ( played != (ChIAss::Chess::Move) {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square} ) {
            ChIAss::Chess::Play(&board, played);
        } else {
            victory = 0;
            break;
        }
        if ( ChIAss::GUI::display_window != NULL ) {
            ChIAss::GUI::DisplayBoard(board, player_colour);
        } else {
            victory = 1;
            continue;
        }

//  To close the interaction thread
        ChIAss::GUI::player_is_playing = true;
        ChIAss::GUI::player_turn = player_colour;
        last_played = played;
        played = {ChIAss::Chess::not_a_square,ChIAss::Chess::not_a_square,ChIAss::Chess::NORMAL_MOVE};
        if ( ChIAss::GUI::GraphicInterfaceGetMove( &board, &played, player_colour, last_played) == 0 ) {
            victory = 1;
            break;
            continue;
        }   

        ChIAss::Chess::Play(&board, played);
        if ( ChIAss::GUI::display_window != NULL ) {
            ChIAss::GUI::DisplayBoard(board, player_colour);
        } else {
            victory = 1;
            continue;
        }
        last_played = played;
        round_counter ++;
    }

    SDL_DetachThread(thread_window);
    ChIAss::GUI::CloseInterface();
    if (victory == 1) {
        printf("ChIAss WINS!\n");
    }
    if (victory == 0) {
        printf("ChIAss LOSES...\n");
    }
    return 0;
}