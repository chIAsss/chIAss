#include "hash.hpp"
const u64 hash_weights[] = {19072022, 2026017569, 1124569336, 1518446530, 17699482, 1258446811, 11102018, 299792458};

u64 ChIAss::Hash(const ChIAss::Chess::Board b) {
    u64 h=0;
    u64* x_i;// dodges the warning
    for (size_t i=0; i<8; i++) {
        x_i = (u64*) &(b.B[8*i]);//b.B+8*i;
        h += hash_weights[i] * (*x_i);
    }
    return h;
}

// u64 ChIAss::Hash64(const ChIAss::Chess::Board b) {
//     u64 h=0;
//     u64* x_i;// dodges the warning
//     for (size_t i=0; i<8; i++) {
//         x_i = (u64*) &(b.B[8*i]);//b.B+8*i;
//         h += hash_weights[i] * (*x_i);
//     }
//     return h;
// }