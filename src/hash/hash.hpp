#ifndef CHIASS_HASH_HPP
#define CHIASS_HASH_HPP
#include "chess.hpp"


namespace ChIAss {
    /*
        \brief Hash function to hash boards. 
        It's a terrible hash function and should never, under no circumstances, be considered to work.
        \param b The board to hash
        \return a 64-bit integer corresponding to the hash of the board.
    */
    u64 Hash(const Chess::Board b);
    /*
        \brief 64-bits hash function to hash boards. 
        It's the same function as Hash.
        \param b The board to hash
        \return a 64-bit integer corresponding to the hash of the board.
    */
    u64 Hash64(const Chess::Board b);
    /*
        \brief 32-bits hash function to hash boards. 
        It's a terrible hash function and should never, under no circumstances, be considered to work.
        \param b The board to hash
        \return a 32-bit integer corresponding to the hash of the board.
    */
    u32 Hash32(const Chess::Board b);
}

#endif // CHIASS_HASH_HPP