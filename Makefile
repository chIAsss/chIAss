#SHELL = /bin/sh

#	Creates build directories if they don't already exist
$(shell mkdir -p build/engine build/chess build/GUI build/opening)

CXX=g++
INCLUDE=$(addprefix -I, include/cute src/chess src/engine src/GUI src/opening src/hash)
CXXFLAGS=-Wall -O3
LDLIBS=-lSDL2 -lSDL2_image -lSDL2_ttf -fopenmp





CHESS_O_FILES=$(addsuffix .o,$(addprefix build/chess/, chess Play Unplay IsKingSafe IsMoveLegal ConvertMove))

ENGINE_BASIC_O_FILES=$(addsuffix .o,$(addprefix build/engine/, CheckCheck BlockLineOfSight TakeCheckingPiece AllPossibleMovesCheck \
AllMovesRook AllMovesKnight AllMovesRookCanCastle AllMovesBishop AllMovesPawn AllMovesStartingPawn \
AllMovesKingCanCastle AllMovesKing AllMovesQueen IsPinnedDown AllPossibleMoves Moves))

ENGINE_O_FILES= $(ENGINE_BASIC_O_FILES) $(addsuffix .o,$(addprefix build/engine/, ValueBoard ValueMove ChIAssMove ) )

GUI_O_FILES=$(addsuffix .o,$(addprefix build/GUI/,interface DisplayBoard DisplayMove \
GraphicUpdateBoard GraphicInterfaceGetMove))

OPENING_O_FILES=$(addsuffix .o,$(addprefix build/opening/, LoadOpening Open ClearOpening hash))

O_FILES=$(addsuffix .o,$(addprefix build/, ChIAss_chess ChIAss_GUI ChIAss_engine ChIAss_opening))



# MAIN PROGRAM ====================================================================================
PlayWithChIAss: src/PlayWithChIAss.cpp $(O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $< $(O_FILES) -o $@ `sdl2-config --cflags --libs` $(LDLIBS)

AskChIAss: src/AskChIAss.cpp $(O_FILES) build/GUI/ChIAssMove_GUI.o \
build/GUI/ChIAssAnalytics.o build/GUI/ChIAssAnalytics_GUI.o build/GUI/SetPosition_GUI.o 
	$(CXX) $(CXXFLAGS) $(INCLUDE) $< $(O_FILES) build/GUI/ChIAssMove_GUI.o \
	build/GUI/SetPosition_GUI.o \
	build/GUI/ChIAssAnalytics.o build/GUI/ChIAssAnalytics_GUI.o \
	-o $@ `sdl2-config --cflags --libs` $(LDLIBS)
# END MAIN PROGRAM ================================================================================	

# TESTS ===========================================================================================
test_chess: test/test_chess.cpp $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $< $(CHESS_O_FILES) -o $@

test_moves: test/test_moves.cpp $(ENGINE_BASIC_O_FILES) $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(ENGINE_BASIC_O_FILES) $<  $(CHESS_O_FILES) -o $@

test_mates: test/test_mates.cpp $(ENGINE_O_FILES) $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $< $(ENGINE_O_FILES) $(CHESS_O_FILES) -o $@ -fopenmp

test_time: test/test_time.cpp $(ENGINE_O_FILES) $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $< $(ENGINE_O_FILES) $(CHESS_O_FILES) -o $@ -fopenmp
# END TESTS =======================================================================================

# GENERAL =========================================================================================
build/chess/%.o: src/chess/%.cc src/chess/chess.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@
build/ChIAss_chess.o: src/chess/chess.hpp $(CHESS_O_FILES)
	ld -r $(CHESS_O_FILES) -o $@

build/engine/%.o: src/engine/%.cc src/engine/engine.hpp $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ -fopenmp
build/ChIAss_engine_basic.o: src/engine/engine.hpp $(ENGINE_BASIC_O_FILES)
	ld -r $(ENGINE_BASIC_O_FILES) -o $@
build/ChIAss_engine.o: src/engine/engine.hpp $(ENGINE_O_FILES)
	ld -r $(ENGINE_O_FILES) -o $@

build/GUI/%.o: src/GUI/%.cc src/GUI/GUI.hpp $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ `sdl2-config --cflags --libs` $(LDLIBS)
build/ChIAss_GUI.o: src/GUI/GUI.hpp $(GUI_O_FILES)
	ld -r $(GUI_O_FILES) -o $@

build/opening/%.o: src/opening/%.cc src/opening/opening.hpp $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDLIBS)
build/opening/hash.o: src/hash/hash.cc src/opening/opening.hpp $(CHESS_O_FILES)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDLIBS)
build/ChIAss_opening.o: src/opening/opening.hpp $(OPENING_O_FILES)
	ld -r $(OPENING_O_FILES) -o $@
# END GENERAL =====================================================================================

clean:
	rm -f test_* PlayWithChIAss AskChIAss build/*.o build/engine/*.o build/GUI/*.o build/chess/*.o
