NEWS
====

This file is an attempt at giving an overview of the changes between each ChIAss version since the 2.0. <br>
It also contains a list of the different bug fixes, for said versions. <br>
The changes and fixes that happened in version 1 are considered irrelevant and not listed here.


2.6.2 Hotfix
--------------------
   * Correcting a bug in TakeCheckingPiece that allowed pinned black pawns to take a piece checking their king.


2.6.1 Hotfix
--------------------
 * A new function was added : ConvertMove, to help with the opening.
 * A new case "not_a_square" with value 65 has been added, to have a proper "give up" move.
 * Bugs fixed : 
    - The bug in CheckCheck which caused checks to be ignored if they happened after an en passant.
      A function has been added in CheckCheck, for the en passant case.
    - A bug in TakeCheckingPiece (line 218).
    - A bug in CheckCheck that hid checks from a1 to h8 specifically. 
      The order of verification has simply been changed, placing UP-RIGHT before UP-LEFT
    - A small bug in the GUI that let the user click under the chess board. Could've lead to strange behaviour.
    - The reloading of the pieces' sprite in DisplayMove.
 * A pretty big trimming happened, removing a lot of useless namespace qualification.
 * Still adding lines to the opening moves.
 * Renamed ChIAss.hpp and interface.hpp into engine.hpp/GUI.hpp
 * ChIAss is considered stable. Even though, as history has shown, it's probably not.


2.6.0 Openings!!!
--------------------
 * ChIAss now has an opening book!!!
    - It is (obviously) terrible
    - A hash function has also been added for it to work
    - You can write your own opening book
 * ChIAss is stabler than ever!
 * Functions of the 2.5 update have been added.
 * Pieces skins have been added in the doc/skins directory


2.5.1 En passant, chapter 3
--------------------
 * The last fixes have been made to the en passant, regarding the check situation
 * ChIAss is now (supposedly) stable!


2.5 Correcting old code
--------------------
 * The functions described were not added because of performance issues. Further testing is necessary to determine if those performance issues are caused by code alignement, efficiency in function design or correctness.
 * AllPossibleMovesCheck update : 
    - Old design : calculate every move, before keeping only the legal ones.
    - New design : Calls two different function : BlockLineOfSight and TakeCheckingPiece
 * BlockLineOfSight : 
    - Checks every square between the king and the piece checking it
    - at every square, finds the pieces that can reach it
 * TakeCheckingPiece : 
    - Finds all the moves that take the piece checking the king. 
 * A few modifications were made to CheckCheck :  
    - The return values changed. 
    - And it's a bit cleaner now.
 * Known issue(s) : 
    - The en passant is still not perfectly working in edge cases : like a check situation.


2.4 En passant, chapter 2
--------------------
 * The en passant has been fixed, and now ChIAss can play it too!
 * Multiples functions have seen their arguments rewritten. Adding 'const',
   replacing arguments with their addresses. 


2.3.5 Namespaces
--------------------
 * Introduced namespace, corresponding to the different programs handling : chess, the engine and the GUI.
 * Files, functions and directories have been renamed, to clean up.


2.3.4 En passant, chapter 1
--------------------
 * Fixed a bug preventing the player to take a pawn with an en passant, using the GUI.


2.3.3 So many bugs...
--------------------
After a lot of work, tracking down every bug, rewriting the chess program multiple times, ChIAss is now more stable!<br>
 * Fixed bugs in ValidKingMove.
 * Fixed bugs in CheckCheck, IsPinnedDown.


2.3 Multithreading
--------------------
ChIAss now uses multithreading, in some capacity, to accelerate its search. <br>
Its multithreading is very basic and is based on the OpenMP API.


2.2 Alpha-Beta pruning
--------------------
An improvement to the tree search, known as "alpha-beta pruning" has been added. <br>
ChIAss is better, but still pretty bad. <br>


2.1 GUI
--------------------
A simple graphical user interface has been added for the user to play against ChIAss, using SDL2.


2.0 ChIAss is back!
--------------------
The entire chess program has been rewritten with new types to allow for all moves to be played.<br>
ChIAss can now theoretically play every possible move, except for the promotions into Bishop and Rook...