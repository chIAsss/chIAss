# Installation
## Dependencies
ChIAss is supposed to be as minimal as possible, but the interface itself requires the use of the SDL2 library. 
Also, the tests, which are not essential to ChIAss, work with the CUTE library.

## Quick installation
Install SDL2, SDL_Image, then
	make PlayWithChIAss 
	./PlayWithChIAss