![ChIAss_logo](doc/ChIAss_logo.png "ChIAss's logo")
**ChIAss** is a simple AI project for the game of chess.<br>
It is mainly written in C, but uses some C++ features for convenience reasons.<br>
The project started on Tuesday 19/07/2022 and was active until its first checkpoint on Friday 29/07/2022.<br>
At this date, the AI was pretty much working, although really slow and not able to perform "complex" moves such as castling. This version is called ChIAss_v1 for obvious reasons and is still up as a landmark. See https://gitlab.com/Poubenne/ChIAsss/ChIAss_v1 for more.<br>
The present ChIAss is the continuation of this first version. It is still active to this day.<br>

## Version
It doesn't mean much, but this is the 2.6.2 version. See NEWS.

### Compiling
A makefile is provided, with very few options, to help you compile.

## Performance
ChIAss explores all possibilities up to depth 6, with a terrible, terrible, terrible board evaluation function. 

## Could Have Been Frequently Asked Questions
### Why C?
Because fast.

### Why C++?
Because CUTE.

### Why SDL2?
Because pain.

### AI good?
AI bad.

### AI elo?
1871 http://elometer.net.

## Future
ChIAss might keep on getting updates. It might get : 
 - ~~a graphic interface~~
 - ~~multithreading~~
 - ~~diverse openings~~
 - machine learning
 - hash optimizations
 - randomness
 - different skill levels
 - a playable online version

Or it might get forgotten, buried and replaced by the chess AI from the KFC restaurant in Karlsruhe.