/*
Test the chess implementation for castling, promotion and en passant
*/

#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_counting_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "chess.hpp"

using namespace ChIAss::Chess;

void TestPlayUnplay(void) {
    Board b;
    std::vector<PieceAndPosition> initial_positions_black;
    std::vector<PieceAndPosition> initial_positions_white;
    std::vector<PieceAndPosition> tested_black;
    std::vector<PieceAndPosition> tested_white;
    std::vector<PieceAndPosition> expected_black;
    std::vector<PieceAndPosition> expected_white;
    Move m;
    Piece taken;


    fprintf(stdout, "\nTesting the Play/Unplay functions : classic move\t\t"); {
        initial_positions_black = {{KING,BLACK,e8}, {PAWN,BLACK,e6}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING,WHITE,e1}, {STARTING_PAWN,WHITE,e2}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m=(Move){e6,e5};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,e8}, {PAWN,BLACK,e5}};
        expected_white={{KING,WHITE,e1}, {STARTING_PAWN,WHITE,e2}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );


        initial_positions_black = {{KING,BLACK,e8}, {PAWN,BLACK,e5}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING,WHITE,e1}, {PAWN,WHITE,e4}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
    
        m=(Move){e4,e5};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING,BLACK,e8}};
        expected_white={{KING,WHITE,e1}, {PAWN,WHITE,e5}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );
    
        fprintf(stdout, "passed!\n");
    }


    fprintf(stdout, "\nTesting the Play/Unplay functions : PAWN first move\t\t"); {
        initial_positions_black = {{KING,BLACK,e8}, {STARTING_PAWN,BLACK,a7}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING,WHITE,e1}, {STARTING_PAWN,WHITE,e2}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e2, e3, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING,BLACK,e8}, {STARTING_PAWN,BLACK,a7}};
        expected_white={{KING,WHITE,e1}, {PAWN,WHITE,e3}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );


        initial_positions_black = {{KING,BLACK,e8}, {STARTING_PAWN,BLACK,a7}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING,WHITE,e1}, {STARTING_PAWN,WHITE,e2}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e2, e4, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING,BLACK,e8}, {STARTING_PAWN,BLACK,a7}};
        expected_white={{KING,WHITE,e1}, {PAWN,WHITE,e4}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );

        fprintf(stdout,"passed!\n");
    }


    fprintf(stdout, "\nTesting the Play/Unplay functions : PAWN promotion\t\t"); {
        initial_positions_black = {{KING,BLACK,e8}, {BISHOP,BLACK,a8}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING,WHITE,e1}, {PAWN,WHITE,b7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {b7, a8, PROMOTION_QUEEN};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING,BLACK,e8}};
        expected_white={{KING,WHITE,e1}, {QUEEN,WHITE,a8}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );

        fprintf(stdout,"passed!\n");
    }


    fprintf(stdout, "\nTesting the Play/Unplay functions : Castling\t\t\t"); {
        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {ROOK_CAN_CASTLE,BLACK,a8}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {ROOK_CAN_CASTLE,WHITE,a1}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );

        m = {a1, d1, CASTLE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}, {ROOK_CAN_CASTLE,BLACK,a8}};
        expected_white={{KING,WHITE,c1}, {ROOK,WHITE,d1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );


        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {ROOK_CAN_CASTLE,BLACK,a8}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {ROOK_CAN_CASTLE,WHITE,h1}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );

        m = {h1, f1, CASTLE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}, {ROOK_CAN_CASTLE,BLACK,a8}};
        expected_white={{KING,WHITE,g1}, {ROOK,WHITE,f1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );


        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {ROOK_CAN_CASTLE,BLACK,a8}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {ROOK_CAN_CASTLE,WHITE,a1}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );

        m = {a8, d8, CASTLE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,c8}, {ROOK,BLACK,d8}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}, {ROOK_CAN_CASTLE,WHITE,a1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );


        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {ROOK_CAN_CASTLE,BLACK,h8}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {ROOK_CAN_CASTLE,WHITE,a1}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );

        m = {h8, f8, CASTLE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,g8}, {ROOK,BLACK,f8}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}, {ROOK_CAN_CASTLE,WHITE,a1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );

        fprintf(stdout,"passed!\n");
    }


    fprintf(stdout, "\nTesting the Play/Unplay functions : WHITE KING first move\t"); {
        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,d1}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,f8}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e1, d1, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}};
        expected_white={{KING,WHITE,d1}, {BISHOP,WHITE,f8}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,d2}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,f7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e1, d2, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}};
        expected_white={{KING,WHITE,d2}, {BISHOP,WHITE,f7}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,e2}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,e7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e1, e2, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}};
        expected_white={{KING,WHITE,e2}, {BISHOP,WHITE,e7}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,f2}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,d7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e1, f2, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}};
        expected_white={{KING,WHITE,f2}, {BISHOP,WHITE,d7}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,f1}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,d8}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e1, f1, FIRST_MOVE};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}};
        expected_white={{KING,WHITE,f1}, {BISHOP,WHITE,d8}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );

        fprintf(stdout,"passed!\n");
    }


    fprintf(stdout, "\nTesting the Play/Unplay functions : BLACK KING first move\t"); {
        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,d1}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,f8}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e8, f8, FIRST_MOVE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,f8}, {BISHOP,BLACK,d1}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,d2}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,f7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e8, f7, FIRST_MOVE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,f7}, {BISHOP,BLACK,d2}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,e2}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,e7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e8, e7, FIRST_MOVE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,e7}, {BISHOP,BLACK,e2}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,f2}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,d7}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e8, d7, FIRST_MOVE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,d7}, {BISHOP,BLACK,f2}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );



        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {BISHOP,BLACK,f1}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {BISHOP,WHITE,d8}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        
        m = {e8, d8, FIRST_MOVE};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING,BLACK,d8}, {BISHOP,BLACK,f1}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_black, tested_white);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );

        fprintf(stdout,"passed!\n");
    }



    fprintf(stdout, "\nTesting the Play/Unplay functions : en passant\t\t\t"); {
        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {PAWN,BLACK,a5}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {PAWN,WHITE,b5}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        m = {b5, a6, EN_PASSANT};
        Play(&b, m, &taken, tested_white, tested_black);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}, {PAWN,WHITE,a6}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );

        Unplay(&b, m, taken, tested_white, tested_black);

        ASSERT_EQUAL( initial_positions_black, tested_black );
        ASSERT_EQUAL( initial_positions_white, tested_white );


        initial_positions_black = {{KING_CAN_CASTLE,BLACK,e8}, {PAWN,BLACK,e4}};
        tested_black = initial_positions_black;
        initial_positions_white = {{KING_CAN_CASTLE,WHITE,e1}, {PAWN,WHITE,f4}};
        tested_white = initial_positions_white;
        SetPosition( &b , initial_positions_white , initial_positions_black );
        m = {e4, f3, EN_PASSANT};
        Play(&b, m, &taken, tested_black, tested_white);
        expected_black={{KING_CAN_CASTLE,BLACK,e8}, {PAWN,BLACK,f3}};
        expected_white={{KING_CAN_CASTLE,WHITE,e1}};
        ASSERT_EQUAL( expected_black, tested_black );
        ASSERT_EQUAL( expected_white, tested_white );


        fprintf(stdout, "passed!\n");
    }
}

void TestPawnMiscellaneous(void) {
    Board expected;
    Board tested;
    Piece taken;
    Move m;

    fprintf(stdout, "\nTesting PAWN first move : One square\t\t"); {
        InitBoard( &expected );
        InitBoard( &tested );

        m = {e2, e3, FIRST_MOVE};
        Play( &tested , m , &taken);
        expected.B[ e2 ] = {EMPTY,WHITE};
        expected.B[ e3 ] = {PAWN ,WHITE};
        ASSERT_EQUAL( expected , tested );


        m = {d7, d6, FIRST_MOVE};
        Play( &tested , m , &taken);
        expected.B[ d7 ] = {EMPTY,WHITE};
        expected.B[ d6 ] = {PAWN ,BLACK};
        ASSERT_EQUAL( expected , tested );

        Unplay( &tested, m, taken );
        expected.B[ d7 ] = {STARTING_PAWN,BLACK};
        expected.B[ d6 ] = {EMPTY,WHITE};
        ASSERT_EQUAL( expected , tested );


        fprintf(stdout, "passed!\n");
    }

    fprintf(stdout, "Testing PAWN first move : taking\t\t"); {
        InitBoard( &expected );
        InitBoard(  &tested  );
        tested.B[d3] = {KNIGHT,BLACK};

        m = {e2, d3, FIRST_MOVE};
        Play( &tested , m , &taken);
        expected.B[ e2 ] = {EMPTY,WHITE};
        expected.B[ d3 ] = {PAWN ,WHITE};
        ASSERT_EQUAL( expected , tested );

        Unplay( &tested, m, taken );
        expected.B[ d3 ] = {KNIGHT,BLACK};
        expected.B[ e2 ] = {STARTING_PAWN,WHITE};
        ASSERT_EQUAL( expected , tested );


        fprintf(stdout, "passed!\n");
    }

    fprintf(stdout, "Testing PAWN first move : Two squares\t\t"); {
        InitBoard( &expected );
        InitBoard( &tested );

        m = {e2, e4, FIRST_MOVE};
        Play( &tested , m , &taken);
        expected.B[ e2 ] = {EMPTY,WHITE};
        expected.B[ e4 ] = {PAWN ,WHITE};
        ASSERT_EQUAL( expected , tested );


        m = {d7, d5, FIRST_MOVE};
        Play( &tested , m , &taken);
        expected.B[ d7 ] = {EMPTY,WHITE};
        expected.B[ d5 ] = {PAWN ,BLACK};
        ASSERT_EQUAL( expected , tested );

        Unplay( &tested , m, taken);
        expected.B[ d7 ] = {STARTING_PAWN,BLACK};
        expected.B[ d5 ] = {EMPTY,WHITE};
        ASSERT_EQUAL( expected , tested );


        fprintf(stdout, "passed!\n");
    }

    fprintf(stdout, "Testing PAWN promotion\t\t\t\t"); {
        SetPosition( &expected , { { QUEEN, WHITE, e8}} );
        SetPosition( &tested , { { PAWN, WHITE, e7 }} );
        m = {e7, e8, PROMOTION_QUEEN };
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { PAWN, WHITE, e7 }} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { KNIGHT, WHITE, e8}} );
        SetPosition( &tested , { { PAWN, WHITE, e7 }} );
        m = {e7, e8, PROMOTION_KNIGHT };
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { PAWN, WHITE, e7 }} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );


        fprintf(stdout, "passed!\n");
    }

    fprintf(stdout, "Testing PAWN promotion : taking\t\t\t"); {
        SetPosition( &expected , { { QUEEN, WHITE, d8}} );
        SetPosition( &tested , { { PAWN, WHITE, e7 }, {KNIGHT, BLACK, d8} } );
        m = {e7, d8, PROMOTION_QUEEN};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { PAWN, WHITE, e7 }, {KNIGHT, BLACK, d8} } );
        Unplay( &tested, m, taken );
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { KNIGHT, WHITE, d8}} );
        SetPosition( &tested , { { PAWN, WHITE, e7 }, {BISHOP, BLACK, d8} } );
        m = {e7, d8, PROMOTION_KNIGHT};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { PAWN, WHITE, e7 }, {BISHOP, BLACK, d8} } );
        Unplay( &tested, m, taken );
        ASSERT_EQUAL( expected , tested );


        fprintf(stdout, "passed!\n");
    }


    fprintf(stdout, "Testing en passant\t\t\t\t"); {
        SetPosition( &expected , { { PAWN, WHITE, a6}} );
        SetPosition( &tested , { { PAWN, BLACK, a5}, { PAWN, WHITE, b5}} );
        m = {b5, a6, EN_PASSANT};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { PAWN, BLACK, a5}, { PAWN, WHITE, b5}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { PAWN, BLACK, f3}} );
        SetPosition( &tested , { { PAWN, BLACK, e4}, { PAWN, WHITE, f4}} );
        m = {e4, f3, EN_PASSANT};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { PAWN, BLACK, e4}, { PAWN, WHITE, f4}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );


        fprintf(stdout, "passed!\n");
    }
}

void TestRookMiscellaneous( void ) {
    Board expected;
    Board tested;
    Piece taken;
    Move m;

    fprintf(stdout, "Testing ROOK first move \t\t\t"); {
        SetPosition( &expected , { { ROOK, WHITE, a6}} );
        SetPosition( &tested , { { ROOK_CAN_CASTLE, WHITE, a1}, { PAWN, BLACK, a6}} );
        m = {a1,a6, FIRST_MOVE};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { ROOK_CAN_CASTLE, WHITE, a1}, { PAWN, BLACK, a6}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );



        SetPosition( &expected , { { ROOK, WHITE, c1}} );
        SetPosition( &tested , { { ROOK_CAN_CASTLE, WHITE, h1}, { PAWN, BLACK, c1}} );
        m = {h1,c1, FIRST_MOVE};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { ROOK_CAN_CASTLE, WHITE, h1}, { PAWN, BLACK, c1}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );



        SetPosition( &expected , { { ROOK, BLACK, e8}} );
        SetPosition( &tested , { { ROOK_CAN_CASTLE, BLACK, a8}, { PAWN, WHITE, e8}} );
        m = {a8,e8, FIRST_MOVE};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { ROOK_CAN_CASTLE, BLACK, a8}, { PAWN, WHITE, e8}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );



        SetPosition( &expected , { { ROOK, BLACK, h4}} );
        SetPosition( &tested , { { ROOK_CAN_CASTLE, BLACK, h8}, { PAWN, WHITE, h4}} );
        m = {h8,h4, FIRST_MOVE};
        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { ROOK_CAN_CASTLE, BLACK, h8}, { PAWN, WHITE, h4}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );



        fprintf(stdout, "passed!\n");
    }
}


void TestKingMiscellaneous( void ) {
    Board expected;
    Board tested;
    Piece taken;
    Move m;

    fprintf(stdout, "Testing King first move \t\t\t"); {
        SetPosition( &expected , { { KING, WHITE, d2}} );
        SetPosition( &tested , { { KING_CAN_CASTLE, WHITE, e1}, { PAWN, BLACK, d2}} );
        m = {e1,d2, FIRST_MOVE};

        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { KING_CAN_CASTLE, WHITE, e1}, { PAWN, BLACK, d2}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );


        SetPosition( &expected , { { KING, BLACK, d7}} );
        SetPosition( &tested , { { KING_CAN_CASTLE, BLACK, e8}, { PAWN, WHITE, d7}} );
        m = {e8,d7, FIRST_MOVE};

        Play( &tested , m , &taken);
        ASSERT_EQUAL( expected , tested );

        SetPosition( &expected , { { KING_CAN_CASTLE, BLACK, e8}, { PAWN, WHITE, d7}} );
        Unplay( &tested , m, taken);
        ASSERT_EQUAL( expected , tested );



        fprintf(stdout, "passed!\n");
    }
}

int main(void) {
//	cute::counting_listener<> listener{};
	cute::ide_listener<> listener{};

    cute::suite suite_of_tests;
    suite_of_tests.push_back( CUTE(TestPlayUnplay));
    suite_of_tests.push_back( CUTE(TestPawnMiscellaneous));
    suite_of_tests.push_back( CUTE(TestRookMiscellaneous));
    suite_of_tests.push_back( CUTE(TestKingMiscellaneous));
    

	cute::makeRunner(listener)(suite_of_tests,"All tests\n");

/*
	std::cerr <<"\n"<< listener.failedTests<<"/"<<listener.numberOfTests<< " Failed" << std::endl;
	std::cerr << listener.errors << " Errors - expect 0 errors" << std::endl;*/
    return 0;
}