#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "engine.hpp"
#include <chrono>

using namespace ChIAss::Chess;
using namespace ChIAss::Engine;

void TestTimeFullBoard(void) {
	fprintf(stdout, "Testing performance on a full board after a standard opening\n");
	Board b;
	InitBoard( &b);
	std::vector<Move> m ={{d2,d4, FIRST_MOVE}, {d7,d5, FIRST_MOVE}, {b1,c3}, {g8,f6}, {c1,f4}, {b8,c6}};
	for (size_t i=0; i<m.size(); i++) {
		Play(&b, m[i]);
	}

	for ( ufast8 depth = 2 ; depth < 8 ; depth++ ) {	
		fprintf(stdout, "\tDepth = %u :\t", depth);
		auto start = std::chrono::steady_clock::now();
		ChIAssMove( &b , WHITE, depth, m.back());
		auto end = std::chrono::steady_clock::now();
		std::cout<< "Time : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
	}
}



void TestTimeMidGamePosition(void) {
	fprintf(stdout, "Testing performance midgame position\n");
	Board b;
    std::vector<PieceAndPosition> white_pieces = { {KING,WHITE,g1}, {PAWN,WHITE,a6}, {STARTING_PAWN,WHITE,c2}, {PAWN,WHITE,e5},
	{PAWN,WHITE,f4}, {STARTING_PAWN,WHITE,g2}, {STARTING_PAWN,WHITE,h2},
	{BISHOP,WHITE,e4}, {BISHOP,WHITE,c1}, {ROOK,WHITE,b3}, {ROOK,WHITE,f1}, {QUEEN,WHITE,d3}  };
    std::vector<PieceAndPosition> black_pieces = { {KING,BLACK,b8}, {STARTING_PAWN,BLACK,a7}, {PAWN,BLACK,b6}, {PAWN,BLACK,c3},
	{PAWN,BLACK,d4}, {PAWN,BLACK,e6}, {STARTING_PAWN,BLACK,f7}, 
	{KNIGHT,BLACK,e3}, {KNIGHT,BLACK,f5}, {ROOK,BLACK,d7}, {ROOK,BLACK,g8}, {QUEEN,BLACK,e7} };
	SetPosition( &b , white_pieces, black_pieces);
	Move m ={h6,f5};

	for ( ufast8 depth = 2 ; depth < 7 ; depth++ ) {
		fprintf(stdout, "\tDepth = %u :\t", depth);
		auto start = std::chrono::steady_clock::now();
		ChIAssMove( &b , WHITE, depth, m);
		auto end = std::chrono::steady_clock::now();
		std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
	}
}



int main(void) {
	TestTimeFullBoard();
	TestTimeMidGamePosition();

}
