#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_counting_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "engine.hpp"
#include <cstdlib>

using namespace ChIAss::Chess;
using namespace ChIAss::Engine;

void TestPawnMoves(void) {
    fprintf( stdout , "Testing PAWN moves\n" );
        fprintf( stdout , "\tTesting WHITE PAWN\n"); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e1}, 
            {PAWN, WHITE, a3}, {PAWN, WHITE, a4}, {PAWN, WHITE, a5}, 
            {PAWN, WHITE, c3}, {PAWN, WHITE, c4},
            {PAWN, WHITE, h3}, {PAWN, WHITE, h4}, {PAWN, WHITE, h5} };
            std::vector<PieceAndPosition> initial_positions_b { {KING,BLACK,e8}, 
            {PAWN, BLACK, b4}, {PAWN, BLACK, c5}, {PAWN, BLACK, g4} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected { {e1,d1}, {e1,d2}, {e1,e2}, {e1,f2}, {e1,f1},
            {a3,b4}, {a5,a6},
            {c3,b4},
            {h3,g4}, {h5,h6}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tTesting BLACK PAWN\n");{
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e1}, 
            {PAWN, WHITE, b5}, {PAWN, WHITE, c4}, {PAWN, WHITE, g4}, {PAWN, WHITE, h4}};
            std::vector<PieceAndPosition> initial_positions_b { {KING,BLACK,e8}, 
            {PAWN, BLACK, a5}, {PAWN, BLACK, a6}, {PAWN, BLACK, d5},
            {PAWN, BLACK, e5}, {PAWN, BLACK, e6}, {PAWN, BLACK, h5},
            {PAWN, BLACK, h6} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected = { {e8,d8}, {e8,d7}, {e8,e7}, {e8,f7}, {e8,f8},
            {a5,a4}, {a6,b5},
            {d5,c4}, {d5,d4},
            {e5,e4}, {h5,g4}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_b ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
}


void TestKnightMoves(void) {
    fprintf( stdout , "Testing KNIGHT moves\n" );
        fprintf( stdout , "\tKNIGHT alone\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w = { {KING,WHITE,h1}, {KNIGHT, WHITE, d4} };
            SetPosition( &testing_board , initial_positions_w );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {d4,c2}, {d4,b3}, {d4,b5}, {d4,c6}, {d4,e6}, {d4,f5}, {d4,f3}, {d4,e2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }

        fprintf( stdout , "\tKNIGHT blocked\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w = { {KING,WHITE,h1}, {KNIGHT, WHITE, d4}, 
            {PAWN,WHITE,b3}, {PAWN,WHITE,f3}, {PAWN,WHITE,c2}, {PAWN,WHITE,e2} };
            SetPosition( &testing_board , initial_positions_w );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {d4,b5}, {d4,c6}, {d4,e6}, {d4,f5},
            {b3,b4}, {f3,f4}, {c2,c3}, {e2,e3}};//    PAWN moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");

            initial_positions_w = { {KING,WHITE,h1}, {KNIGHT, WHITE, d4}, 
            {PAWN,WHITE,b5}, {PAWN,WHITE,c6}, {PAWN,WHITE,e6}, {PAWN,WHITE,f5} };
            SetPosition( &testing_board , initial_positions_w );
            expected = { {h1,g1}, {h1,g2}, {h1,h2},
            {d4,c2}, {d4,b3}, {d4,f3}, {d4,e2},
            {b5,b6}, {c6,c7}, {e6,e7}, {f5,f6}};// PAWN moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }

        fprintf( stdout , "\tKNIGHT taking\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w = { {KING,WHITE,h1}, {KNIGHT, WHITE, d4}};
            std::vector<PieceAndPosition> initial_positions_b = {{PAWN,BLACK,b5}, {PAWN,BLACK,c6}, {PAWN,BLACK,e6}, {PAWN,BLACK,f5},
            {PAWN,BLACK,b4}, {PAWN,BLACK,f4}, {PAWN,BLACK,c3}, {PAWN,BLACK,e3}};
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {d4,c2}, {d4,b3}, {d4,b5}, {d4,c6}, {d4,e6}, {d4,f5}, {d4,f3}, {d4,e2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }

        fprintf( stdout , "\tKNIGHT on the edge\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,h1}, {KNIGHT, WHITE, a2} };
            SetPosition( &testing_board , initial_positions_w );
            std::vector< Move > expected = { {h1,g1}, {h1,g2}, {h1,h2},
            {a2,b4}, {a2,c3}, {a2,c1}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );

            initial_positions_w[1].position = b8;
            SetPosition( &testing_board , initial_positions_w );
            expected = { {h1,g1}, {h1,g2}, {h1,h2},
            {b8,a6}, {b8,c6}, {b8,d7}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );

            initial_positions_w[1].position = h7;
            SetPosition( &testing_board , initial_positions_w );
            expected = { {h1,g1}, {h1,g2}, {h1,h2},
            {h7,f8}, {h7,f6}, {h7,g5}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );

            initial_positions_w[1].position = g1;
            SetPosition( &testing_board , initial_positions_w );
            expected = { /*{h1,g1},*/ {h1,g2}, {h1,h2},
            {g1,e2}, {g1,f3}, {g1,h3}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
}

void TestBishopMoves(void) {
    fprintf( stdout , "Testing BISHOP moves\n" );
        fprintf( stdout , "\tBISHOP alone\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,a1}, {BISHOP, WHITE, e4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {a1,b1}, {a1,b2}, {a1,a2},
            {e4,d5}, {e4,c6}, {e4,b7}, {e4,a8}, 
            {e4,f5}, {e4,g6}, {e4,h7}, 
            {e4,f3}, {e4,g2}, {e4,h1},
            {e4,d3}, {e4,c2}, {e4,b1} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tBISHOP blocked\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,a1}, {BISHOP, WHITE, e4},
            {PAWN, WHITE, c6}, {PAWN, WHITE, g6}, {PAWN, WHITE, f3}, {PAWN, WHITE, c2}  };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {a1,b1}, {a1,b2}, {a1,a2},
            {e4,d5},  
            {e4,f5}, 
            {e4,d3},
            {c6,c7}, {g6,g7}, {f3,f4}, {c2,c3} } ;// PAWN moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tBISHOP taking\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,a1}, {BISHOP, WHITE, e4}};
            std::vector<PieceAndPosition> initial_positions_b{ {PAWN, BLACK, b7}, {PAWN, BLACK, g6}, {PAWN, BLACK, f3}, {PAWN, BLACK, c2}  };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected { /*{a1,b1},*/ {a1,b2}, {a1,a2},
            {e4, d5}, {e4, c6}, {e4, b7},
            {e4, f5}, {e4, g6},
            {e4, f3},
            {e4, d3}, {e4, c2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tBISHOP on the edge\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,a1}, {BISHOP, WHITE, a4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {a1,b1}, {a1,b2}, {a1,a2},
            {a4 , b5 }, {a4 , c6 }, {a4 , d7}, {a4, e8}, 
            {a4 , b3 }, {a4 , c2 }, {a4 , d1 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );

            initial_positions[1].position = c8;
            expected = { {a1,b1}, {a1,b2}, {a1,a2},
            {c8,c7}, {c8,c6}, 
            {c8,d7}, {c8,e6}, {c8,f5}, {c8,g4}, {c8,h3}};

            initial_positions[1].position = h7;
            expected = { {a1,b1}, {a1,b2}, {a1,a2},
            {h7,g8}, 
            {h7,g6}, {h7,f5}, {h7,e4}, {h7,d3}, {h7,c2}, {h7,b1}};

            initial_positions[1].position = h1;
            expected = { {a1,b1}, {a1,b2}, {a1,a2},
            {h7,g2}, {h7,f3}, {h7,e4}, {h7,d5}, {h7,c6}, {h7,b7}, {h7,a8}};
            fprintf( stdout , "\t\tpassed!\n");
        }
}

void TestRookMoves(void) {
    fprintf( stdout , "Testing ROOK moves\n" );
        fprintf( stdout , "\tROOK alone\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,h1}, {ROOK, WHITE, e4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {e4 , e5 }, {e4 , e6}, {e4 , e7}, {e4, e8}, 
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4}, {e4 , h4},
            {e4 , d4}, {e4 , c4}, {e4 , b4}, {e4, a4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }

        fprintf( stdout , "\tROOK blocked\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,h1}, {ROOK, WHITE, e4},
            {PAWN, WHITE, e6}, {PAWN, WHITE, e1}, {PAWN, WHITE, g4}, {PAWN, WHITE, d4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {e4 , e5 }, 
            {e4 , e3}, {e4 , e2}, 
            {e4 , f4},
            {e6, e7}, {e1, e2}, {g4, g5}, {d4, d5} };// PAWN moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }

        fprintf( stdout , "\tROOK taking\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,h1}, {ROOK, WHITE, e4} };
            std::vector<PieceAndPosition> initial_positions_b { {PAWN, BLACK, e6}, {PAWN, BLACK, e1}, 
            {PAWN, BLACK, g4}, {PAWN, BLACK, d4} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {e4 , e5 }, {e4 , e6},
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4},
            {e4 , d4}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tROOK on the edge\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,h1}, {ROOK, WHITE, a1} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {h1,g1}, {h1,g2}, {h1,h2},
            {a1,a2}, {a1,a3}, {a1,a4}, {a1,a5}, {a1,a6}, {a1,a7}, {a1,a8}, 
            {a1,b1}, {a1,c1}, {a1,d1}, {a1,e1}, {a1,f1}, {a1,g1} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );

            initial_positions[0].position = a1;
            initial_positions[0].position = h8;
            expected = { {a1,b1}, {a1,b2}, {a1,a2},
            {h8,h2}, {h8,h3}, {h8,h4}, {h8,h5}, {h8,h6}, {h8,h7}, {h8,h1}, 
            {h8,a8}, {h8,b8}, {h8,c8}, {h8,d8}, {h8,e8}, {h8,f8}, {h8,g8} };
            fprintf( stdout , "\t\tpassed!\n");
        }
}


void TestQueenMoves(void) {
    fprintf( stdout , "Testing QUEEN moves\n" );
        fprintf( stdout , "\tQUEEN alone\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,a1}, {QUEEN, WHITE, e4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {a1,b1}, {a1,b2}, {a1,a2},
            {e4 , d5 }, {e4 , c6 }, {e4 , b7}, {e4, a8}, 
            {e4 , f5 }, {e4 , g6 }, {e4 , h7 }, 
            {e4 , f3 }, {e4 , g2 }, {e4 , h1 },
            {e4 , d3 }, {e4 , c2 }, {e4 , b1 },
            {e4 , e5 }, {e4 , e6}, {e4 , e7}, {e4, e8}, 
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4}, {e4 , h4},
            {e4 , d4}, {e4 , c4}, {e4 , b4}, {e4, a4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tQUEEN blocked\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,a1}, {QUEEN, WHITE, e4}, 
            {PAWN, WHITE, d5}, {PAWN, WHITE, g6}, {PAWN, WHITE, f3}, 
            {PAWN, WHITE, c2}, {PAWN, WHITE, e6}, {PAWN, WHITE, e1}, 
            {PAWN, WHITE, g4}, {PAWN, WHITE, d4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {a1,b1}, {a1,b2}, {a1,a2},
            {e4 , f5}, 
            {e4 , d3},
            {e4 , e5 }, 
            {e4 , e3}, {e4 , e2}, 
            {e4 , f4},
            {d5, d6}, {g6, g7}, {f3, f4}, {c2, c3}, // PAWN moves
            {e6, e7}, {e1, e2}, {g4, g5} };// PAWN moves
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tQUEEN taking\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,a1}, {QUEEN, WHITE, e4}};
            std::vector<PieceAndPosition> initial_positions_b { {PAWN, BLACK, b7}, {PAWN, BLACK, g6}, 
            {PAWN, BLACK, f3}, {PAWN, BLACK, c2}, {PAWN, BLACK, e6}, 
            {PAWN, BLACK, e1}, {PAWN, BLACK, g4}, {PAWN, BLACK, d4} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected { /*{a1,b1},*/ {a1,b2}, {a1,a2},
            {e4, d5}, {e4, c6}, {e4, b7},
            {e4, f5}, {e4, g6},
            {e4, f3},
            {e4, d3}, {e4, c2},
            {e4 , e5 }, {e4 , e6},
            {e4 , e3}, {e4 , e2}, {e4 , e1}, 
            {e4 , f4}, {e4 , g4},
            {e4 , d4} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tQUEEN on the edge\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING,WHITE,h2}, {QUEEN, WHITE, a1} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {h2,h1}, {h2,g1}, {h2,g2}, {h2, g3}, {h2,h3},
            {a1,b2}, {a1,c3}, {a1,d4}, {a1,e5}, {a1,f6}, {a1,g7}, {a1,h8},
            {a1,a2}, {a1,a3}, {a1,a4}, {a1,a5}, {a1,a6}, {a1,a7}, {a1,a8},
            {a1,b1}, {a1,c1}, {a1,d1}, {a1,e1}, {a1,f1}, {a1,g1}, {a1,h1} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );

            initial_positions = { {KING,WHITE,a2}, {QUEEN, WHITE, h8} };
            SetPosition( &testing_board , initial_positions );
            expected = { {a2,a1}, {a2,b1}, {a2,b2}, {a2,b3}, {a2,a3},
            {h8,g8}, {h8,f8}, {h8,e8}, {h8,d8}, {h8,c8}, {h8,b8}, {h8,a8},
            {h8,g7}, {h8,f6}, {h8,e5}, {h8,d4}, {h8,c3}, {h8,b2}, {h8,a1},
            {h8,h7}, {h8,h6}, {h8,h5}, {h8,h4}, {h8,h3}, {h8,h2}, {h8,h1}};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
}

void TestKingMoves(void) {
    fprintf( stdout , "Testing KING moves\n" );
        fprintf( stdout , "\tKING alone\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING, WHITE, e4} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {e4 , d4}, 
            {e4 , d5 }, 
            {e4 , e5 },
            {e4 , e3},
            {e4 , d3 },
            {e4 , f3 },
            {e4 , f4},
            {e4 , f5 } };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tKING blocked\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING, WHITE, e4}, {PAWN, WHITE, d4}, {PAWN, WHITE, d5}, {PAWN, WHITE, e5}, {PAWN, WHITE, e3}, {PAWN, WHITE, d3}, {PAWN, WHITE, f3}, {PAWN, WHITE, f4}, {PAWN, WHITE, f5} };
            SetPosition( &testing_board , initial_positions );
            std::vector< Move > expected { {d5, d6},
            {e5, e6}, 
            {f5, f6} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tKING taking\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, e4} };
            std::vector<PieceAndPosition> initial_positions_b { {PAWN, BLACK, d3}, {PAWN, BLACK, d4}, {PAWN, BLACK, d5}, {PAWN, BLACK, e5}, {PAWN, BLACK, e3}, {PAWN, BLACK, f3}, {PAWN, BLACK, f4}, {PAWN, BLACK, f5} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector< Move > expected { {e4, d5},
            {e4 , e5 },
            {e4 , d3 },
            {e4 , f3 },
            {e4 , f5 }};
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
            fprintf( stdout , "\t\tpassed!\n");
        }
        fprintf( stdout , "\tKING on the edge\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions { {KING, WHITE, a4} };
            SetPosition( &testing_board , initial_positions );
            std::vector<Move> tested;
            std::vector< Move > expected { {a4, b4},
            {a4, a5},
            {a4, b5}, 
            {a4, a3},
            {a4, b3} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );

            initial_positions = { {KING, WHITE, e1} };
            SetPosition( &testing_board , initial_positions );
            expected = { {e1, d1}, {e1, d2}, {e1, e2}, {e1, f1}, {e1, f2} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );

            initial_positions = { {KING, WHITE, h4} };
            SetPosition( &testing_board , initial_positions );
            expected = { {h4, g4}, {h4, g5}, {h4, h5}, {h4, h3}, {h4, g3} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions ) );

            initial_positions = { {KING, WHITE, e8} };
            SetPosition( &testing_board , initial_positions );
            expected = { {e8, d8}, {e8, e7}, {e8, d7}, {e8, f7}, {e8, f8} };
            tested = AllPossibleMoves( &testing_board , initial_positions );
            ASSERT_EQUAL( expected , tested );
            fprintf( stdout , "\t\tpassed!\n");
        }

        fprintf( stdout , "\tKING threatened\n" ); {
            Board testing_board;
            std::vector<PieceAndPosition> initial_positions_w = { {KING, WHITE, e5} };
            std::vector<PieceAndPosition> initial_positions_b = { {KING, BLACK, a1},
            {ROOK, BLACK, a4}, {BISHOP, BLACK, b1}, {PAWN, BLACK, c7}, {KNIGHT, BLACK, g7} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            std::vector<Move> expected = { {e5,d5}, {e5, f6} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board, initial_positions_w ) );
        
            initial_positions_w = { {KING, WHITE, d7} };
            initial_positions_b = { {KNIGHT, BLACK, a8} };
            SetPosition( &testing_board , initial_positions_w, initial_positions_b );
            expected = { {d7, c6}, {d7, d6}, {d7, e6},
            {d7, e7},
            {d7, c8}, {d7, d8}, {d7, e8} };
            ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board, initial_positions_w ) );

            fprintf( stdout , "\t\tpassed!\n"); 
        }
}

void TestPinned(void) {
    fprintf( stdout , "Testing pinned pieces\n" ); 
        fprintf(stdout, "\tPAWN\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e5}, {STARTING_PAWN,WHITE,e2},
        {PAWN,WHITE,c5}, {PAWN,WHITE,f5}, {PAWN,WHITE,e6} };
        std::vector<PieceAndPosition> initial_positions_b { {ROOK, BLACK, e1},{ROOK, BLACK, a5},
        {QUEEN, BLACK, h5},{QUEEN, BLACK, e8},
        {PAWN,BLACK,d3}, {PAWN,BLACK,b6}, {PAWN,BLACK,g6}, {PAWN,BLACK,f7} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5},
        {e5,d6}, {e5,f6},
        {e2,e3, FIRST_MOVE}, {e2,e4, FIRST_MOVE}, 
        {e6,e7} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tlines passed!\n");
        
        initial_positions_w = { {KING, WHITE, e5}, {STARTING_PAWN, WHITE, b2},
        {PAWN,WHITE,g3}, {PAWN,WHITE,c7}, {PAWN,WHITE,f6} };
        initial_positions_b = { {BISHOP,BLACK,a1}, {BISHOP,BLACK,h2},
        {QUEEN,BLACK,b8}, {QUEEN,BLACK,h8},
        {PAWN,BLACK,c3}, {PAWN,BLACK,d8}, {PAWN,BLACK,e7}, {PAWN,BLACK,h4} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = {  {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5}, {e5,f5},
        {e5,e6},
        {b2,b3}, {b2,b4}, {b2,c3},
        {c7,b8,PROMOTION_QUEEN}, {c7,b8,PROMOTION_KNIGHT} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tdiagonals passed!\n");
        }
        fprintf(stdout, "\tKNIGHT\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e5},
        {KNIGHT,WHITE,e2}, {KNIGHT,WHITE,c5}, {KNIGHT,WHITE,f5}, {KNIGHT,WHITE,e6} };
        std::vector<PieceAndPosition> initial_positions_b { {ROOK, BLACK, e1},{ROOK, BLACK, a5},
        {QUEEN, BLACK, h5},{QUEEN, BLACK, e8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5},
        {e5,d6}, {e5,f6} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tlines passed!\n");
        
        initial_positions_w = { {KING, WHITE, e5},
        {KNIGHT,WHITE,b2}, {KNIGHT,WHITE,g3}, {KNIGHT,WHITE,c7}, {KNIGHT,WHITE,f6} };
        initial_positions_b = { {BISHOP,BLACK,a1}, {BISHOP,BLACK,h2},
        {QUEEN,BLACK,b8}, {QUEEN,BLACK,h8}};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = {  {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5}, {e5,f5},
        {e5,e6}, {e5,d6} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tdiagonals passed!\n");
        }
        fprintf(stdout, "\tBISHOP\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e5},
        {BISHOP,WHITE,e2}, {BISHOP,WHITE,c5}, {BISHOP,WHITE,f5}, {BISHOP,WHITE,e6} };
        std::vector<PieceAndPosition> initial_positions_b { {ROOK, BLACK, e1},{ROOK, BLACK, a5},
        {QUEEN, BLACK, h5},{QUEEN, BLACK, e8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5},
        {e5,d6}, {e5,f6} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tlines passed!\n");
        
        initial_positions_w = { {KING, WHITE, e5},
        {BISHOP,WHITE,b2}, {BISHOP,WHITE,g3}, {BISHOP,WHITE,c7}, {BISHOP,WHITE,f6} };
        initial_positions_b = { {BISHOP,BLACK,a1}, {BISHOP,BLACK,h2},
        {QUEEN,BLACK,b8}, {QUEEN,BLACK,h8}};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = {  {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5}, {e5,f5},
        {e5,e6}, {e5,d6},
        {b2,a1}, {b2,c3}, {b2,d4}, 
        {g3,h2}, {g3,f4}, 
        {f6,g7}, {f6,h8}, 
        {c7,b8}, {c7,d6} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tdiagonals passed!\n");
        }
        fprintf(stdout, "\tROOK\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e5},
        {ROOK,WHITE,e2}, {ROOK,WHITE,c5}, {ROOK,WHITE,f5}, {ROOK,WHITE,e6} };
        std::vector<PieceAndPosition> initial_positions_b { {ROOK, BLACK, e1},{ROOK, BLACK, a5},
        {QUEEN, BLACK, h5},{QUEEN, BLACK, e8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5},
        {e5,d6}, {e5,f6},
        {e2,e1}, {e2,e3}, {e2,e4},
        {c5,a5}, {c5,b5}, {c5,d5},
        {f5,h5}, {f5,g5},
        {e6,e8}, {e6,e7} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tlines passed!\n");
        
        initial_positions_w = { {KING, WHITE, e5},
        {ROOK,WHITE,b2}, {ROOK,WHITE,g3}, {ROOK,WHITE,c7}, {ROOK,WHITE,f6} };
        initial_positions_b = { {BISHOP,BLACK,a1}, {BISHOP,BLACK,h2},
        {QUEEN,BLACK,b8}, {QUEEN,BLACK,h8}};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = {  {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5}, {e5,f5},
        {e5,e6}, {e5,d6} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tdiagonals passed!\n");
        }
        fprintf(stdout, "\tQUEEN\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e5},
        {QUEEN,WHITE,e2}, {QUEEN,WHITE,c5}, {QUEEN,WHITE,f5}, {QUEEN,WHITE,e6} };
        std::vector<PieceAndPosition> initial_positions_b { {ROOK, BLACK, e1},{ROOK, BLACK, a5},
        {QUEEN, BLACK, h5},{QUEEN, BLACK, e8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5},
        {e5,d6}, {e5,f6},
        {e2,e1}, {e2,e3}, {e2,e4},
        {c5,a5}, {c5,b5}, {c5,d5},
        {f5,h5}, {f5,g5},
        {e6,e8}, {e6,e7} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tlines passed!\n");
        
        initial_positions_w = { {KING, WHITE, e5},
        {QUEEN,WHITE,b2}, {QUEEN,WHITE,g3}, {QUEEN,WHITE,c7}, {QUEEN,WHITE,f6} };
        initial_positions_b = { {BISHOP,BLACK,a1}, {BISHOP,BLACK,h2},
        {QUEEN,BLACK,b8}, {QUEEN,BLACK,h8}};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = {  {e5,e4}, {e5,d4}, {e5,f4},
        {e5,d5}, {e5,f5},
        {e5,e6}, {e5,d6},
        {b2,a1}, {b2,c3}, {b2,d4}, 
        {g3,h2}, {g3,f4}, 
        {f6,g7}, {f6,h8}, 
        {c7,b8}, {c7,d6} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board , initial_positions_w ) );
        fprintf(stdout, "\t\tdiagonals passed!\n");
        }
        fprintf(stdout, "\tEn passant\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e3}, {PAWN,WHITE,e5},
        {PAWN,WHITE,c5} };
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, h8}, 
        {ROOK, BLACK, e8}, {BISHOP, BLACK, a7},
        {PAWN,BLACK,d5} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e3,d4}, {e3,f4},
        {e3,d3}, {e3,f3},
        {e3,d2}, {e3,e2}, {e3,f2}, 
        {e5,e6} };
        Move last_move;
        last_move.beg = d7;
        last_move.end = d5;
        last_move.type = FIRST_MOVE;
        ufast8 result_check;
        std::vector< Move > tested = Moves( &testing_board , initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected , tested );
        fprintf(stdout, "\t\tEn passant passed!\n");
        }
}

void TestCheckCheck(void) {
    fprintf( stdout , "Testing check\n" ); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w;
        std::vector<PieceAndPosition> initial_positions_b;
        Move last_move;
        int expected;
//  TODO : blocked checks
        initial_positions_w = { {KING, WHITE, e5} };
        initial_positions_b = { {ROOK, BLACK, a4} };
        last_move.beg = a8;
        last_move.end = a4;
        fprintf( stdout , "\tNo check\n");
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 255 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
        fprintf( stdout , "\t\tpassed!\n"); 

// diagonals check
    // UP RIGHT
        initial_positions_w = { {KING, WHITE, e8}, {QUEEN, WHITE, a1} };
        initial_positions_b = { {KING, BLACK, h8}, {QUEEN, BLACK, h2} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = a3;
        last_move.end = a1;
        expected = a1;
        ASSERT_EQUAL( expected, (int) CheckCheck(&testing_board , last_move , initial_positions_b[0] ) );
exit(EXIT_FAILURE);
    // UP LEFT
        initial_positions_w = { {KING, WHITE, e1}, {QUEEN, WHITE, h1} };
        initial_positions_b = { {KING, BLACK, a8}, {QUEEN, BLACK, a3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = h4;
        last_move.end = h1;
        expected = h1;
        ASSERT_EQUAL( expected, (int) CheckCheck(&testing_board , last_move , initial_positions_b[0] ) );

// lines check
    // UP
        fprintf( stdout , "\tLines+Diagonals check\n");
        initial_positions_w = { {KING, WHITE, e5} };
        initial_positions_b = { {ROOK, BLACK, e7}, {BISHOP, BLACK, b8} };
        last_move.beg = c7;
        last_move.end = e7;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // DOWN
        initial_positions_b = { {ROOK, BLACK, e3}, {QUEEN, BLACK, h2} };
        last_move.beg = g3;
        last_move.end = e3;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // LEFT/DOWN LEFT
        initial_positions_b = { {ROOK, BLACK, b5}, {BISHOP, BLACK, a1} };
        last_move.beg = b2;
        last_move.end = b5;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // RIGHT/ UP RIGHT
        initial_positions_b = { {ROOK, BLACK, g5}, {QUEEN, BLACK, h8} };
        last_move.beg = g7;
        last_move.end = g5;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
        fprintf( stdout , "\t\tpassed!\n"); 
// lines check
    // UP/UP LEFT
        fprintf( stdout , "\tDiagonals+Lines check\n");
        initial_positions_w = { {KING, WHITE, e5} };
        initial_positions_b = { {ROOK, BLACK, e8}, {BISHOP, BLACK, d6} };
        last_move.beg = e7;
        last_move.end = d6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // DOWN/DOWN RIGHT
        initial_positions_b = { {QUEEN, BLACK, e1}, {BISHOP, BLACK, f4} };
        last_move.beg = e3;
        last_move.end = f4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // LEFT/DOWN LEFT
        initial_positions_b = { {ROOK, BLACK, a5}, {BISHOP, BLACK, d4} };
        last_move.beg = c5;
        last_move.end = d4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // RIGHT/ UP RIGHT
        initial_positions_b = { {QUEEN, BLACK, h5}, {BISHOP, BLACK, f6} };
        last_move.beg = g5;
        last_move.end = f6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
        fprintf( stdout , "\t\tpassed!\n");
// KNIGHT check
        fprintf( stdout , "\tKNIGHT check\n");
        initial_positions_w = { {KING, WHITE, e5} };
        initial_positions_b = { {KNIGHT, BLACK, c4} };
        last_move.beg = d2;
        last_move.end = c4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 126 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

//  KNIGHT + Lines
    // UP
        initial_positions_b = { {KNIGHT, BLACK, g6}, {ROOK, BLACK, e8} };
        last_move.beg = e7;
        last_move.end = g6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // DOWN
        initial_positions_b = { {KNIGHT, BLACK, c4}, {QUEEN, BLACK, e1}  };
        last_move.beg = e3;
        last_move.end = c4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // LEFT
        initial_positions_b = { {KNIGHT, BLACK, d7}, {ROOK, BLACK, a5} };
        last_move.beg = c5;
        last_move.end = d7;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // RIGHT
        initial_positions_b = { {KNIGHT, BLACK, f3}, {QUEEN, BLACK, h5} };
        last_move.beg = g5;
        last_move.end = f3;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

//  KNIGHT + Diagonals
    // UP LEFT
        initial_positions_b = { {KNIGHT, BLACK, f7}, {BISHOP, BLACK, b8} };
        last_move.beg = d6;
        last_move.end = f7;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // DOWN RIGHT
        initial_positions_b = { {KNIGHT, BLACK, d3}, {QUEEN, BLACK, h2}  };
        last_move.beg = f4;
        last_move.end = d3;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // UP RIGHT
        initial_positions_b = { {KNIGHT, BLACK, g4}, {BISHOP, BLACK, h8} };
        last_move.beg = f6;
        last_move.end = g4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
    // DOWN LEFT
        initial_positions_b = { {KNIGHT, BLACK, c4}, {QUEEN, BLACK, a1} };
        last_move.beg = b2;
        last_move.end = c4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        fprintf( stdout , "\t\tpassed!\n"); 

//PAWN check
        fprintf( stdout , "\tPAWN check\n");
        fprintf( stdout, "\t\tWHITE\n"); {
        initial_positions_w = { {KING, WHITE, e4} };
        initial_positions_b = { {PAWN, BLACK, f5} };
        last_move.beg = f7;
        last_move.end = f5;
        last_move.type = FIRST_MOVE;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) f5 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );
        last_move.type = NORMAL_MOVE;

        initial_positions_w = { {KING, WHITE, e5} };
        initial_positions_b = { {PAWN, BLACK, d6}, {ROOK, BLACK, e8} };
        last_move.beg = e7;
        last_move.end = d6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        initial_positions_b = { {PAWN, BLACK, b6}, {BISHOP, BLACK, b8} };
        last_move.beg = c7;
        last_move.end = b6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) b8 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        initial_positions_b = { {PAWN, BLACK, f6}, {QUEEN, BLACK, h8} };
        last_move.beg = g7;
        last_move.end = f6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) f6 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        initial_positions_b = { {PAWN, BLACK, d4}, {QUEEN, BLACK, a5} };
        last_move.beg = d5;
        last_move.end = d4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) a5 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        fprintf( stdout, "\t\t\tpassed\n");
        }
        fprintf( stdout, "\t\tBLACK\n"); {
        initial_positions_w = { {PAWN, WHITE, d4}, {ROOK, WHITE, e1} };
        initial_positions_b = { {KING, BLACK, e5} };
        last_move.beg = e3;
        last_move.end = d4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_b[0] ) );

        initial_positions_w = { {PAWN, WHITE, d4}, {BISHOP, WHITE, a1} };
        last_move.beg = c3;
        last_move.end = d4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) d4 , (int) CheckCheck(& testing_board , last_move , initial_positions_b[0] ) );

        initial_positions_w = { {PAWN, WHITE, g4}, {QUEEN, WHITE, h2} };
        last_move.beg = g3;
        last_move.end = g4;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) h2 , (int) CheckCheck(& testing_board , last_move , initial_positions_b[0] ) );

        initial_positions_w = { {PAWN, WHITE, g6}, {QUEEN, WHITE, h5} };
        last_move.beg = g5;
        last_move.end = g6;
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) h5 , (int) CheckCheck(& testing_board , last_move , initial_positions_b[0] ) );

        fprintf( stdout, "\t\t\tpassed\n");
        }
// Promotion
        fprintf( stdout , "\tPromotion check\t\t\t");
        initial_positions_w = { {KING, WHITE, e1} };
        initial_positions_b = { {QUEEN, BLACK, c1}, {BISHOP, BLACK, d8} };
        last_move = {c2, c1, PROMOTION_QUEEN};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) c1 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        initial_positions_b = { {QUEEN, BLACK, d1}, {ROOK, BLACK, e8} };
        last_move = {e2, d1, PROMOTION_QUEEN};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        initial_positions_w = { {KING, WHITE, e2} };
        initial_positions_b = { {QUEEN, BLACK, f1}, {QUEEN, BLACK, h2} };
        last_move = {f2, f1, PROMOTION_QUEEN};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        initial_positions_w = { {KING, WHITE, e2} };
        initial_positions_b = { {KNIGHT, BLACK, g1}, {QUEEN, BLACK, h2} };
        last_move = {g2, g1, PROMOTION_KNIGHT};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( 200 , (int) CheckCheck(& testing_board , last_move , initial_positions_w[0] ) );

        fprintf( stdout , "passed!\n");
// En passant
        fprintf( stdout , "\tEn passant check\t\t");
        initial_positions_w = { {KING, WHITE, e1}, {PAWN, WHITE, c6}};
        initial_positions_b = { {KING, BLACK, d7} };
        last_move = {d5, c6, EN_PASSANT};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        ASSERT_EQUAL( (int) c6 , (int) CheckCheck(& testing_board , last_move , initial_positions_b[0] ) );

        fprintf( stdout , "passed!\n"); 
        fprintf( stdout , "Testing check\t\t\t\tpassed!\n");  
    }
}

void TestCheck(void) {
    fprintf( stdout , "Testing checked\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w;
        std::vector<PieceAndPosition> initial_positions_b;
        Move last_move;
        last_move.type = NORMAL_MOVE;
        ufast8 result_check;
        std::vector< Move > expected;
        std::vector< Move > tested;

//  Line checks
        initial_positions_w = { {KING, WHITE, e5}, {PAWN, WHITE, d6} };
        initial_positions_b = { {ROOK, BLACK, e7} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = a7;
        last_move.end = e7;
        expected = { {e5, d4}, {e5, f4},
        {e5, d5}, {e5, f5},
        {e5, f6},
        {d6, e7} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e3}, {STARTING_PAWN, WHITE, d2} };
        initial_positions_b = { {ROOK, BLACK, c3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c1;
        last_move.end = c3;
        expected = { {e3, e2}, {e3, f2},
        {e3, d4}, {e3, e4}, {e3, f4},
        {d2, d3, FIRST_MOVE}, {d2, c3, FIRST_MOVE} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e4}, {STARTING_PAWN, WHITE, d2} };
        initial_positions_b = { {ROOK, BLACK, c4} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c1;
        last_move.end = c4;
        expected = { {e4, d3}, {e4, e3}, {e4, f3},
        {e4, d5}, {e4, e5}, {e4, f5},
        {d2, d4, FIRST_MOVE} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e5}, {KNIGHT, WHITE, c3} };
        initial_positions_b = { {ROOK, BLACK, e1} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = a1;
        last_move.end = e1;
        expected = { {e5, d4}, {e5, f4},
        {e5, d5}, {e5, f5},
        {e5, f6}, {e5, d6},
        {c3, e2}, {c3, e4} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e5}, {BISHOP, WHITE, c6} };
        initial_positions_b = { {ROOK, BLACK, c5} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c1;
        last_move.end = c5;
        expected = { {e5, d4}, {e5, f4}, {e5, e4},
        {e5, f6}, {e5, d6}, {e5, e6},
        {c6, d5} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e5}, {ROOK, WHITE, h8} };
        initial_positions_b = { {ROOK, BLACK, e8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = a8;
        last_move.end = e8;
        expected = { {e5, d4}, {e5, f4},
        {e5, d5}, {e5, f5},
        {e5, f6}, {e5, d6},
        {h8, e8} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

//  Diagonals checks
        initial_positions_w = { {KING, WHITE, e5}, {PAWN, WHITE, d5} };
        initial_positions_b = { {BISHOP, BLACK, b8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = a7;
        last_move.end = b8;
        expected = { {e5, d4}, {e5, e4},
        {e5, f5},
        {e5, f6}, {e5, e6},
        {d5, d6} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e5}, {KNIGHT, WHITE, f1} };
        initial_positions_b = { {BISHOP, BLACK, h2} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = g1;
        last_move.end = h2;
        expected = { {e5, d4}, {e5, e4},
        {e5, f5}, {e5, d5},
        {e5, f6}, {e5, e6},
        {f1, h2}, {f1, g3} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e5}, {BISHOP, WHITE, h5} };
        initial_positions_b = { {BISHOP, BLACK, g7} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = h6;
        last_move.end = g7;
        expected = { {e5, f4}, {e5, e4},
        {e5, f5}, {e5, d5},
        {e5, d6}, {e5, e6}};
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

        initial_positions_w = { {KING, WHITE, e5}, {QUEEN, WHITE, b1} };
        initial_positions_b = { {BISHOP, BLACK, b2} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c1;
        last_move.end = b2;
        expected = { {e5, f4}, {e5, e4},
        {e5, f5}, {e5, d5},
        {e5, d6}, {e5, e6},
        {b1, b2} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );

//  Reveal checks
    // Lines
        initial_positions_w = { {KING, WHITE, e5}, {QUEEN, WHITE, d7} };
        initial_positions_b = { {ROOK, BLACK, e7}, {KNIGHT, BLACK, g7} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = e6;
        last_move.end = g7;
        expected = { {e5, f4}, {e5, d4},
        {e5, d5},
        {e5, d6}, {e5, f6},
        {d7, e7}, {d7, e6} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

        initial_positions_w = { {KING, WHITE, e5}, {QUEEN, WHITE, c4} };
        initial_positions_b = { {QUEEN, BLACK, e2}, {BISHOP, BLACK, f3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = e4;
        last_move.end = f3;
        expected = { {e5, f4}, {e5, d4},
        {e5, f5},
        {e5, d6}, {e5, f6},
        {c4, e4}, {c4, e2} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

        initial_positions_w = { {KING, WHITE, e5}, {PAWN, WHITE, b4} };
        initial_positions_b = { {ROOK, BLACK, a5}, {PAWN, BLACK, d4} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = d5;
        last_move.end = d4;
        expected = { {e5, f4}, {e5, d4}, {e5, e4},
        {e5, d6}, {e5, e6}, {e5, f6},
        {b4, a5}, {b4, b5} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

    // Diagonals
        initial_positions_w = { {KING, WHITE, e5}, {ROOK, WHITE, b6} };
        initial_positions_b = { {BISHOP, BLACK, b8}, {KNIGHT, BLACK, e6} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c7;
        last_move.end = e6;
        expected = { {e5, e4},
        {e5, d5}, {e5, f5},
        {e5, e6}, {e5, f6},
        {b6, b8}, {b6, d6} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

        initial_positions_w = { {KING, WHITE, e5}, {BISHOP, WHITE, a1}, {BISHOP, WHITE, f6} };
        initial_positions_b = { {QUEEN, BLACK, b2}, {KNIGHT, BLACK, b3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = d4;
        last_move.end = b3;
        expected = { {e5, e4}, {e5, f4},
        {e5, d5}, {e5, f5},
        {e5, e6}, {e5, d6},
        {a1, b2} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");
    
    // Double check + Knight check
        initial_positions_w = { {KING, WHITE, e5}, {ROOK, WHITE, e6}, {KNIGHT, WHITE, d6} };
        initial_positions_b = { {KNIGHT, BLACK, c4} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = b2;
        last_move.end = c4;
        expected = { {e5, e4}, {e5, d4}, {e5, f4},
        {e5, d5}, {e5, f5},
        {e5, f6},
        {d6, c4} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

        initial_positions_w = { {KING, WHITE, e5}, {ROOK, WHITE, a2}, {KNIGHT, WHITE, d2} };
        initial_positions_b = { {QUEEN, BLACK, a5}, {BISHOP, BLACK, d6} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c5;
        last_move.end = d6;
        expected = { {e5, e4}, {e5, d4},
        {e5, d6}, {e5, e6}, {e5, f6} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");
    
    // En passant check
        initial_positions_w = { {KING_CAN_CASTLE, WHITE, e1}, {ROOK_CAN_CASTLE, WHITE, a1} };
        initial_positions_b = { {KING, BLACK, e5}, {QUEEN, BLACK, a5}, {PAWN, BLACK, b3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = c4;
        last_move.end = b3;
        last_move.type = EN_PASSANT;
        expected = { {e1, d1}, {e1, f1}, {e1, e2},
        {e1, f2},
        {a1, a5} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

        initial_positions_w = { {KING, WHITE, d2}, {KNIGHT, WHITE, c4} };
        initial_positions_b = { {KING, BLACK, e8}, {BISHOP, BLACK, h6}, {PAWN, BLACK, e3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = f4;
        last_move.end = e3;
        last_move.type = EN_PASSANT;
        expected = { {d2, d3}, {d2, c3},
        {d2, c2}, {d2, e2}, 
        {d2, c1}, {d2, d1}, {d2, e1},
        {c4, e3} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");

        initial_positions_w = { {KING, WHITE, d2}, {KNIGHT, WHITE, e4} };
        initial_positions_b = { {KING, BLACK, e8}, {ROOK, BLACK, d8}, {PAWN, BLACK, c3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        last_move.beg = d4;
        last_move.end = c3;
        last_move.type = EN_PASSANT;
        expected = { {d2, c3}, {d2, e3},
        {d2, c2}, {d2, e2}, 
        {d2, c1}, {d2, e1} };
        tested = Moves(&testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested );
        fprintf( stdout , "\tpassed!\n");
    }
}

void TestCastleMoves(void) {
    fprintf(stdout , "Testing castling moves\n");
    fprintf(stdout , "\tFirst KING/ROOK moves WHITE \n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, e1}, {ROOK_CAN_CASTLE, WHITE, a1},
        {ROOK_CAN_CASTLE, WHITE, h1} };
        std::vector<PieceAndPosition> initial_positions_b { {KING,BLACK,e8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector<Move> expected = { {e1, d1, FIRST_MOVE}, {e1, d2, FIRST_MOVE}, {e1, e2, FIRST_MOVE}, {e1, f1,FIRST_MOVE}, {e1, f2,FIRST_MOVE}, 
        {a1, b1, FIRST_MOVE}, {a1, c1, FIRST_MOVE}, {a1, d1, FIRST_MOVE},
        {a1, a2, FIRST_MOVE}, {a1, a3, FIRST_MOVE}, {a1, a4, FIRST_MOVE},
        {a1, a5, FIRST_MOVE}, {a1, a6, FIRST_MOVE}, {a1, a7, FIRST_MOVE},
        {a1, a8, FIRST_MOVE},
        {h1, g1, FIRST_MOVE}, {h1, f1, FIRST_MOVE},
        {h1, h2, FIRST_MOVE}, {h1, h3, FIRST_MOVE}, {h1, h4, FIRST_MOVE},
        {h1, h5, FIRST_MOVE}, {h1, h6, FIRST_MOVE}, {h1, h7, FIRST_MOVE},
        {h1, h8, FIRST_MOVE}};
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board, initial_positions_w ) );

        
        initial_positions_w = { {KING_CAN_CASTLE, WHITE, e1}, {ROOK_CAN_CASTLE, WHITE, a1},
        {ROOK_CAN_CASTLE, WHITE, h1} };
        initial_positions_b = { {KING,BLACK,e8}, {ROOK, BLACK, a2}, {ROOK, BLACK, h2} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = { {e1, d1, FIRST_MOVE}, {a1, d1, CASTLE},
        {e1, f1,FIRST_MOVE}, {h1, f1, CASTLE},
        {a1, b1, FIRST_MOVE}, {a1, c1, FIRST_MOVE}, {a1, d1, FIRST_MOVE},
        {h1, g1, FIRST_MOVE}, {h1, f1, FIRST_MOVE},
        {a1, a2, FIRST_MOVE}, {h1, h2, FIRST_MOVE} };
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board, initial_positions_w ) );
        fprintf( stdout , "\t\tpassed!\n"); 
    }
    fprintf(stdout , "\tFirst ROOK moves BLACK \n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, e1} };
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, e8}, {ROOK_CAN_CASTLE, BLACK, a8}, 
        {ROOK_CAN_CASTLE, BLACK, h8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector<Move> expected = { {e8, f8, FIRST_MOVE}, {e8, f7, FIRST_MOVE}, {e8, e7, FIRST_MOVE}, {e8, d7,FIRST_MOVE}, {e8, d8,FIRST_MOVE}, 
        {a8, b8, FIRST_MOVE}, {a8, c8, FIRST_MOVE}, {a8, d8, FIRST_MOVE},
        {a8, a7, FIRST_MOVE}, {a8, a6, FIRST_MOVE}, {a8, a5, FIRST_MOVE},
        {a8, a4, FIRST_MOVE}, {a8, a3, FIRST_MOVE}, {a8, a2, FIRST_MOVE},
        {a8, a1, FIRST_MOVE},
        {h8, g8, FIRST_MOVE}, {h8, f8, FIRST_MOVE},
        {h8, h7, FIRST_MOVE}, {h8, h6, FIRST_MOVE}, {h8, h5, FIRST_MOVE},
        {h8, h4, FIRST_MOVE}, {h8, h3, FIRST_MOVE}, {h8, h2, FIRST_MOVE},
        {h8, h1, FIRST_MOVE}};
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board, initial_positions_b ) );

        
        initial_positions_w = { {KING, WHITE, e1}, {ROOK, WHITE, a7}, {ROOK, WHITE, h7} };
        initial_positions_b = { {KING_CAN_CASTLE, BLACK, e8}, {ROOK_CAN_CASTLE, BLACK, a8},
        {ROOK_CAN_CASTLE, BLACK, h8} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        expected = { {e8, f8, FIRST_MOVE}, {a8, d8, CASTLE},
        {e8, d8,FIRST_MOVE}, {h8, f8,CASTLE},
        {a8, b8, FIRST_MOVE}, {a8, c8, FIRST_MOVE}, {a8, d8, FIRST_MOVE},
        {a8, a7, FIRST_MOVE},
        {h8, g8, FIRST_MOVE}, {h8, f8, FIRST_MOVE},
        {h8, h7, FIRST_MOVE}};
        ASSERT_EQUAL( expected , AllPossibleMoves( &testing_board, initial_positions_b ) );
        fprintf( stdout , "\t\tpassed!\n"); 
    }
}

void RandomTest(void) {
    fprintf(stdout , "Testing specific case\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING,WHITE,e3}, {PAWN,WHITE,e4}};
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, h8}, 
        {QUEEN, BLACK, e8}};
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        std::vector< Move > expected { {e3,d4}, {e3,f4},
        {e3,d3}, {e3,f3},
        {e3,d2}, {e3,e2}, {e3,f2}, 
        {e4,e5} };
        Move last_move;
        last_move.beg = a8;
        last_move.end = h8;
        last_move.type = NORMAL_MOVE;
        ufast8 result_check;
        std::vector< Move > tested = Moves( &testing_board , initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected , tested );
    }
    fprintf( stdout , "\tpassed!\n"); 
}

/*void ActualMovesFunction(void) {
    fprintf(stdout, "Testing the Moves function\n"); {
        Board testing_board;
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, e8}, {BISHOP, BLACK, b4}, {PAWN, BLACK, b5} };
        std::vector<PieceAndPosition> initial_positions_w { {KING_CAN_CASTLE, WHITE, f2}, {KNIGHT, WHITE, c3} };
        SetPosition( &testing_board , initial_positions_w, initial_positions_b );
        Move last_move = {f8, b4};
        ufast8 result_check;
        PrintBoard(testing_board);

        std::vector<Move> expected = {{d3, d1}, {f2,e2}, {f2,e1}, {f2,f1}, {f2,g1}};
        std::vector<Move> tested =  Moves( &testing_board, initial_positions_w, last_move, &result_check );
        ASSERT_EQUAL( expected, tested);

    }
    fprintf(stdout, "Testing the Moves function\t\t\tpassed!\n");
}*/

int main(void) {
	cute::ide_listener<> listener{};
//	cute::counting_listener<> listener{};

    cute::suite suite_of_tests;
    suite_of_tests.push_back( CUTE(TestPawnMoves));
    suite_of_tests.push_back( CUTE(TestKnightMoves));
    suite_of_tests.push_back( CUTE(TestBishopMoves));
    suite_of_tests.push_back( CUTE(TestRookMoves));
    suite_of_tests.push_back( CUTE(TestQueenMoves));
    suite_of_tests.push_back( CUTE(TestKingMoves));
    suite_of_tests.push_back( CUTE(TestPinned));
    suite_of_tests.push_back( CUTE(TestCheckCheck));
    suite_of_tests.push_back( CUTE(TestCheck));
    suite_of_tests.push_back( CUTE(TestCastleMoves));
    suite_of_tests.push_back( CUTE(RandomTest));
//    suite_of_tests.push_back( CUTE(ActualMovesFunction));

	cute::makeRunner(listener)(suite_of_tests,"All tests\n");

/*	std::cerr <<"\n"<< listener.failedTests << " Failed, Out Of "<< listener.numberOfTests << std::endl;
	std::cerr << listener.errors << " Errors - expect 0 errors" << std::endl;*/

    return 0;
}