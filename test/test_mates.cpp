#include "cute.h"
#include "cute_runner.h"
#include "ide_listener.h"
#include "cute_test.h"
#include "cute_equals.h"
#include "cute_suite.h"

#include "engine.hpp"

#include <chrono>

using namespace ChIAss::Chess;
using namespace ChIAss::Engine;

auto start = std::chrono::steady_clock::now();
auto end = std::chrono::steady_clock::now();


void Test(void) {
    fprintf( stdout , "Testing opening\n" );

    fprintf( stdout , "Not implemented yet\n" );
    fprintf( stdout , "Testing endings WHITE\n" );
    fprintf( stdout , "\tTesting mate in 1, WHITE\t\t\t"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, g3}, {PAWN, WHITE, h4}, {PAWN, WHITE, g2}, 
        {PAWN, WHITE, e3}, {PAWN, WHITE, a5}, {BISHOP, WHITE, c3}, {KNIGHT, WHITE, f4}, {ROOK, WHITE, c5} };
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, h6}, {PAWN, BLACK, h7}, {PAWN, BLACK, f5},
        {ROOK, BLACK, a2}, {ROOK, BLACK, f1}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move expected { c5 , c6, NORMAL_MOVE };
        Move last_move = {f6, f5, NORMAL_MOVE};

        start = std::chrono::steady_clock::now();
        Move tested = ChIAssMove( &empty_board, WHITE, 4, last_move );
        end = std::chrono::steady_clock::now();
        ASSERT_EQUAL( expected , tested );
        fprintf( stdout , "passed!");
		std::cout<< " in : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
    }
    fprintf( stdout , "\tTesting mate in 2, WHITE\t\t\t"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, b2}, {PAWN, WHITE, c2}, {PAWN, WHITE, b3}, 
        {PAWN, WHITE, e5}, {BISHOP, WHITE, d4}, {QUEEN, WHITE, f8}, {ROOK, WHITE, b4} };
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, c6}, {PAWN, BLACK, g7}, {PAWN, BLACK, d5},
        {PAWN, BLACK, c7}, {PAWN, BLACK, a6},
        {ROOK, BLACK, h2}, {QUEEN, BLACK, d3}, {BISHOP, BLACK, e6}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move expected { f8 , e8, NORMAL_MOVE };
        Move last_move = {d6, d5, NORMAL_MOVE};

        Move tested = ChIAssMove( &empty_board, WHITE, 4, last_move );
        ASSERT_EQUAL( expected , tested );
        

        UpdateBoard(&empty_board, expected);
        last_move = {e6,d7, NORMAL_MOVE};
        UpdateBoard(&empty_board, last_move);
        initial_positions_w = { {KING, WHITE, b2}, {PAWN, WHITE, c2}, {PAWN, WHITE, b3}, 
        {PAWN, WHITE, e5}, {BISHOP, WHITE, d4}, {QUEEN, WHITE, e8}, {ROOK, WHITE, b4} };
        initial_positions_b = { {KING, BLACK, c6}, {PAWN, BLACK, g7}, {PAWN, BLACK, d5},
        {PAWN, BLACK, c7}, {PAWN, BLACK, a6},
        {ROOK, BLACK, h2}, {QUEEN, BLACK, d3}, {BISHOP, BLACK, d7}};

        expected = {e8,a8, NORMAL_MOVE};
        start = std::chrono::steady_clock::now();
        tested = ChIAssMove( &empty_board, WHITE, 4, last_move );
        end = std::chrono::steady_clock::now();
        ASSERT_EQUAL( expected , tested );
        fprintf( stdout , "passed!");
		std::cout<< " in : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
    }
    fprintf( stdout , "\tTesting mate in 3, WHITE\t\t\t"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, b1}, {PAWN, WHITE, b2}, {PAWN, WHITE, c2}, 
        {PAWN, WHITE, d5}, {PAWN, WHITE, f3}, {PAWN, WHITE, g6}, {BISHOP, WHITE, f5}, {ROOK, WHITE, c1},
        {QUEEN, WHITE, f8} };
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, c7}, {PAWN, BLACK, a7}, {PAWN, BLACK, b4},
        {PAWN, BLACK, d6}, {PAWN, BLACK, e5}, {ROOK, BLACK, a2}, {ROOK, BLACK, b8}, {QUEEN, BLACK, a5}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move expected {f8 , e7, NORMAL_MOVE};
        Move last_move = {b5, b4, NORMAL_MOVE};

        start = std::chrono::steady_clock::now();
        Move tested = ChIAssMove( &empty_board, WHITE, 6, last_move );
        end = std::chrono::steady_clock::now();
        ASSERT_EQUAL( expected , tested );
        fprintf( stdout , "passed!");
		std::cout<< " in : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
    }
    fprintf( stdout , "\tTesting mate in 7, WHITE\t\t\t"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, d2}, {PAWN, WHITE, e2}, {KNIGHT, WHITE, c1} };
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, a1}, {PAWN, BLACK, a5}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move expected {d2 , c2, NORMAL_MOVE};
        Move last_move = {a6, a5, NORMAL_MOVE};

        start = std::chrono::steady_clock::now();
        Move tested = ChIAssMove( &empty_board, WHITE, 8, last_move );
        end = std::chrono::steady_clock::now();
        ASSERT_EQUAL( expected , tested );
        fprintf( stdout , "passed!");
		std::cout<< " in : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
    }

    fprintf( stdout , "Testing endings BLACK\n" );
    fprintf( stdout , "\tTesting mate in 1, BLACK\t\t\t"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, a5}, {PAWN, BLACK, b5}, {BISHOP, BLACK, g3},
        {QUEEN, BLACK, e5}, {ROOK, BLACK, e3} };
        std::vector<PieceAndPosition> initial_positions_w { {KING, WHITE, f1}, {PAWN, WHITE, g2}, {PAWN, WHITE, b3},
        {PAWN, WHITE, a2}, {ROOK, WHITE, b7}, {QUEEN, WHITE, b2}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move expected { e3 , e1, NORMAL_MOVE };
        Move last_move = {c2, b2, NORMAL_MOVE};

        start = std::chrono::steady_clock::now();
        Move tested = ChIAssMove( &empty_board, BLACK, 4, last_move );
        end = std::chrono::steady_clock::now();
        ASSERT_EQUAL( expected , tested );
        fprintf( stdout , "passed!");
		std::cout<< " in : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
    }
    fprintf( stdout , "\tTesting mate in 3, BLACK\t\t\t"); {
        Board empty_board;
        std::vector<PieceAndPosition> initial_positions_w {  {KING, WHITE, f3}, {PAWN, WHITE, a2}, {PAWN, WHITE, c3}, 
        {PAWN, WHITE, d5}, {PAWN, WHITE, f2}, {PAWN, WHITE, h4}, {BISHOP, WHITE, e5}, {ROOK, WHITE, b8}, {ROOK, WHITE, h1},
        {QUEEN, WHITE, c6}};
        std::vector<PieceAndPosition> initial_positions_b { {KING, BLACK, f7}, {PAWN, BLACK, a7}, {PAWN, BLACK, c5},
        {PAWN, BLACK, d6}, {PAWN, BLACK, e7}, {PAWN, BLACK, f5}, {ROOK, BLACK, h8}, {QUEEN, BLACK, c4}};
        SetPosition( &empty_board , initial_positions_w, initial_positions_b );
        Move expected {c4 , e4, NORMAL_MOVE };
        Move last_move = {c2, c3, NORMAL_MOVE};

        start = std::chrono::steady_clock::now();
        Move tested = ChIAssMove( &empty_board, BLACK, 6, last_move );
        end = std::chrono::steady_clock::now();
        ASSERT_EQUAL( expected , tested );
        fprintf( stdout , "passed!");
		std::cout<< " in : " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";
    }
}


int main(void) {

    unsigned short number_of_test = 0;
    unsigned short test_passed = 0;
	cute::ide_listener<> listener{};


    number_of_test++;
    if (cute::makeRunner(listener)(Test)){
        fprintf( stdout , "Testing endings\t\tpassed!" );
        test_passed++;
    } else {
        fprintf( stdout , "Testing endings\t\tfailure...\n" );
    }


    fprintf(stdout , "\nPassed %d/%d test\n", test_passed, number_of_test );
    return 0;


}
